/**
 * 
 */
package cnh.mongodb.components;

/**
 * @author Nick Collier
 */
public class TextUtils {
	
	/**
	 * Cleans the specified String by replace "garbage" characters with suitable replacements.
	 * 
	 * @param str
	 * @return the cleaned string.
	 */
	public static String clean(String str) {
		//for (int i = 0; i < str.length(); i++) {
		//	System.out.println(str.charAt(i) + " (" + Integer.toHexString((int)str.charAt(i)) + ")");
		//}
		// left quote
		String ret = str.replace((char)0x201C, '\"');
		// right quote
		ret = ret.replace((char)0x201D, '\"');
		// right single quote
		ret = ret.replace((char)0x2019, '\'');
		// left single quote
		ret = ret.replace((char)0x2018, '\'');
		// elipsis
		ret = ret.replace(new Character((char)0x2026).toString(), "...");
		// line sep
		ret = ret.replace((char)0x2028, ' ');
		// paragraph sep
		ret = ret.replace((char)0x2029, ' ');
		// dash
		ret = ret.replace((char)0x2014, '-');
		// low quote mark
		ret = ret.replace(new Character((char)0x201A).toString(), "");
		// dagger
		ret = ret.replace(new Character((char)0x2020).toString(), "");
		
		ret = ret.replace(new Character((char)0xc4).toString(), "");
		
		ret = ret.replace(new Character((char)0xae).toString(), "");
		ret = ret.replace(new Character((char)0xac).toString(), "");
		ret = ret.replace(new Character((char)0xf9).toString(), "");
		ret = ret.replace(new Character((char)0xb6).toString(), "");
		ret = ret.replace(new Character((char)0xf4).toString(), "");
		ret = ret.replace(new Character((char)0xfa).toString(), "");
		ret = ret.replace(new Character((char)0xee).toString(), "");
		
		ret = ret.replace("\\\"\"", "\"");
		ret = ret.replace("\\\"", "\"");
		ret = ret.replace("\\n", " ");
		
		return ret;	
	}
}
