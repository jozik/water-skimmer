package cnh.mongodb.components;

import static cnh.mongodb.DBConstants.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import opennlp.uima.Sentence;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import anl.mifs.types.Date;
import anl.mifs.types.DocumentMetaData;
import anl.mifs.types.Name;
import cnh.annotator.sentiment.Sentiment;
import cnh.annotator.types.CNHDocument;
import cnh.annotator.types.Title;
import cnh.mongodb.MongoDBUtil;
import cnh.water.annotation.InstitutionAnnotation;
import cnh.water.annotation.LocationAnnotation;
import cnh.water.annotation.WaterAnnotation;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

// Adapts 
public class MongoDBSentimentOutputter extends JCasAnnotator_ImplBase {
	
	public static final String MESSAGE_DIGEST = "cnh.mongodb.components.AnnotatorMessages";

	DB db;
	DBCollection collection;

	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		String dbName = (String) aContext.getConfigParameterValue("dbName");
		String collectionName = (String) aContext.getConfigParameterValue("collectionName");
		Boolean deleteCollection = (Boolean)aContext.getConfigParameterValue("dropCollection");

		try {
			String host = (String) aContext.getConfigParameterValue("serverAddress");
			Mongo m = null;
			if (host == null || host.isEmpty()) {
				m = new Mongo();
			} else {
				Integer port = (Integer) aContext.getConfigParameterValue("port");
				if (port == null) {
					m = new Mongo(host);
				} else {
					m = new Mongo(host, port);
				}
			}
			db = m.getDB(dbName);
			if (MongoDBUtil.authenticate(db, "Enter your username and password for db: " + dbName)) {
				collection = db.getCollection(collectionName);
				
				if (deleteCollection) {
					collection.drop();
				}
			} else {
				throw new Exception("MongoDB authentication failed for database: " + dbName);
			}

		} catch (Exception e) {
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

			BasicDBObject doc = new BasicDBObject();
			insertDocumentMetaData(doc, aJCas);
			insertTitleAnnotation(doc, aJCas);
			insertWaterAnnotations(doc, aJCas);
			insertLocationAnnotations(doc, aJCas);
			insertInstitutionAnnotations(doc, aJCas);
			insertSentimentAnnotations(doc, aJCas);
			insertSentenceAnnotations(doc, aJCas);
			insertCNHDocumentAnnotations(doc, aJCas);
			
			collection.insert(doc);
	}
	
	private void insertTitleAnnotation(BasicDBObject doc, JCas jcas) {
		FSIterator<Annotation> iter = jcas.getAnnotationIndex(Title.type).iterator(); 
		if (iter.hasNext()) {
			Title title = (Title) iter.next();
			doc.put(TITLE_START, title.getBegin());
			doc.put(TITLE_END, title.getEnd());
		}
		
	}
	
	private void insertCNHDocumentAnnotations(BasicDBObject doc, JCas jcas) {
		FSIterator<Annotation> iter = jcas.getAnnotationIndex(CNHDocument.type).iterator(); 
		if (iter.hasNext()) {
			CNHDocument cnhDoc = (CNHDocument) iter.next();
			doc.put(TOKEN_COUNT, cnhDoc.getTokenCount());
		}
	}

	private void insertSentenceAnnotations(BasicDBObject doc, JCas aJCas) {
		// put sentence annotations
		List<BasicDBObject> sentenceList = new ArrayList<BasicDBObject>();
		for (FSIterator<Annotation> sentenceIter = aJCas.getAnnotationIndex(Sentence.type).iterator(); sentenceIter
				.hasNext();) {
			Sentence sent = (Sentence) sentenceIter.next();
			BasicDBObject n = new BasicDBObject();
//			n.put(TEXT, sent.getCoveredText());
			n.put(START_TOKEN_INDEX, sent.getStartTokenIndex());
			n.put(END_TOKEN_INDEX, sent.getEndTokenIndex());
			n.put(START, sent.getBegin());
			n.put(END, sent.getEnd());
			sentenceList.add(n);
		}
		doc.put(DOCUMENT_SENTENCES, sentenceList);
	}

	private void insertSentimentAnnotations(BasicDBObject doc, JCas aJCas) {
		// put sentiment annotations
		FSIterator<Annotation> sentimentIterator = aJCas.getAnnotationIndex(Sentiment.type).iterator();
		List<BasicDBObject> sentimentList = new ArrayList<BasicDBObject>();
		while (sentimentIterator.hasNext()) {
			Sentiment sent = (Sentiment) sentimentIterator.next();
			BasicDBObject n = new BasicDBObject();
			n.put(SUBJECTIVITY_TYPE, sent.getSubjType());
			n.put(PRIOR_POLARITY, sent.getPriorP());
			n.put(TEXT, sent.getCoveredText());
			n.put(TOKEN_INDEX, sent.getTokenIndex());
			n.put(START, sent.getBegin());
			n.put(END, sent.getEnd());
			sentimentList.add(n);
		}
		doc.put(DOCUMENT_SENTIMENT, sentimentList);
	}
	
	private void insertLocationAnnotations(BasicDBObject doc, JCas aJCas) {
		FSIterator<Annotation> locIterator = aJCas.getAnnotationIndex(LocationAnnotation.type).iterator();
		List<BasicDBObject> locList = new ArrayList<BasicDBObject>();
		while (locIterator.hasNext()) {
			LocationAnnotation location = (LocationAnnotation) locIterator.next();
			BasicDBObject n = new BasicDBObject();
			n.put(LOCATION_CATEGORY, location.getLocation());
			n.put(SUB_LOCATION_CATEGORY, location.getSublocation());
			n.put(START_TOKEN_INDEX, location.getStartTokenIndex());
			n.put(END_TOKEN_INDEX, location.getEndTokenIndex());
			n.put(TEXT, location.getCoveredText());
			n.put(START, location.getBegin());
			n.put(END, location.getEnd());
			locList.add(n);
		}
		doc.put(DOCUMENT_LOCATION, locList);
	}
	
	private void insertInstitutionAnnotations(BasicDBObject doc, JCas aJCas) {
		FSIterator<Annotation> locIterator = aJCas.getAnnotationIndex(InstitutionAnnotation.type).iterator();
		List<BasicDBObject> locList = new ArrayList<BasicDBObject>();
		while (locIterator.hasNext()) {
			InstitutionAnnotation institution = (InstitutionAnnotation) locIterator.next();
			BasicDBObject n = new BasicDBObject();
			n.put(INSTITUTION_LOCATION_CATEGORY, institution.getLocation());
			n.put(INSTITUTION_NAME_CATEGORY, institution.getName());
			n.put(START_TOKEN_INDEX, institution.getStartTokenIndex());
			n.put(END_TOKEN_INDEX, institution.getEndTokenIndex());
			n.put(TEXT, institution.getCoveredText());
			n.put(START, institution.getBegin());
			n.put(END, institution.getEnd());
			locList.add(n);
		}
		doc.put(DOCUMENT_INSTITUTION, locList);
	}

	private void insertWaterAnnotations(BasicDBObject doc, JCas aJCas) {
		// put water annotations
		FSIterator<Annotation> waterIterator = aJCas.getAnnotationIndex(WaterAnnotation.type).iterator();
		List<BasicDBObject> waterList = new ArrayList<BasicDBObject>();
		while (waterIterator.hasNext()) {
			WaterAnnotation water = (WaterAnnotation) waterIterator.next();
			BasicDBObject n = new BasicDBObject();
			n.put(KIND, water.getWaterCategory());
			n.put(START_TOKEN_INDEX, water.getStartTokenIndex());
			n.put(END_TOKEN_INDEX, water.getEndTokenIndex());
			n.put(TEXT, water.getCoveredText());
			n.put(START, water.getBegin());
			n.put(END, water.getEnd());
			waterList.add(n);
		}
		doc.put(DOCUMENT_WATER, waterList);
	}
	
	

	private void insertDocumentMetaData(BasicDBObject doc, JCas jcas) throws AnalysisEngineProcessException {
		FSIterator<Annotation> iter = jcas.getAnnotationIndex(DocumentMetaData.type).iterator();
		if (!iter.hasNext()) throw new AnalysisEngineProcessException(MESSAGE_DIGEST, "doc_metadata_not_found_error", new Object[0]);
		DocumentMetaData annot = (DocumentMetaData)iter.next();
		doc.put(TITLE, annot.getTitle());
		doc.put(LOCATION, annot.getLocation());
		Date mifsDate = annot.getDate();
		Calendar cal = new GregorianCalendar(mifsDate.getYear(), mifsDate.getMonth() - 1, mifsDate.getDay());
		doc.put(DATE, cal.getTime());
		doc.put(SOURCE, annot.getSource());
		doc.put(LENGTH, annot.getEnd() - annot.getBegin());

		FSArray authors = annot.getAuthors();
		List<BasicDBObject> authList = new ArrayList<BasicDBObject>();
		for (int i = 0; i < authors.size(); i++) {
			Name name = (Name) authors.get(i);
			BasicDBObject n = new BasicDBObject();
			n.put(FIRST_NAME, name.getFirstName());
			n.put(LAST_NAME, name.getLastName());
			n.put(INITIALS, name.getInitials());
			authList.add(n);
		}
		doc.put(AUTHORS, authList);
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		super.collectionProcessComplete();
		Mongo m = db.getMongo();
		m.close();
	}

}
