package cnh.mongodb.components;

import static cnh.mongodb.DBConstants.CONTENTS;
import static cnh.mongodb.DBConstants.DATE;
import static cnh.mongodb.DBConstants.LOCATION;
import static cnh.mongodb.DBConstants.SOURCE;
import static cnh.mongodb.DBConstants.TITLE;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.collection.CollectionReader_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Progress;

import anl.mifs.reader.ParsedAuthors;
import anl.mifs.reader.ParsedDate;
import anl.mifs.types.DocumentMetaData;
import cnh.mongodb.MongoDBUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class MongoDBReader extends CollectionReader_ImplBase {

	DB db;
	String collectionName;
	DBCursor cursor;

	public void initialize() throws ResourceInitializationException {
		String dbName = (String) getConfigParameterValue("dbName");
		collectionName = (String) getConfigParameterValue("collectionName");

		try {
			String host = (String) getConfigParameterValue("serverAddress");
			Mongo m = null;
			if (host == null || host.isEmpty()) {
				m = new Mongo();
			} else {
				Integer port = (Integer) getConfigParameterValue("port");
				if (port == null) {
					m = new Mongo(host);
				} else {
					m = new Mongo(host, port);
				}
			}
			db = m.getDB(dbName);
			if (MongoDBUtil.authenticate(db, "Enter your username and password for db: " + dbName)) {
				cursor = db.getCollection(collectionName).find();
			} else {
				throw new Exception("MongoDB authentication failed for database: " + dbName);
			}
		} catch (Exception e) {
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void getNext(CAS cas) throws IOException, CollectionException {
		JCas jcas;
		try {
			jcas = cas.getJCas();
			// the snowball processor expects a document language so we have
			// to provide one here.
			jcas.setDocumentLanguage("en");
			
			DBObject next = cursor.next();
			ReaderResult result = read(next);
			String contents = result.getContents();
			
			int length = contents.length();
			DocumentMetaData data = new DocumentMetaData(jcas, 0, length);
			ReaderResult.Error error = result.process(data, jcas);

			if (error != ReaderResult.Error.NONE) {
				UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error on " + error.toString());
			} 

		} catch (Throwable t) {
			UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", t);
			t.printStackTrace();
		}
	}

	/**
	 * Gets the URL of the current document.
	 * 
	 * @return the URL of the current document.
	 */
	protected String getDocumentURL() {
		return location;
	}

	String location;

	/**
	 * Reads the text from the specified DBObject.
	 * 
	 * @param dbObject
	 *            the dbObject of the article
	 * @return the result of reading and parsing the text
	 * @throws IOException
	 *             if there is an error while reading
	 */
	public ReaderResult read(DBObject dbObject) throws IOException {
		ReaderResult result = new ReaderResult();

		String title = (String) dbObject.get(TITLE);
		result.setTitle(title.trim());

		location = (String) dbObject.get(LOCATION);
		result.setLocation(location);

		Date date = (Date) dbObject.get(DATE);
		result.setDate(new ParsedDate(date));

		String source = (String) dbObject.get(SOURCE);
		result.setSource(source);

		result.setLink("");

		ParsedAuthors authors = new ParsedAuthors();
		@SuppressWarnings("unchecked")
		List<BasicDBObject> authList = (List<BasicDBObject>) dbObject.get("authors");
		for (BasicDBObject auth : authList) {
			String firstName = (String) auth.get("firstName");
			String lastName = (String) auth.get("lastName");
			String initials = (String) auth.get("initials");
			authors.addName(firstName, lastName, initials);
		}
		result.setAuthors(authors);

		String contents = (String) dbObject.get(CONTENTS);
		result.setContents(contents);

		return result;
	}

	@Override
	public void close() throws IOException {
		Mongo m = db.getMongo();
		m.close();
	}

	@Override
	public Progress[] getProgress() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasNext() throws IOException, CollectionException {
		return cursor.hasNext();
	}

}
