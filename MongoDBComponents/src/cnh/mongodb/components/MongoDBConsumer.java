package cnh.mongodb.components;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import anl.mifs.types.Date;
import anl.mifs.types.DocumentMetaData;
import anl.mifs.types.Name;

import cnh.mongodb.MongoDBUtil;
import static cnh.mongodb.DBConstants.*;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;

// Adapts 
public class MongoDBConsumer extends JCasAnnotator_ImplBase {
	
	DB db;
	
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		String dbName = (String)aContext.getConfigParameterValue("dbName");
		
		try {
			String host = (String) aContext.getConfigParameterValue("serverAddress");
			Mongo m = null;
			if (host == null || host.isEmpty()){
				m = new Mongo();
			}
			else {
				Integer port = (Integer) aContext.getConfigParameterValue("port");
				if (port == null){
					m = new Mongo(host);
				}
				else {
					m = new Mongo(host,port);
				}
			}
			db = m.getDB(dbName);
			if (MongoDBUtil.authenticate(db, "Enter your username and password for db: " + dbName)){
			}
			else {
				throw new Exception("MongoDB authentication failed for database: " + dbName);
			}
		} catch (Exception e) {
			throw new ResourceInitializationException(e);
		}
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {

		String docText = aJCas.getDocumentText();
		FSIterator<Annotation> documentMetaDataIterator = aJCas.getAnnotationIndex(DocumentMetaData.type).iterator();
		DocumentMetaData annot = null;
		if (documentMetaDataIterator.hasNext()) {
	          // get OpenNLP Token
	          annot = (DocumentMetaData) documentMetaDataIterator.next();
	          
	      }
		if (annot != null){
			
			BasicDBObject doc = new BasicDBObject();

	        doc.put(TITLE, annot.getTitle());
	        doc.put(LOCATION, annot.getLocation());
	        Date mifsDate = annot.getDate();
	        Calendar cal = new GregorianCalendar(mifsDate.getYear(), mifsDate.getMonth() - 1, mifsDate.getDay());
	        doc.put(DATE, cal.getTime());
	        String source = annot.getSource();
	        doc.put(SOURCE, source);
	        FSArray authors = annot.getAuthors();
	        List<BasicDBObject> authList = new ArrayList<BasicDBObject>();
	        for (int i = 0; i < authors.size(); i++){
	        	Name name = (Name)authors.get(i);
	        	BasicDBObject n = new BasicDBObject();
	        	n.put(FIRST_NAME, name.getFirstName());
	        	n.put(LAST_NAME, name.getLastName());
	        	n.put(INITIALS, name.getInitials());
	        	authList.add(n);
	        }
	        doc.put(AUTHORS, authList);
	        doc.put(CONTENTS, docText);
	        doc.put(LENGTH, annot.getEnd() - annot.getBegin());
	        
	        DBCollection coll = db.getCollection(getDerivedCollectionName(source));
	        coll.insert(doc);
		}
	}
	
	protected static String getDerivedCollectionName(String sourceName){
		return StringUtils.deleteWhitespace(sourceName);
	}

	@Override
	public void collectionProcessComplete()
			throws AnalysisEngineProcessException {
		super.collectionProcessComplete();
		Mongo m = db.getMongo();
		m.close();
	}
	
	

}
