package cnh.mongodb.analysis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * Calculates a tf-idf and topicality score for all the docs returned by a query
 * or all the docs in a collection. The document frequency is the number of
 * documents that is at least one element in the documentWater list where "kind"
 * is equal to a specified category. The term count is the number of elements in
 * the documentWater list where kind is equal to a specified category. This will
 * update the document or documents with a new property containing the score.
 * The property name is DBConstants.TF_IDF + "_" + category where category is
 * the specified water annotation category.
 * 
 * @author Nick Collier
 */
public abstract class AbstractScorer {

	private static int MIN_SENTENCE_COUNT = 4;

	private DB db;

	private IDFCalculator idfCalc;
	private TFCalculator tfCalc;

	public AbstractScorer(DB db, TFCalculator tfCalc, IDFCalculator idfCalc) {
		this.db = db;
		this.tfCalc = tfCalc;
		this.idfCalc = idfCalc;
	}

	public CategoryScores createPrimaryCategoryScores(String collection, String category) {
		return createPrimaryCategoryScores(collection, new BasicDBObject(), category);
	}

	/**
	 * Creates a CategoryScores collection that includes a tf/idf score for each
	 * document in the query for the specified category. This does NOT update
	 * the database.
	 * 
	 * @param collection
	 * @param query
	 * @param category
	 * @return
	 */
	public CategoryScores createPrimaryCategoryScores(String collection, DBObject query, String category) {
		CategoryScores scores = new CategoryScores(category);
		DBCollection dbCollection = db.getCollection(collection);
		float idf = idfCalc.idf(dbCollection, category);
		for (DBCursor cursor = dbCollection.find(query); cursor.hasNext();) {
			DBObject doc = cursor.next();
			float score = calcDocScore(doc, category, idf);
			scores.addScore(doc, score);
		}

		return scores;
	}

	public void scoreCollection(String collection, CategoryScores catScores, String... categories) {
		// empty query object will return everything
		scoreCollection(collection, new BasicDBObject(), catScores, categories);
	}
	
	/**
	 * Scores the documents in the collection with ONLY a tf-idf score for each
	 * category.
	 * 
	 * @param collection
	 * @param query
	 * @param categories
	 */
	public void scoreCollectionTFIDF(String collection, String... categories) {
		scoreCollectionTFIDF(collection, new BasicDBObject(), categories);
	}

	/**
	 * Scores the documents in the collection with ONLY a tf-idf score for each
	 * category.
	 * 
	 * @param collection
	 * @param query
	 * @param categories
	 */
	public void scoreCollectionTFIDF(String collection, DBObject query, String... categories) {
		DBCollection dbCollection = db.getCollection(collection);
		Map<String, Float> idfMap = new HashMap<String, Float>();
		for (String category : categories) {
			idfMap.put(category, idfCalc.idf(dbCollection, category));
		}

		for (DBCursor cursor = dbCollection.find(query); cursor.hasNext();) {
			DBObject doc = cursor.next();
			for (String category : categories) {
				float idf = idfMap.get(category);
				float score = calculateScore(doc, category, idf);
				doc.put(createTFIDFKey(category), score);
			}
			dbCollection.save(doc);
		}
		
	}

	/**
	 * Scores every document returned by the query with both a tf/idf score and
	 * a topicality score. The score categories are the one used in the
	 * CategoryScores and its co-occurence with the specified categories.
	 * 
	 * @param collection
	 * @param query
	 * @param catScores
	 * @param categories
	 */
	public void scoreCollection(String collection, DBObject query, CategoryScores catScores, String... categories) {
		DBCollection dbCollection = db.getCollection(collection);
		String primaryCategory = catScores.getCategory();

		Map<String, Float> idfMap = new HashMap<String, Float>();
		for (String category : categories) {
			idfMap.put(category, idfCalc.idf(dbCollection, category));
		}

		for (DBCursor cursor = dbCollection.find(query); cursor.hasNext();) {
			DBObject doc = cursor.next();
			float waterScore = catScores.getScore(doc);
			doc.put(createTFIDFKey(primaryCategory), waterScore);
			if (waterScore == 0) {
				for (String category : categories) {
					doc.put(createTFIDFKey(primaryCategory, category), 0d);
					doc.put(createTopicalityKey(primaryCategory, category), 0d);
				}

			} else {
				for (String category : categories) {
					float sum = waterScore;
					float idf = idfMap.get(category);
					float score = calculateScore(doc, category, idf);
					if (score > 0)
						sum += score;
					else
						sum = 0;

					doc.put(createTFIDFKey(primaryCategory, category), sum);
					float topicality = sum * catScores.getNormalizedScore(doc);
					doc.put(createTopicalityKey(primaryCategory, category), topicality);

				}
			}

			dbCollection.save(doc);
		}
	}

	private float calcDocScore(DBObject doc, String category, float idf) {
		if (((List<?>) doc.get("documentSentences")).size() >= MIN_SENTENCE_COUNT) {
			return calculateScore(doc, category, idf);
		}
		return 0f;
	}

	private float calculateScore(DBObject doc, String category, float idf) {
		float tf = tfCalc.tf(doc, category);
		// System.out.printf("tf: %f, idf: %f%n", tf, idf);
		return idf * tf * norm(doc);
	}

	private float norm(DBObject doc) {
		int termCount = ((Integer) doc.get(DBConstants.TOKEN_COUNT)).intValue();
		return (float) (1.0 / Math.sqrt(termCount));
	}

	
	private String createTopicalityKey(String... categories) {
		return createKey(DBConstants.TOPICALITY, categories);
	}

	private String createKey(String prefix, String... categories) {
		StringBuilder catBuf = new StringBuilder(prefix);
		
		for (String category : categories) {
			//category = category.replace(".", ""); 
			catBuf.append("_");
			catBuf.append(category);
		}

		return catBuf.toString();
	}

	private String createTFIDFKey(String... categories) {
		return createKey(DBConstants.TF_IDF, categories);
	}
}
