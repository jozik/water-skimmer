/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DBObject;

/**
 * Calculates the term frequency of term within a DBObject.
 * 
 * @author Nick Collier
 */
public interface TFCalculator {
	
	float BOOST_VAL = 1.5f;
	
	/**
	 * Calculates the term frequency of term within a DBObject.
	 * 
	 * @param doc
	 * @param term
	 * 
	 * @return  the term frequency of term within a DBObject.
	 */
	 float tf(DBObject doc, String category);

}
