/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * AggregateAnalyzer that "calculates" the total doc count.
 * 
 * @author Nick Collier
 */
public class TotalDocCount implements AggregateAnalyzer {
	

	/* (non-Javadoc)
	 * @see cnh.mongodb.analysis.AggregateAnalyzer#getAnalysis()
	 */
	@Override
	public String getAnalysisName() {
		return "TOTAL_DOC_COUNT";
	}

	/* (non-Javadoc)
	 * @see cnh.mongodb.analysis.AggregateAnalyzer#run(com.mongodb.DBCollection, com.mongodb.DBObject)
	 */
	@Override
	public double run(DBCollection collection, DBObject query) {
		return collection.count(query);
	}
}
