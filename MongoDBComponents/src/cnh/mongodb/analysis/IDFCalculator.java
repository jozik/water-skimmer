/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DBCollection;

/**
 * Calculates the inverse document frequency for a term over a DBCollection.
 * 
 * @author Nick Collier
 */
public interface IDFCalculator {
	
	/**
	 * Calculates the inverse document frequency for a term over a DBCollection.
	 * 
	 * @return the inverse document frequency for a term over a DBCollection.
	 */
	float idf(DBCollection collection, String term);

}
