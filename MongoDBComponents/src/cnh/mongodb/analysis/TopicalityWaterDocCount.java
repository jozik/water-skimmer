package cnh.mongodb.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Counts the number of documents that contain water annotations plus
 * optional other water categories, and includes only a top fraction of
 * based on topicality or tf_idf.
 * 
 * @author Nick Collier
 */
public class TopicalityWaterDocCount implements AggregateAnalyzer {
	
	// Score type to use for ordering
	public enum SCORING_TYPE {
		TF_IDF,TOPICALITY
	}

	private List<String> categories = new ArrayList<String>();
	private String name;
	private final double fraction;
	private SCORING_TYPE score;
	
	// Maybe
//	public static TopicalityWaterDocCount createTWDC(String category,double fraction){
//		
//	}
	
	public TopicalityWaterDocCount(double fraction) {
		categories.add("WATER");
		this.fraction = fraction;
		this.score = SCORING_TYPE.TF_IDF;
	}
	
	public TopicalityWaterDocCount(double fraction, String otherCategory) {
		this(fraction);
		categories.add(otherCategory);
		this.score = SCORING_TYPE.TOPICALITY;
	}
	
	protected String getStringFromFraction(double fraction){
		return Double.toString(fraction);
	}

	/* (non-Javadoc)
	 * @see cnh.mongodb.analysis.AggregateAnalyzer#getAnalysisName()
	 */
	@Override
	public String getAnalysisName() {
		if (name == null) {
			StringBuilder builder = new StringBuilder(categories.get(0));
			for (int i = 1; i < categories.size(); i++) {
				builder.append("_");
				builder.append(categories.get(i));
			}
			builder.append("_");
			builder.append(getStringFromFraction(fraction));
			builder.append("_DOC_COUNT");
			name = builder.toString();
		}
		return name;
	}
	
	// Map to hold full collection name and min score
	private Map<String,Double> collectionMinScore = new HashMap<String,Double>();
	
	protected String getScoringKey() {
		StringBuilder sb = new StringBuilder();
		String prefix = score == SCORING_TYPE.TF_IDF ? "tf_idf" : "topicality";
		sb.append(prefix);
		for (String catString : categories){
			sb.append("_");
			sb.append(catString);
		}
		return  sb.toString();
	}

	@Override
	public double run(DBCollection collection, DBObject query) {
		String collectionName = collection.getFullName();
		Double minScore = collectionMinScore.get(collectionName);
		
		// Build up the categories for $and queries below
		List<BasicDBObject> list = new ArrayList<BasicDBObject>();
		for (String category : categories) {
			BasicDBObject n = new BasicDBObject();
			n.put(DBConstants.DOCUMENT_WATER_KIND, category);
			list.add(n);
		}
		// Get the scoring key (e.g., tf_idf_WATER)
		String scoringKey = getScoringKey();
		
		if (minScore == null){
			// Figure out the minimum allowed score to be in top fraction
			
			// $and query
			BasicDBObjectBuilder objBuilder = BasicDBObjectBuilder.start();
			objBuilder.add("$and", list);
			
			
			
			// Only look for scoring keys greater than zero
			DBObject greaterThanZero = new BasicDBObject("$gt", 0);
			objBuilder.add(scoringKey, greaterThanZero);
			
			// Count the number of non-zero score elements in this collection
			DBObject q1 = objBuilder.get();
//			objBuilder.pop();
			
			long numberOfNonZeroScores = collection.count(q1);
			
			System.out.println("Number of non zero docs for collection " + collectionName + " " + scoringKey + " is: " + numberOfNonZeroScores);
			
			// Derive cutoff number corresponding to top fraction
			int cutoff = (int) Math.floor((double) numberOfNonZeroScores * fraction);
			
			// Sort by scoring key, descending
			DBObject orderBy = new BasicDBObject(scoringKey, -1);
			if (cutoff < 1){
				minScore = Double.MAX_VALUE;
			}
			else {
				// Skip cutoff - 1 entries
				DBObject minObject = collection.find(q1).sort(orderBy).skip(cutoff - 1).next();
				// Get score of cutoff element
				minScore = (Double) minObject.get(scoringKey);
			}
			System.out.println("MinScore for " + scoringKey + " is: " + minScore);
			// Store min score with collection name
			collectionMinScore.put(collectionName, minScore);
		}
	
		if (minScore == Double.MAX_VALUE){
			return 0;
		}
		BasicDBObjectBuilder objBuilder = BasicDBObjectBuilder.start(query.toMap());
		objBuilder.add("$and", list);
		DBObject greaterThanMinScore = new BasicDBObject("$gt", minScore);
		objBuilder.add(scoringKey, greaterThanMinScore);
		return collection.count(objBuilder.get());
	}

}
