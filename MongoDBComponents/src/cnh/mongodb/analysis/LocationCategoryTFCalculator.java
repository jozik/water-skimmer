/**
 * 
 */
package cnh.mongodb.analysis;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Calculates the TF for location categories.
 * 
 * @author Nick Collier
 */
public class LocationCategoryTFCalculator implements TFCalculator {

	/**
	 * Counts the number of documentLocation.locationCategory and
	 * documentLocation.subLocation entries that match the category and returns
	 * the sqrt of that multiplied the title boost. These categories shouldn't
	 * overlap so we can use this to count both separately.
	 * 
	 * @param doc
	 * @param category
	 * @return the sqrt of the number of documentLocation.location or documentLocation.subLocation entries that match
	 *         the category.
	 */
	public float tf(DBObject doc, String category) {
		long count = 0;
		int boostCount = 0;
		int titleEnd = ((Integer) doc.get(DBConstants.TITLE_END)).intValue();
		for (Object obj : (BasicDBList) doc.get(DBConstants.DOCUMENT_LOCATION)) {
			BasicDBObject locationObj = (BasicDBObject) obj;
			if (locationObj.getString(DBConstants.LOCATION_CATEGORY).equals(category)) {
				count++;
				if (locationObj.getInt(DBConstants.START) < titleEnd) {
					boostCount++;
				}
			} else if (locationObj.getString(DBConstants.SUB_LOCATION_CATEGORY).equals(category)) {
				count++;
				if (locationObj.getInt(DBConstants.START) < titleEnd) {
					boostCount++;
				}
			}
		}

		float boost = boostCount == 0 ? 1 : BOOST_VAL * boostCount;
		return (float) Math.sqrt(count) * boost;
	}

}
