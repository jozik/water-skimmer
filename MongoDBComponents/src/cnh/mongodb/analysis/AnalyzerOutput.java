/**
 * 
 */
package cnh.mongodb.analysis;

import java.util.HashMap;
import java.util.Map;

import anl.mifs.util.DateRange;

/**
 * Stores an individual line of output from a MonthlyAggregator. 
 * 
 * @author Nick Collier
 */
public class AnalyzerOutput {
	
	private DateRange range;
	private String collection;
	
	private Map<String, Double> output = new HashMap<String, Double>();

	
	public AnalyzerOutput(DateRange range, String collection) {
		this.range = range;
		this.collection = collection;
	}
	
	/**
	 * Gets the DateRange over which this output was calculated.
	 * 
	 * @return the DateRange over which this output was calculated.
	 */
	public DateRange getDateRange() {
		return range;
	}
	
	/**
	 * Gets the name of the collection over which this output was calculated.
	 * 
	 * @return the name of the collection over which this output was calculated.
	 */
	public String getCollection() {
		return collection;
	}
	
	/**
	 * Gets the output value associated with the specified key.
	 * 
	 * @param key
	 * 
	 * @return the output value associated with the specified key.
	 */
	public double getValue(String key) {
		Double val = output.get(key);
		if (val == null) throw new IllegalArgumentException(key + " not found.");
		return val.doubleValue();
	}

	/**
	 * Adds the specified output value to this AnalyzerOutput and associates that
	 * value with the specified name. 
	 * 
	 * @param name
	 * @param value
	 */
	public void addOutput(String name, double value) {
		output.put(name, value);
	}
}
