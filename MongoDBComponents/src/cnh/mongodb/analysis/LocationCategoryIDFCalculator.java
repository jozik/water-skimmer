/**
 * 
 */
package cnh.mongodb.analysis;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.QueryBuilder;

/**
 * Calculates the IDF for location category annotations.
 * 
 * @author Nick Collier
 */
public class LocationCategoryIDFCalculator implements IDFCalculator {
	
	/**
	 * Calculates idf as log(numDocs / (docFreq + 1)) + 1
	 * 
	 * @param collection
	 * @param feature
	 * 
	 * @return the idf as log(numDocs / (docFreq + 1)) + 1
	 */
	public float idf(DBCollection collection, String feature) {
		long numDocs = collection.count();

		// these features shouln't overlap so this should work.
		BasicDBObject query1 = new BasicDBObject();
		query1.put(DBConstants.DOCUMENT_LOCATION_CATEGORY, feature);
		
		BasicDBObject query2 = new BasicDBObject();
		query2.put(DBConstants.DOCUMENT_SUBLOCATION_CATEGORY, feature);
		
		QueryBuilder builder = new QueryBuilder();
		builder.or(query1, query2);
		long docFreq = collection.find(builder.get()).count();

		return (float) (Math.log(numDocs / (double) (docFreq + 1)) + 1.0);
	}

}
