package cnh.mongodb.analysis;

public class DocCountAggregatorConstants {
	// all but water
		public static final String[] CATEGORIES = {"WQUALITY", "WRECHARGE", "FLOOD", "ADAPTIVER", "AHABITAT",
				"CLIMATEC", "DROUGHT", "DWATER", "ENVIRONMENT", "GROUNDWATER", "HABITAT", "IRRIGATION", "MALADAPT",
				"PRECIPITATION", "TEMPERATURE", "WTABLE", "WILDFIRE", "WLEVEL", "WSUPPLY", "METALS", "WUSE"
				};
		
		public static final String[] TEST_CATEGORIES = {"WQUALITY"};
		
		public static String[] COLLECTIONS = {"ArizonaDailyStar", "ArizonaDailySun", "GrandJunctionFreePress", "LasVegasSun"};
		
		public static String[] TEST_COLLECTIONS = {"GrandJunctionFreePress"};

}
