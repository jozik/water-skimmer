/**
 * 
 */
package cnh.mongodb.analysis;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;

/**
 * Calculates the IDF for water category annotations.
 * 
 * @author Nick Collier
 */
public class WaterCategoryIDFCalculator implements IDFCalculator {
	
	/**
	 * Calculates idf as log(numDocs / (docFreq + 1)) + 1
	 * 
	 * @param collection
	 * @param feature
	 * 
	 * @return the idf as log(numDocs / (docFreq + 1)) + 1
	 */
	public float idf(DBCollection collection, String feature) {
		long numDocs = collection.count();

		BasicDBObject query = new BasicDBObject();
		query.put(DBConstants.DOCUMENT_WATER_KIND, feature);
		long docFreq = collection.find(query).count();

		return (float) (Math.log(numDocs / (double) (docFreq + 1)) + 1.0);
	}

}
