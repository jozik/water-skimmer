/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DB;

/**
 * Calculates the tf/idf and topicality scores for the water categories. 
 * 
 * @author Nick Collier
 */
public class WaterCategoryScorer extends AbstractScorer {
	
	public WaterCategoryScorer(DB db) {
		super(db, new WaterCategoryTFCalculator(), new WaterCategoryIDFCalculator());
	}

}
