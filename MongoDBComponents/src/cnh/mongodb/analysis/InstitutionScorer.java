/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DB;

/**
 * Scores tf/idf for institution annotation categories. 
 * 
 * @author Nick Collier
 */
public class InstitutionScorer extends AbstractScorer {
	
	public InstitutionScorer(DB db) {
		super(db, new InstitutionCategoryTFCalculator(), new InstitutionCategoryIDFCalculator());
	}

}
