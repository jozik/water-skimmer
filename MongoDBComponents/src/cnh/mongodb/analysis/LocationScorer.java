/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DB;

/**
 * Scores tf/idf for location annotation categories. 
 * 
 * @author Nick Collier
 */
public class LocationScorer extends AbstractScorer {
	
	public LocationScorer(DB db) {
		super(db, new LocationCategoryTFCalculator(), new LocationCategoryIDFCalculator());
	}

}
