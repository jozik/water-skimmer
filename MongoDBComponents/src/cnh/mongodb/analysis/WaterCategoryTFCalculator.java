/**
 * 
 */
package cnh.mongodb.analysis;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Calculates the TF for water categories.
 * 
 * @author Nick Collier
 */
public class WaterCategoryTFCalculator implements TFCalculator {

	/**
	 * Counts the number of documentWater.kind entries that match the category
	 * and returns the sqrt of that multiplied the title boost.
	 * 
	 * @param doc
	 * @param category
	 * @return the sqrt of the number of documentWater.kind entries that match
	 *         the category.
	 */
	public float tf(DBObject doc, String category) {
		long count = 0;
		int boostCount = 0;
		int titleEnd = ((Integer) doc.get(DBConstants.TITLE_END)).intValue();
		for (Object obj : (BasicDBList) doc.get(DBConstants.DOCUMENT_WATER)) {
			BasicDBObject waterObj = (BasicDBObject) obj;
			if (waterObj.getString(DBConstants.KIND).equals(category)) {
				count++;
				if (waterObj.getInt(DBConstants.START) < titleEnd) {
					boostCount++;
				}
			}
		}

		float boost = boostCount == 0 ? 1 : BOOST_VAL * boostCount;
		return (float) Math.sqrt(count) * boost;
	}

}
