/**
 * 
 */
package cnh.mongodb.analysis;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Calculates the TF for institution categories.
 * 
 * @author Nick Collier
 */
public class InstitutionCategoryTFCalculator implements TFCalculator {

	/**
	 * Counts the number of documentInstitution.institutionLocation and
	 * documentInstitution.name entries that match the category and returns the
	 * sqrt of that multiplied the title boost. These categories shouldn't
	 * overlap so we can use this to count both separately.
	 * 
	 * @param doc
	 * @param category
	 * @return the sqrt of the number of documentInstitution.institutionLocation
	 *         or documentInstitution.name entriesentries that match the
	 *         category.
	 */
	public float tf(DBObject doc, String category) {
		long count = 0;
		int boostCount = 0;
		int titleEnd = ((Integer) doc.get(DBConstants.TITLE_END)).intValue();
		for (Object obj : (BasicDBList) doc.get(DBConstants.DOCUMENT_INSTITUTION)) {
			BasicDBObject institutionObj = (BasicDBObject) obj;
			if (institutionObj.getString(DBConstants.INSTITUTION_LOCATION_CATEGORY).equals(category)) {
				count++;
				if (institutionObj.getInt(DBConstants.START) < titleEnd) {
					boostCount++;
				}
			} else if (institutionObj.getString(DBConstants.INSTITUTION_NAME_CATEGORY).equals(category)) {
				count++;
				if (institutionObj.getInt(DBConstants.START) < titleEnd) {
					boostCount++;
				}
			}
		}

		float boost = boostCount == 0 ? 1 : BOOST_VAL * boostCount;
		return (float) Math.sqrt(count) * boost;
	}

}
