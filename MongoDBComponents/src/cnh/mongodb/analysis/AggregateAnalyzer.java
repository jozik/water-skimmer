/**
 * 
 */
package cnh.mongodb.analysis;

import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Interface for classes that can perform an aggreate analysis over
 * multiple DBObjects.
 * 
 * @author Nick Collier
 */
public interface AggregateAnalyzer {
	
	/**
	 * Gets the name of this analysis. This will be used to
	 * id the results of this analysis.
	 * 
	 * @return the name of this analysis
	 */
	String getAnalysisName();
	
	/**
	 * Runs the analysis on the specifie collection
	 * using the specified query. Implementors can add
	 * to the query to create their own cursors etc.
	 * 
	 * @param collection
	 * @param query
	 * 
	 * return the result of the analysis.
	 */
	double run(DBCollection collection, DBObject query);
}
