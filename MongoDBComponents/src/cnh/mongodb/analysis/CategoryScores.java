/**
 * 
 */
package cnh.mongodb.analysis;

import java.util.HashMap;
import java.util.Map;

import org.bson.types.ObjectId;

import com.mongodb.DBObject;

/**
 * Collects the if idf scores for a category. 
 * 
 * @author Nick Collier
 */
public class CategoryScores {
	
	private Map<ObjectId, Float> scores = new HashMap<ObjectId, Float>();
	private float min = Float.POSITIVE_INFINITY, max = Float.NEGATIVE_INFINITY;
	private String category;
	
	public CategoryScores(String category) {
		this.category = category;
	}
	
	/**
	 * Gets the category that this carries the scores for.
	 * 
	 * @return the category that this carries the scores for.
	 */
	public String getCategory() {
		return category;
	}
	
	/**
	 * Adds the score for the specified doc to this PrimaryScores.
	 * 
	 * @param doc
	 * @param score
	 */
	public void addScore(DBObject doc, float score) {
		scores.put((ObjectId)doc.get("_id"), score);
		if ((score > 0) && (score < min)) min = score; // Odd case where all scores are zero doesn't matter...
//		if (score < min) min = score;
		if (score > max) max = score;
	}
	
	/**
	 * Gets the score for the specified doc.
	 * @param doc
	 * @return the score the specified doc.
	 * 
	 * @throws IllegalArgumentException if there is no score for the specified doc.
	 */
	public float getScore(DBObject doc) {
		Float val = scores.get((ObjectId)doc.get("_id"));
		if (val == null) throw new IllegalArgumentException("No score has been added for DBObject " + doc);
		return val.floatValue();
	}
	
	public float getNormalizedScore(DBObject doc) {
		float score = getScore(doc);
		if(min == Float.POSITIVE_INFINITY) return 0;
		return (score - min) / (max - min);
	}
	
	/**
	 * Gets the minimum score.
	 * 
	 * @return the min score.
	 */
	public float getMin() {
		return min;
	}
	
	/**
	 * Gets the maximum score.
	 * 
	 * @return the max score.
	 */
	public float getMax() {
		return max;
	}

}
