/**
 * 
 */
package cnh.mongodb.analysis;

import java.util.List;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import cnh.annotator.dictionary.water.Categories;
import cnh.mongodb.DBConstants;
import cnh.mongodb.MongoDBUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

/**
 * Calculates and adds TF_IDF scores to each document in a specified collection.
 * 
 * @author Nick Collier
 */
public class ScorerAnnotator extends JCasAnnotator_ImplBase {

	private String dbName, collectionName;
	private String serverAddress;
	private Integer port;
	
	private Logger logger;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.uima.analysis_component.JCasAnnotator_ImplBase#process(org
	 * .apache.uima.jcas.JCas)
	 */
	@Override
	public final void process(JCas arg0) throws AnalysisEngineProcessException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize
	 * (org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
		logger = aContext.getLogger();
		dbName = (String) aContext.getConfigParameterValue("dbName");
		collectionName = (String) aContext.getConfigParameterValue("collectionName");
		serverAddress = (String) aContext.getConfigParameterValue("serverAddress");
		port = (Integer) aContext.getConfigParameterValue("port");
	}

	private DB initDB() throws AnalysisEngineProcessException {

		try {
			Mongo m = null;
			if (serverAddress == null || serverAddress.isEmpty()) {
				m = new Mongo();
			} else {
				if (port == null) {
					m = new Mongo(serverAddress);
				} else {
					m = new Mongo(serverAddress, port);
				}
			}
			return m.getDB(dbName);

		} catch (Exception e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		super.collectionProcessComplete();
		logger.log(Level.INFO, "Scoring Started");
		DB db = initDB();
		boolean res = MongoDBUtil.authenticate(db, "Enter your username and password for db: " + dbName);
		if (!res)
			throw new RuntimeException("MongoDB authentication failed for database: " + dbName);

		// score using the first category by itself
		WaterCategoryScorer waterScorer = new WaterCategoryScorer(db);
		String primary = Categories.getInstance().getPrimaryCategory();
		// score the water categories
		CategoryScores catScores = waterScorer.createPrimaryCategoryScores(collectionName, primary);
		
		List<String> waterCategories = Categories.getInstance().getCategories(Categories.WATER_CATEGORIES);
		waterCategories.remove(primary);

		// score all the other categories in combination with the first
		String[] others = waterCategories.toArray(new String[]{});
		waterScorer.scoreCollection(collectionName, catScores, others);
		
		LocationScorer locScorer = new LocationScorer(db);
		String[] locations = Categories.getInstance().getCategories(Categories.LOCATION_CATEGORIES).toArray(new String[]{});
		locScorer.scoreCollectionTFIDF(collectionName, locations);
		
		InstitutionScorer instScorer = new InstitutionScorer(db);
		String[] categories = Categories.getInstance().getCategories(Categories.INSTITUTION_CATEGORIES).toArray(new String[]{});
		instScorer.scoreCollectionTFIDF(collectionName, categories);
		
		indexScoreCategories(db);
		Mongo m = db.getMongo();
		m.close();
		logger.log(Level.INFO, "Scoring Finished");
	}

	private void indexScoreCategories(DB db) {
		DBCollection dbCollection = db.getCollection(collectionName);
		DBObject obj = dbCollection.findOne();
		DBObject keys = new BasicDBObject();
		for (String key : obj.keySet()) {
			if (key.contains(DBConstants.TF_IDF) || key.contains(DBConstants.TOPICALITY)) {
				keys.put(key, 1);
			}
		}

		for (String key : keys.keySet()) {
			dbCollection.ensureIndex(key);
		}
	}
}
