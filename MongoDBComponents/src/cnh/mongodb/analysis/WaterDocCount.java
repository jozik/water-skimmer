/**
 * 
 */
package cnh.mongodb.analysis;

import java.util.ArrayList;
import java.util.List;

import cnh.mongodb.DBConstants;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Counts the number of documents that contain water annotations plus
 * optional other water categories.
 * 
 * @author Nick Collier
 */
public class WaterDocCount implements AggregateAnalyzer {
	
	private List<String> categories = new ArrayList<String>();
	private String name;
	
	public WaterDocCount() {
		categories.add("WATER");
	}
	
	public WaterDocCount(String otherCategory) {
		this();
		categories.add(otherCategory);
	}

	/* (non-Javadoc)
	 * @see cnh.mongodb.analysis.AggregateAnalyzer#getAnalysisName()
	 */
	@Override
	public String getAnalysisName() {
		if (name == null) {
			StringBuilder builder = new StringBuilder(categories.get(0));
			for (int i = 1; i < categories.size(); i++) {
				builder.append("_");
				builder.append(categories.get(i));
			}
			builder.append("_DOC_COUNT");
			name = builder.toString();
		}
		
		return name;
	}

	/* (non-Javadoc)
	 * @see cnh.mongodb.analysis.AggregateAnalyzer#run(com.mongodb.DBCollection, com.mongodb.DBObject)
	 */
	@Override
	public double run(DBCollection collection, DBObject query) {
		BasicDBObjectBuilder objBuilder = BasicDBObjectBuilder.start(query.toMap());
		List<BasicDBObject> list = new ArrayList<BasicDBObject>();
		for (String category : categories) {
			BasicDBObject n = new BasicDBObject();
			n.put(DBConstants.DOCUMENT_WATER_KIND, category);
			list.add(n);
		}
		objBuilder.add("$and", list);
		return collection.count(objBuilder.get());
	}
}
