import string


#FEDERALIST No. 1
#
#General Introduction
#For the Independent Journal.
#Saturday, October 27, 1787
#
#HAMILTON


def read_header(lines, index):
    # skip first \n
    for i in range(0, 10):
        line = lines[index]
        if (string.strip(line) != ''):
            break
        index += 1
    
    fed = lines[index]
    count = 0
    for i in range(0, 10):
        line = lines[index]
        index += 1
        if (string.strip(line) == ''):
            count += 1
        if count == 2:
            break;
        
    author = lines[index]
    
    bits = string.split(fed)
    author = string.strip(author)
    author = string.replace(author, ", with ", "_WITH_")
    title = bits[0] + "_" + bits[2] + "_" + author
    print title
    return open("./tmp/" + title + ".txt", 'w')
    

def run():
    doc = open("./tmp/feder10a.txt")
    lines = doc.readlines()
    doc.close()
    index = 0
    file = read_header(lines, index)
    for line in lines:
        if (string.strip(line) == '____'):
            file.close()
            file = read_header(lines, index + 1)
        else:
            file.write(line)
        index += 1
    file.close()
        
        

run()