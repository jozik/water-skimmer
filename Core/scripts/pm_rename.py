# renames promed files according to their id -- assuming their original
# name is ...,id.txt

import os, string

for fname in os.listdir("/Users/nick/tmp/promed"):
    if fname.find("p=") == 0:
        index = fname.rfind(",")
        newname = "ProMedMail_" + fname[index + 1:]
        #print fname
        os.rename("/Users/nick/tmp/promed/" +fname, "/Users/nick/tmp/promed/" + newname)
            