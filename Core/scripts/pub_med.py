import string

ARTICLE_END = "</PubmedArticle>"
ARTICLE_START = "<PubmedArticle>"
HEADER = """<?xml version="1.0"\?>
<!DOCTYPE PubmedArticleSet PUBLIC "-//NLM//DTD PubMedArticle, 1st January 2009//EN" "http://www.ncbi.nlm.nih.gov/entrez/query/DTD/pubmed_090101.dtd">\n"""

def run():
    doc = open("../corpus_source/pubmed_result.txt")
    lines = doc.readlines()
    doc.close()
    do_write = 0
    index = 1
    # skip the file header stuff
    for line in lines[3:]:
        if (string.strip(line) == ARTICLE_START):
            doc = open(("../corpus/pub_med/pub_med_abstract_%d.xml" % index), 'w')
            index = index + 1
            #doc.write(HEADER)
            doc.write(line)
            do_write = 1
            
        elif (string.strip(line) == ARTICLE_END):
            doc.write(line)
            doc.close()
            do_write = 0
        
        elif (do_write):
            doc.write(line)
            
run()