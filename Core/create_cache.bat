SET JAVA="C:\Program Files\Java\jre6\bin\java"
SET MAIN=anl.mifs.CPERunner
SET CP1=./bin;./lib/mifs_core.jar;lib/commons-collections-3.2.1.jar;./lib/log4j-1.2.15.jar;./lib/jdom.jar
SET CP2=./lib/rome-1.0.jar;./lib/lucene-core-2.4.0.jar;./lib/lucene-analyzers-2.4.0.jar;./lib/lucene-snowball-2.4.0.jar
SET CP3=./lib/lucene-queries-2.4.0;./lib/uima-core.jar;./lib/uima-cpe.jar;./lib/semanticvectors-1.16
SET CP4=./lib/commons-vfs-1.0.jar;./lib/htmlparser.jar;./lib/gson-1.3b3.jar;./lib/mailapi.jar;./lib/smtp.jar
SET GATE_LIB1=./gate_app/lib/gate.jar;./gate_app/lib/xercesImpl.jar;./gate_app/lib/nekohtml-1.9.8+2039483.jar
SET GATE_LIB2=./gate_app/lib/PDFBox-0.7.2.jar;./gate_app/lib/ontotext.jar;./gate_app/lib/gate-asm.jar
SET GATE_LIB3=./gate_app/lib/gateHmm.jar;./gate_app/lib/jasper-compiler-jdt.jar;./gate_app/lib/xpp3-1.1.3.3_min.jar;./gate_app/lib/xstream-1.2.jar;./gate_app/lib/commons-lang-2.4.jar

%JAVA% -Xmx640M -cp "%CP1%;%CP2%;%CP3%;%CP4%;%GATE_LIB1%;%GATE_LIB2%;%GATE_LIB3%" %MAIN%