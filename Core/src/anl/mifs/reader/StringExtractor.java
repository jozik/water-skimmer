/**
 * 
 */
package anl.mifs.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.htmlparser.Parser;
import org.htmlparser.beans.StringBean;
import org.htmlparser.util.EncodingChangeException;
import org.htmlparser.util.ParserException;

/**
 * Extracts lines of text and links from a URL and passes them to a
 * DocumentProcessor.
 * 
 * @author Nick Collier
 */
public class StringExtractor {

  private static final String EXCEPTION_STRING = "org.htmlparser.util.EncodingChangeException";

  private StringBean bean;

  /**
   * Creates a StringExtractor that uses the specified bean to extract the text.
   * 
   * @param bean
   *          the bean to use.
   */
  public StringExtractor(StringBean bean) {
    this.bean = bean;
    bean.setCollapse(true);
    bean.setLinks(true);
    bean.setReplaceNonBreakingSpaces(true);
  }

  /**
   * Creates a StringExtractor that uses the default StringBean to extract the
   * text.
   */
  public StringExtractor() {
    this(new StringBean());
  }

  /**
   * Runs this StringExtractor over the specified url. Each extracted string
   * will be passed to the specified DocumentProcessor until all the Strings
   * have been passed or the processor returns done.
   * 
   * @param url
   *          the url to run on
   * @param referer
   *          a link to set in the http REFERER header. This is optional and can
   *          be an empty string
   * @param processor
   *          the processor to pass the extracted strings to
   * 
   * @throws IOException
   *           if there is an error extracting the strings.
   */
  public void run(String url, String referer, DocumentProcessor processor) throws IOException {
    URL theURL = new URL(url);
    HttpURLConnection conn = (HttpURLConnection) theURL.openConnection();
    conn.setRequestProperty("User-Agent", "HTMLParser/1.6");
    conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
    conn.setRequestProperty("REFERER", referer);
    this.run(conn, processor);
  }

  /**
   * Runs this StringExtractor over the specified url connection. Each extracted
   * string will be passed to the specified DocumentProcessor until all the
   * Strings have been passed or the processor returns done.
   * 
   * @param conn
   *          the url connection to run on
   * @param processor
   *          the processor to pass the extracted strings to
   * 
   * @throws IOException
   *           if there is an error extracting the strings.
   */
  public void run(URLConnection conn, DocumentProcessor processor) throws IOException {
    bean.setConnection(conn);
    if (bean.getStrings().startsWith(EXCEPTION_STRING)) {
      throw new IOException(new EncodingChangeException(bean.getStrings().substring(
	  EXCEPTION_STRING.length())));
    }
    processStrings(processor);
  }

  /**
   * Runs this StringExtractor over the specified url. Each extracted string
   * will be passed to the specified DocumentProcessor until all the Strings
   * have been passed or the processor returns done.
   * 
   * @param url
   *          the url to run on
   * @param processor
   *          the processor to pass the extracted strings to
   * 
   * @throws IOException
   *           if there is an error extracting the strings.
   */
  public void run(String url, DocumentProcessor processor) throws IOException {
    bean.setURL(url);
    if (bean.getStrings().startsWith(EXCEPTION_STRING)) {
      throw new IOException(new EncodingChangeException(bean.getStrings().substring(
	  EXCEPTION_STRING.length())));
    }
    processStrings(processor);
  }

  public void runWithManualFetch(String url, DocumentProcessor processor) throws IOException {
    String html = ReaderUtilities.getHTMLAsString(new URL(url));
    Parser parser = new Parser();
    try {
      parser.setInputHTML(html);
      parser.visitAllNodesWith(bean);
      processStrings(processor);
    } catch (ParserException ex) {
      ex.printStackTrace();
      throw new IOException(ex);
    }
  }

  private void processStrings(DocumentProcessor processor) throws IOException {
    BufferedReader reader = new BufferedReader(new StringReader(bean.getStrings()));
    String line;

    while ((line = reader.readLine()) != null && !processor.done()) {
      processor.processLine(line);
    }
    reader.close();
  }
}
