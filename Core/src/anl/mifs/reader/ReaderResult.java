/**
 * 
 */
package anl.mifs.reader;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;

import anl.mifs.types.Date;
import anl.mifs.types.DocumentMetaData;

/**
 * The result of parsing a document with a reader. This makes the
 * parsed metadata available. It also includes some basic error
 * checking.
 * 
 * @author Nick Collier
 */
public class ReaderResult {
  
  public static enum Error {NONE, CONTENTS, DATE, AUTHORS, TITLE, LOCATION, SOURCE};
  
  private ParsedDate date;
  private ParsedAuthors authors;
  private String title, location, link, source, contents;
  
  public ReaderResult() {
    title = location = link = source = contents = "";
  }
  
  public ReaderResult(ReaderResult result) {
    this.title = result.title;
    this.location = result.location;
    this.link = result.link;
    this.source = result.source;
    this.contents = result.contents;
    if (result.date != null) this.date = new ParsedDate(result.date.getJavaDate());
    if (result.authors != null) this.authors = new ParsedAuthors(result.authors);
  }
  
  /**
   * Gets the parsed Date.
   * 
   * @return the parsed Date.
   */
  public ParsedDate getDate() {
    return date;
  }
  
  /**
   * Gets a formatted String representation of this ReaderResult.
   * 
   * @return a formatted String representation of this ReaderResult.
   */
  public String toString() {
    String sDate = date == null ? "null" : date.getJavaDate().toString();
    String sAut = authors == null ? "null" : authors.toString();
    return String.format("ReaderResult:%n\t%s%n\t%s%n\t%s%n\t%s%n\t%s%n\t%s%n", title, location, sDate, sAut, source, contents);
  }
  
  public void setDate(ParsedDate date) {
    this.date = date;
  }

  /**
   * Gets the authors.
   * 
   * @return the authors.
   */
  public ParsedAuthors getAuthors() {
    return authors;
  }

  public void setAuthors(ParsedAuthors authors) {
    this.authors = authors;
  }

  /**
   * Gets the title.
   * 
   * @return the title.
   */
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * Gets the location (e.g. the URL) of the document.
   * @return the location (e.g. the URL) of the document.
   */
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  /**
   * Gets an additional link provided by the document (if available).
   * 
   * @return an additional link provided by the document (if available).
   */
  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  /**
   * Gets the source of the document (e.g. NY Times etc.).
   * 
   * @return the source of the document (e.g. NY Times etc.).
   */
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }
  
  public void setContents(String contents) {
    this.contents = contents;
  }
  
  /**
   * Gets the contents of the document.
   *  
   * @return the contents of the document.
   */
  public String getContents() {
    return contents;
  }
  
  /**
   * Fills in the DocumentMetaData from the results using the
   * specified cas.
   * 
   * @param data the DocumentMetaData to fill in.
   * @param cas the cas to use
   * @return the error status of the result.
   */
  public Error process(DocumentMetaData data, JCas cas) {
    Error error = isValid();
    if (error == Error.NONE) {
      Date uDate = date.toDateType(cas);
      data.setDate(uDate);
      
      FSArray array = authors.toAuthorsType(cas);
      data.setAuthors(array);
      
      data.setLocation(location);
      data.setTitle(title);
      data.setLink(link);
      data.setSource(source);
      
      cas.setDocumentText(contents);
    } 
    return error;
  }
  
  /**
   * Checks whether this ReaderResult is valid.
   * 
   * @return the Error code. If the result is valid then Error.NONE will
   * be returned, otherwise the Error code specifies where there error is. 
   */
  public Error isValid() {
    boolean contentsOK = contents != null && contents.length() > 0;
    if (!contentsOK) {
      return Error.CONTENTS;
    }
    
    boolean titleOK = title != null && title.length() > 0;
    if (!titleOK) {
      return Error.TITLE;
    }
    
    boolean sourceOK = source != null && source.length() > 0;
    if (!sourceOK) {
      return Error.SOURCE;
    }
    
    boolean locationOK = location != null && location.length() > 0;
    if (!locationOK) {
      return Error.LOCATION;
    }
    
    if (date == null || date.getJavaDate() == null) {
      return Error.DATE;
    }
    
    if (authors == null) {
      return Error.AUTHORS;
    }
    return Error.NONE;
  }
}
