package anl.mifs.reader;

import java.io.IOException;
import java.util.Date;

public class GuardianHTMLReader implements ContentReader{
	 // appends as content lines between
	 
	  private class Processor implements DocumentProcessor {

	    private static final int WAITING = 0;
	    private static final int COLLECTING = 1;
	    private static final int STOPPED = 2;
	    
	    private int status = WAITING;

	    public void processLine(String line) {
	      // System.out.println("line = " + line);
	      if (status == COLLECTING && !line.contains("<http:")) {
		if (line.startsWith("Recipient's email address")) {
		  status = STOPPED;
		} else {
		  content.append(line);
		  System.out.println(line);
		  content.append(" ");
		}
	      }

	      if (status == WAITING && line.contains("http://www.guardian.co.uk"))
	    	  status = COLLECTING;
	    }
	    
	    @Override
	    public boolean done() {
	      return status == STOPPED;
	    }
	    
	    public void reset() {
	      status = WAITING;
	    }
	  }

	  private StringExtractor extractor = new StringExtractor();
	  private DocumentProcessor processor = new Processor();
	  private StringBuilder content;
	  private ReaderResult result;

	  /**
	   * Reads the text from the specified URL. The title and date are expected to
	   * be provided from another source, e.g. an RSS feed entry or an archive
	   * "index."
	   * 
	   * @param url
	   *          the url to read from
	   * @param title
	   *          the article title
	   * @param date
	   *          the date of the article
	   * @return the result of reading and parsing the text at the url
	   * @throws IOException
	   *           if there is an error while reading
	   */
	  public ReaderResult read(String url, String title, Date date, String author) throws IOException {
	    result = new ReaderResult();
	    content = new StringBuilder();

	    result.setTitle(title);
	    result.setDate(new ParsedDate(date));
	    result.setLocation(url);
	    result.setSource(ReaderConstants.GUARDIAN_SOURCE);
	    result.setLink("");

	    ParsedAuthors authors = new ParsedAuthors();
	    authors.addName("", author, "");
	    result.setAuthors(authors);

	    processor.reset();
	    extractor.run(url, processor);
	    result.setContents(content.toString());

	    return result;
	  }
}
