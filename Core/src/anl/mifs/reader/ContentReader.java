/**
 * 
 */
package anl.mifs.reader;

import java.io.IOException;
import java.util.Date;

/**
 * Interface for classes that read / parse content
 * from a url and return a ReaderResult.
 * 
 * @author Nick Collier
 */
public interface ContentReader {
  
  /**
   * Reads the content from the url. The title
   * etc. are passed in and should be returned in
   * the ReaderResult.
   * 
   * @param url the url to read
   * @param title the document title
   * @param date the document'd ate
   * @param author the author (can be empty string)
   * @return the ReaderResult
   * @throws IOException if there is an error reading the content
   */
  ReaderResult read(String url, String title, Date date, String author) throws IOException;

}
