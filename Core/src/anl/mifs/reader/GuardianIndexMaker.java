package anl.mifs.reader;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class GuardianIndexMaker {
	private static String START_DATE="2008-2-1";
	private static String END_DATE="2008-2-2";
	static int timeOfYear;
	static String timeOfDay;
	static String timeOfMonth;
//	static String category;
    private static boolean have=true;
    static int countHave=0;
    public DateFormat format = new SimpleDateFormat("yyyy-M-d");
    String[] monthsI={"01","02","03","04","05","06","07","08","09","10","11","12"};
    String[] days={"01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16",
    		"17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"};
	int[]year;
	public long totalTicks;
	public Date start;
	public Date end;
	
	public GuardianIndexMaker(){
	}
	  private static class ArchiveMaker implements DocumentProcessor {

		    // timestamp:::link:::title:::author
          
		    private PrintWriter writer;
		    private String month = "";
		    public long timeStamp;
		    public ArchiveMaker(String file) throws IOException {
		      writer = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		    }

		    public void processLine(String line) {
		     
		        if(line.contains("/science/")|| line.contains("/world/")|| line.contains("/uk/")
		        		|| line.contains("/news/")) {
		        
		        	if (line.startsWith("<http://www.guardian.co.uk/")&& line.contains(Integer.toString(timeOfYear))&&line.contains(timeOfDay)) {
		        		have=true;
		        		int index = line.indexOf(">");
		        		String link = line.substring(1, index);
		        		index = line.indexOf(Integer.toString(timeOfYear)+"/");
		        		int start = index + 5;
		        		int end = start + 3;
		        		String linkMonth = line.substring(start, end);
		        		start = end + 1;
		        		end = start + 2;
	//	        		String day = line.substring(start, end);
		        		System.out.println(line);
		        		String title = "";
		        		start = line.indexOf(">") + 1;
		        		end = line.indexOf(" By<");
		        		if (end != -1) {
		        			title = line.substring(start, end);
		        		} else {
		        			end = line.length();
		        			title = line.substring(start, end);
		        		}
		        	
		        		if (!month.equals(linkMonth)) {
		        			//writer.println(String.format("%s 2009", linkMonth));
		        			month = linkMonth;
		        		}
		        		writer.println(String.format("%d:::%s:::%s:::", timeStamp, link, title));
	//				 System.out.printf(String.format("%d:::%s:::%s:::", timeStamp, link, title));
	//	        		countHave=+1;
				      }
		        }
		        if(countHave>0)
		        	have=true;
		        else
		        	have=false;
		}


		    @Override
		    public boolean done() {
		      return false;
		    }

		    public void close() throws IOException {
		      writer.close();
		    }

		    /*
		     * (non-Javadoc)
		     * 
		     * @see anl.mifs.reader.DocumentProcessor#reset()
		     */
		    @Override
		    public void reset() {
		      // TODO Auto-generated method stub

		    }
		  }
		  
	  public void parseTime() throws ParseException{
			start = format.parse(START_DATE);
		    end = format.parse(END_DATE);
		    long days=end.getTime()-start.getTime();
		    totalTicks=(days/86400000)+1;
		}

	public static void main(String[] args) throws IOException, ParseException {
		GuardianIndexMaker gim = new GuardianIndexMaker();
		StringExtractor extractor = new StringExtractor();
		ArchiveMaker maker = new ArchiveMaker("./guardian_archive_index.txt");
		String[] months = { "jan","feb","mar", "apr", "may","jun",
				"jul","aug","sep","oct","nov","dec" };
		gim.parseTime();

		Calendar cal = GregorianCalendar.getInstance();
		int current=0;
		cal.setTime(gim.start);
		   while(current<gim.totalTicks) {
		    
			  timeOfYear=cal.get(Calendar.YEAR);
		    	  
		      maker.timeStamp = cal.getTime().getTime();
	
		      int tens=0;
		      while(tens<310) {
		    	  countHave=0;
		    	  int day=cal.get(Calendar.DAY_OF_MONTH)-1;
		    	  String url = "http://browse.guardian.co.uk/search/all?lDim=N%3D3092%2B3332%2B3341&month="
		    		  +gim.monthsI[cal.get(Calendar.MONTH)]+"&No="+tens+"&day="
    		  		+gim.days[day]+"&year="+cal.get(Calendar.YEAR);
		    	  timeOfMonth=months[cal.get(Calendar.MONTH)];
		    	  timeOfDay=Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
		    	  System.out.println(timeOfDay);
		    	  System.out.println("Processing URL: " + url);
		    	  extractor.run(url, maker);
		    	  tens+=10;
		      }
		      have=true;
		      cal.add(Calendar.DAY_OF_MONTH, 1);
		      current++;
		   }
		    maker.close();
		  } 
	
}
