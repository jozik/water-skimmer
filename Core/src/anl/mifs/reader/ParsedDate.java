package anl.mifs.reader;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.uima.jcas.JCas;

/**
 * Encapsulates a Date metadata retrieved from a
 * corpus document. 
 * 
 * @author Nick Collier
 */
public class ParsedDate {
  
  private static Calendar cal = GregorianCalendar.getInstance();
  
  private Date date;
  
  public ParsedDate(Date date) {
    this.date = date;
  }
  
  /**
   * Gets the date as a Java Date.
   * 
   * @return the date as a Java Date.
   */
  public Date getJavaDate() {
    return date;
  }
  
  /**
   * Creates an mifs UIMA Date type from the
   * encapsulated date.
   * 
   * @param cas the JCas to create the Date type with
   * @return the created Date type.
   */
  public anl.mifs.types.Date toDateType(JCas cas) {
    anl.mifs.types.Date date = new anl.mifs.types.Date(cas);
    cal.setTime(this.date);
    date.setDay(cal.get(Calendar.DAY_OF_MONTH));
    date.setMonth(cal.get(Calendar.MONTH) + 1);
    date.setYear(cal.get(Calendar.YEAR));

    return date;
  }
}
