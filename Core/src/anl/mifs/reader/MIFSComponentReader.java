/**
 * 
 */
package anl.mifs.reader;

import java.io.IOException;
import java.util.Date;

import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.resource.ConfigurableResource;
import org.apache.uima.resource.ResourceInitializationException;

/**
 * Interface for classes that are components in a MIFSCompositeReader.
 * The CompositeReader delegates the reading to its individual components.
 * 
 * @author Nick Collier
 */
public interface MIFSComponentReader {
  

  final static String MESSAGE_DIGEST = "anl.mifs.reader.Reader_Messages";
  
  /**
   * Initializes this MIFSComponentReader, optionally
   * using the resource.
   * 
   * @param resource the resource to use for configuration
   * @throws ResourceInitializationException if there is an error initializing 
   * the reader
   */
  void initialize(ConfigurableResource resource, Date cacheDateDate) throws ResourceInitializationException;
  
  /**
   * Gets the next document etc. into the CAS.
   * 
   * @param cas the CAS to put the document into
   * 
   * @throws IOException if there is an error reading the document
   * @throws CollectionException if there is an error reading the document
   */
  void getNext(CAS cas) throws IOException, CollectionException;
  
  /**
   * Gets whether or not this MIFSComponentReader has
   * more documents to process.
   * 
   * @return true if done, otherwise false.
   * @throws IOException if there is an error in determining if there are more docs to process.
   * @thros CollectionException if there is an error in determining if there are more docs to process.
   */
  boolean hasNext() throws IOException, CollectionException;
  
  /**
   * Closes this MIFSComponentReader and any resources it 
   * may have opened.
   * 
   * @throws IOException if there is an error closing the reader.
   */
  void close() throws IOException;

}
