package anl.mifs.reader;

import java.io.IOException;

import anl.mifs.util.MIFSException;

/**
 * Processes a {@link ReaderResult} is someone implementor
 * specific way. For example, an implemenation might add the
 * results to a lucene index, or add them to a JCas etc. 
 * 
 * @author Nick Collier
 */
public interface ReaderResultProcessor {
  
  /**
   * Initializes the processor.
   * 
   * @throws MIFSException
   */
  void initialize() throws MIFSException;
  
  /**
   * Process the specified result.
   * 
   * @param result the result to process
   * @throws MIFSException if there is an error processing the result
   */
  void process(ReaderResult result) throws MIFSException;
  
  /**
   * Notifies the processor that it is done processing results.
   * 
   * @throws MIFSException
   */
  void done() throws IOException;

}
