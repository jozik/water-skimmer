package anl.mifs.reader;

import java.util.ArrayList;
import java.util.List;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.cas.FSArray;

import anl.mifs.types.Name;

/**
 * Encapsulates author info as returned by a 
 * document reader. 
 * 
 * @author Nick Collier
 */
public class ParsedAuthors {
  
  List<String[]> names = new ArrayList<String[]>();
  
  public ParsedAuthors() {}
 
  public void addName(String first, String last, String mi) {
    String[] nameArray = new String[3];
    nameArray[0] = last;
    nameArray[1] = first;
    nameArray[2] = mi;
    names.add(nameArray);
  }
  
  public ParsedAuthors(ParsedAuthors authors) {
    for (String[] name : authors.names) {
      String[] nameArray = new String[3];
      nameArray[0] = name[0];
      nameArray[1] = name[1];
      nameArray[2] = name[2];
      this.names.add(nameArray);
    }
  }
  
  /**
   * Gets a String representation of the authors.
   * 
   * @return a String representation of the authors.
   */
  public String getAuthors() {
    StringBuffer buf = new StringBuffer();
    int i = 0;
    int j = 0;
    for (String[] nameArray : names) {
      if (j > 0) buf.append("; ");
      for (String name : nameArray) {
	if (i == 1 && name.length() > 0) buf.append(", ");
	buf.append(name);
	i++;
      }
      j++;
      i = 0;
    }
    return buf.toString();
  }
  
  /**
   * Creates an FSArray UIMA type of Name types from the 
   * author info.
   * 
   * @param cas the JCas to use in creating the type
   * @return the created array.
   */
  public FSArray toAuthorsType(JCas cas) {
    FSArray fsArray = new FSArray(cas, names.size());
    for (int i = 0, n = names.size(); i < n; i++) {
      String[] nameArray = names.get(i);
      Name name = new Name(cas);
      name.setFirstName(nameArray[1]);
      name.setLastName(nameArray[0]);
      name.setInitials(nameArray[2]);
      fsArray.set(i, name);
    }
    
    return fsArray;
  }

}
