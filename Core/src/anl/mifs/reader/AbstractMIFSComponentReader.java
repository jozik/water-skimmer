/**
 * 
 */
package anl.mifs.reader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ConfigurableResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;

import anl.mifs.types.DocumentMetaData;
import anl.mifs.util.IndexIterator;
import anl.mifs.util.MIFSException;
import anl.mifs.util.StandardIndexLineParser;

/**
 * Abstract implementation of MIFSComponentReader that processes a 
 * ReaderResult into a CAS by iterating over a "standard" index and
 * retreiving the results. Subclasses are responsible for producing the 
 * ReaderResult. 
 * 
 * @author Nick Collier
 */
public abstract class AbstractMIFSComponentReader implements MIFSComponentReader {
  
  protected IndexIterator indexIter;
  protected String[] lineItems = new String[4];
  protected String location = "";
  
  protected List<ReaderResultProcessor> processors = new ArrayList<ReaderResultProcessor>();
  protected Date startDate;
  
  /* (non-Javadoc)
   * @see anl.mifs.reader.MIFSComponentReader#getNext(org.apache.uima.cas.CAS)
   */
  @Override
  public void getNext(CAS cas) throws IOException, CollectionException {
    JCas jcas;
    try {
      jcas = cas.getJCas();
      ReaderResult result = getNextResult();
      ReaderResult.Error error = result.isValid();
      if (error != ReaderResult.Error.NONE) {
    	  CollectionException ex = new CollectionException(MESSAGE_DIGEST, "reader_error", new Object[] {
    			  getDocumentURL(), error.toString() });
    	  UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", ex);
    	  throw ex;
      }
      
      DocumentMetaData data = new DocumentMetaData(jcas, 0, result.getContents().length());
      result.process(data, jcas);
      data.addToIndexes();
      
      for (ReaderResultProcessor proc: processors) {
	proc.process(result);
      }
      
    } 
     catch(Throwable t){
    	 UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", t);
    	 t.printStackTrace();
     }
/*     catch (CASException e) {
      UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", e);
      throw new CollectionException(e);
    } catch (MIFSException ex) {
      UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", ex);
      throw new CollectionException(ex);
*/
    /*
    }
    catch (IOException ex) {
      UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", ex);
      throw ex;
    } catch (CollectionException ex) {
      UIMAFramework.getLogger(getClass()).log(Level.WARNING, "Error reading url: '" + getDocumentURL() + "'", ex);
      throw ex;
    */
      
     finally {
      postNext();
    }
  }
  
  /**
   * Checks if the date is before the cache start date and logs a warning.
   * 
   * @param date the date to check
   */
  protected void checkDate(Date date) {
    if (date.before(startDate)) {
      UIMAFramework.getLogger(getClass()).log(Level.WARNING, String.format("Document %s with timestamp %s is prior to start date %s", 
	  location, date.toString(), startDate.toString()));
    }
  }
  
  /**
   * Gets the next ReaderResult.
   * 
   * @return the next ReaderResult.
   * @throws IOException if there is an error getting the result
   */
  protected abstract ReaderResult getNextResult() throws IOException;
  
  /**
   * Gets the URL of the current document.
   * 
   * @return the URL of the current document.
   */
  protected String getDocumentURL() {
    return location;
  }
  
  /**
   * Called at the completion of {@link #getNext(CAS)}
   * 
   * @throws IOException if there is an error getting the result
   */
  protected void postNext() throws IOException {}

  /* (non-Javadoc)
   * @see anl.mifs.reader.MIFSComponentReader#close()
   */
  @Override
  public void close() throws IOException {
    for (ReaderResultProcessor proc : processors) {
	proc.done();
      } 
  }

  /* (non-Javadoc)
   * @see anl.mifs.reader.MIFSComponentReader#hasNext()
   */
  @Override
  public boolean hasNext() throws IOException, CollectionException {
    return indexIter.hasNext();
  }
  
  /**
   * Gets the name of the parameter key for the index file. The index file
   * contains the links etc to read.
   * 
   * @return the name of the parameter key for the index file.
   */
  protected abstract String getIndexFileKey();
  
  /* (non-Javadoc)
   * @see anl.mifs.reader.MIFSComponentReader#initialize(org.apache.uima.resource.ConfigurableResource)
   */
  @Override
  public void initialize(ConfigurableResource resource, Date cacheStartDate) throws ResourceInitializationException {
    this.startDate = cacheStartDate;
    String indexFile = (String)resource.getConfigParameterValue(getIndexFileKey());
    
    UIMAFramework.getLogger(getClass()).log(Level.INFO, String.format("%s using index file %s", getClass(), indexFile));
    
    try {
      indexIter = new IndexIterator(indexFile);
    } catch (IOException ex) {
      throw new ResourceInitializationException(MESSAGE_DIGEST, "reading_index_error", new Object[] {indexFile}, ex);
    }
  }
}
