package anl.mifs.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.htmlparser.lexer.Stream;

/**
 * Reader related utility methods.
 * 
 * @author Nick Collier
 */
public class ReaderUtilities {

  /**
   * Gets the contents of the url as a String. Assumes the charset is
   * ISO-8859-1.
   * 
   * @param url
   *          the url whose content we get
   * @return the contents of the url as a string
   * 
   * @throws IOException
   *           if there is an error getting the url content
   */
  public static String getHTMLAsString(URL url) throws IOException {
    HttpURLConnection.setFollowRedirects(true);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestProperty("User-Agent", "HTMLParser/1.6");
    conn.setRequestProperty("Accept-Encoding", "gzip, deflate");
    // conn.setRequestProperty("REFERER", referer);
    // String type = conn.getHeaderField("Content-Type");
    String charset = "ISO-8859-1";

    String encoding = conn.getContentEncoding();
    InputStream stream = conn.getInputStream();
    if (encoding != null) {
      if (encoding.indexOf("gzip") != -1) {
	stream = new GZIPInputStream(stream);
      } else if (encoding.indexOf("deflate") != -1) {
	stream = new Stream(new InflaterInputStream(stream, new Inflater(true)));
      }
    }

    BufferedReader reader = new BufferedReader(new InputStreamReader(stream, charset));
    StringBuilder builder = new StringBuilder();
    String line = "";

    while ((line = reader.readLine()) != null) {
      builder.append(line);
      builder.append("\n");
    }
    reader.close();
    return builder.toString();
  }

  /**
   * Tests the specified URL for redirection and if its been redirected then
   * follows the redirect until it finds a non 301 URL.
   * 
   * @param urlString
   * @return
   * @throws IOException
   */
  public static URL getRedirectedURL(String urlString) throws IOException {
    HttpURLConnection.setFollowRedirects(false);
    URL url = new URL(urlString);
    try {
      boolean done = false;
      String cookie = null;

      while (!done) {
	HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	if (cookie != null)
	  conn.setRequestProperty("Cookie", cookie);

	conn.connect();
	int response = conn.getResponseCode();
	if (response == 301 || response == 302) {
	  url = new URL(conn.getHeaderField("Location"));
	  done = response != 301;
	} else {
	  done = true;
	}
	conn.disconnect();
      }
    } finally {
      // set this back to the default
      HttpURLConnection.setFollowRedirects(true);
    }

    return url;

  }
}
