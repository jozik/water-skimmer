package anl.mifs.reader;

/**
 * Constants used by the various readers.
 * 
 * @author Nick Collier
 */
public interface ReaderConstants {
  
  String PRO_MED_SOURCE = "ProMed Mail";
  String NTARC_SOURCE = "NTARC";
  String CHICAGO_TRIBUNE_SOURCE = "Chicago Tribune";
  String NYTIMES_SOURCE = "New York Times";
  String ALL_AFRICA_SOURCE = "All Africa";
  String LATIMES_SOURCE="Los Angeles Times";
  String GUARDIAN_SOURCE="Guardian";
  String INDEX_LINE_DELIMETER = ":::";
  String TELEGRAPH_SOURCE = "Telegraph";
  String INDEPENDENT_SOURCE ="Independent";
}
