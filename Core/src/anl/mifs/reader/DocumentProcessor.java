/**
 * 
 */
package anl.mifs.reader;

/**
 * Interface for classes that can process lines of text, such as
 * those extracted from an html page.
 * 
 * @author Nick Collier
 */
public interface DocumentProcessor {

  /**
   * Process the line. The line may be text or a html link.
   * 
   * @param line the line to process
   */
  void processLine(String line);
  
  /**
   * If this returns true, the code that is feeding lines to this
   * processor can then stop.
   * 
   * @return true when the processor is done, otherwise false.
   */
  boolean done();
  
  /**
   * Resets the processor so it may be used with another document.
   */
  void reset();

}
