package anl.mifs.reader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * ContentReader that reads cached content.
 * 
 * @author Nick Collier
 */
public class CachedContentReader implements ContentReader {

  private String source, dir;

  /**
   * Creates a CachedContentReader that will produce ReaderResults with the
   * specified source property.
   * 
   * @param source
   *          the content source name
   * @param cacheDir
   *          the top level cached content directory.
   */
  public CachedContentReader(String source, String cacheDir) {
    this.source = source;
    this.dir = cacheDir + "/";
  }

  private void readContents(String file, ReaderResult result) throws IOException {
    int index = file.indexOf("!");
    String zipFile = file.substring(0, index);
    String entry = file.substring(index + 1, file.length());

    ZipEntry zipEntry = null;
    ZipInputStream stream = new ZipInputStream(new BufferedInputStream(new FileInputStream(dir
	+ zipFile)));
    while ((zipEntry = stream.getNextEntry()) != null) {
      if (zipEntry.getName().equals(entry)) {
	BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
	String location = reader.readLine();
	result.setLocation(location);
	StringBuilder contents = new StringBuilder();
	String line = null;
	while ((line = reader.readLine()) != null) {
	  contents.append(line);
	  contents.append("\n");
	}
	result.setContents(contents.toString());
      }
    }

    stream.close();
  }

  /**
   * Reads the text content from the url and uses that as the reader result
   * contents. No parsing is done. The url is assumed to contain a cached
   * content file whose first line is the true or original url from which the
   * article originated.
   * 
   * @param url
   *          the content url
   * @param title
   *          the title of the article
   * @param date
   *          the date of the article
   * @return
   * @throws IOException
   *           if there is an error
   */
  @Override
  public ReaderResult read(String url, String title, Date date, String author) throws IOException {

    ReaderResult result = new ReaderResult();

    result.setTitle(title);
    result.setDate(new ParsedDate(date));
    result.setSource(source);

    ParsedAuthors authors = new ParsedAuthors();
    authors.addName("", author, "");
    result.setAuthors(authors);
    readContents(url, result);

    return result;
  }
}