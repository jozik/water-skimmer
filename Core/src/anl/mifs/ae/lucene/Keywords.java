/**
 * 
 */
package anl.mifs.ae.lucene;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import anl.mifs.util.AIDAUtils;

/**
 * Manages keywords, keyword sets and keyword fields (a lucene field associated
 * with a particular keyword set).
 * 
 * @author Nick Collier
 */
public class Keywords {

  private Map<String, Set<String>> keywordMap = new HashMap<String, Set<String>>();
  private List<String> fields = new ArrayList<String>();

  /**
   * Adds the specified field name as a keyword field.
   * 
   * @param fieldName
   *          the name of the keyword field
   */
  public void addKeywordField(String fieldName) {
    fields.add(fieldName);
  }

  /**
   * Gets an iterable over the lucene field names.
   * 
   * @return an iterable over the lucene field names.
   */
  public Iterable<String> keywordFields() {
    return fields;
  }
  
  /**
   * Gets the keywords in the their term format.
   * The term format is the lower-cased canonical 
   * format.
   * 
   * @return  the keywords in the their term format.
   */
  public Set<String> getKeywordsAsTerms() {
    Set<String> tmp = new HashSet<String>();
    for (Set<String> set : keywordMap.values()) {
      for (String word : set) {
	tmp.add(word.toLowerCase());
      }
    }
    
    return tmp;
  }

  /**
   * Clears this Keywords of any keywords and keyword sets.
   */
  public void clearKeywords() {
    keywordMap.clear();
  }

  /**
   * Adds the specified keyword to the specified set. The keyword should be the
   * canonical name. This is then lower cased to create a the keyword term.
   * 
   * @param keywordSet
   *          the name of the keyword set
   * @param keyword
   *          the keyword itself
   */
  public void addKeyword(String keywordSet, String keyword) {
    Set<String> set = keywordMap.get(keywordSet);
    if (set == null) {
      set = new HashSet<String>();
      keywordMap.put(keywordSet, set);
    }
    set.add(keyword);
  }

  public void writeXML(PrintWriter out) {
    out.printf("\t<keywords>%n");
    for (String key : keywordMap.keySet()) {
      String str = key;
      str = str.replace("&", "&amp;");
      out.printf("\t\t<keyword_set name=\"%s\">%n", str);
      Set<String> keywords = keywordMap.get(key);
      for (String keyword : keywords) {
	out.printf("\t\t\t<keyword>%n");
	out.printf("\t\t\t\t<term>%s</term>%n", AIDAUtils.toInternalFormat(keyword));
	out.printf("\t\t\t\t<label>%s</label>%n", keyword);
	out.printf("\t\t\t</keyword>%n");
      }
      out.printf("\t\t</keyword_set>%n");
    }
    out.printf("\t</keywords>%n");
  }
}
