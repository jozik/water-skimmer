package anl.mifs.core;

/**
 * Callback type predicate that can be applied to a ScoredTermsCollection.
 *
 * @author Nick Collier
 */
public interface ScoredDocumentCallback {

  /**
   * Called when a new document is about to be processed.
   *
   * @param docIndex the index of the document in the collection
   * @param document the document itself.
   */
  void newDocument(int docIndex, MIFSDocument document);

  /**
   * Provides the score / frequency for the specified term for the document
   * specified in the last newDocument call.
   *
   * @param termIndex the index of the term in the collection
   * @param term      the term itself
   * @param frequency the frequency of the term in the doc
   * @param score     the terms score in the document
   */
  void apply(int termIndex, MIFSTerm term, int frequency, float score);


}
