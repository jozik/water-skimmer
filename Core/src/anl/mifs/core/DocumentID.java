package anl.mifs.core;

/**
 * Encapsulates a Documents STC id and its path.
 *
 * @author Nick Collier
 */
public class DocumentID {

  private int stcIndex;
  private String path;
  private int hashCode;

  public DocumentID(int stcIndex, String path) {
    this.stcIndex = stcIndex;
    this.path = path;
    hashCode = 17;
    hashCode = 31 * hashCode + path.hashCode();
    hashCode = 31 * hashCode + stcIndex;
  }

  public int getSTCIndex() {
    return stcIndex;
  }

  public String getPath() {
    return path;
  }

  public boolean equals(Object obj) {
    if (obj instanceof DocumentID) {
      DocumentID other = (DocumentID) obj;
      return stcIndex == other.stcIndex && path.equals(other.path);
    }

    return false;
  }

  public String toString() {
    return stcIndex + " - " + path;
  }

  public int hashCode() {
    return hashCode;
  }
}

