package anl.mifs.core;

/**
 * Interface for classes that implement a match on a document id.
 *
 * @author Nick Collier
 */
public interface DocumentIDMatcher {

  /**
   * @param id   the id of the document
   * @param path the path of the documeny
   * @return true if there is a match otherwise false.
   */
  public boolean match(int id, String path);
}
