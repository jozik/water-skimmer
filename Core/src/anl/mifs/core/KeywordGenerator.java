package anl.mifs.core;

import anl.mifs.util.HighestTermScores;
import anl.mifs.util.MIFSException;

/**
 * @author Nick Collier
 *         Date: Feb 9, 2009 9:55:50 AM
 */
public interface KeywordGenerator {
  /**
   * Generates and returns the specified number of keywords for the document
   * identified by the specified path.
   *
   * @param docPath the path of the document to generate words for
   * @param count   the number of keywords to generate
   * @return the collection of HighestTermScores
   * @throws anl.mifs.util.MIFSException if there is an error generating the keywords
   */
  HighestTermScores generate(String docPath, int count) throws MIFSException;

  /**
   * Generates the specified number of keywords that are most similar
   * to the specified vector.
   *
   * @param vector the vector to search against
   * @param count  the number of terms to return
   * @return the found keywords.
   */
  HighestTermScores generate(float[] vector, int count);
}
