package anl.mifs.core;

/**
 * Term interface for terms in a ScoredTermsCollection. The
 * term data is for the collection in which the term is part.
 *
 * @author Nick Collier
 */
public interface MIFSTerm {

  /**
   * Gets the number of documents this term appears in.
   *
   * @return number of documents this term appears in.
   */
  int getDocumentCount();

  /**
   * Gets the minimum score of this term across all
   * documents in the collection.
   *
   * @return the minimum score of this term across all
   *         documents.
   */
  float getMinScore();

  /**
   * Gets the maximum score of this term across all
   * documents in the collection.
   *
   * @return the maximum score of this term across all
   *         documents.
   */
  float getMaxScore();

  /**
   * Gets the minimum frequency count of this term across all
   * documents in the collection.
   *
   * @return the minimum frequency count of this term across all
   *         documents.
   */
  int getMinFrequency();

  /**
   * Gets the maximum frequency count of this term across all
   * documents in the collection.
   *
   * @return the maximum frequency count of this term across all
   *         documents.
   */
  int getMaxFrequency();

  /**
   * Gets the text of this term.
   *
   * @return the text of this term.
   */
  String getText();

}
