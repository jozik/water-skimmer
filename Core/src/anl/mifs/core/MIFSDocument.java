package anl.mifs.core;

import java.net.URL;

/**
 * Encapsulates Document info. MIFSDocument's are
 * considered equal if they have the same path.
 *
 * @author Nick Collier
 */
public class MIFSDocument {

  private String title;
  private DocumentID id;

  public MIFSDocument(int stcIndex, String title, URL path) {
    this.title = title;
    this.id = new DocumentID(stcIndex, path.toExternalForm());
  }

  public DocumentID getId() {
    return id;
  }

  /**
   * Gets the index of this document in its ScoredTermsCollection.
   *
   * @return the index of this document in its ScoredTermsCollection.
   */
  public int getSTCIndex() {
    return id.getSTCIndex();
  }

  /**
   * Gets the title of this document.
   *
   * @return the title of this document.
   */
  public String getTitle() {
    return title;
  }

  /**
   * Gets the path of this document.
   *
   * @return the path of this document.
   */
  public String getPath() {
    return id.getPath();
  }

  public boolean equals(Object obj) {
    return (obj instanceof MIFSDocument && id.equals(((MIFSDocument) obj).id));
  }

  public int hashCode() {
    return id.hashCode();
  }
}
