package anl.mifs.core;

import anl.mifs.util.MIFSException;

/**
 * Interface for classes that want to be notified of
 * "events" while iterating through a ScoredTermsCollection
 * in term order.
 *
 * @author Nick Collier
 */
public interface ScoredTermCallback {

  /**
   * Called when a new term is encountered during
   * iteration.
   *
   * @param termIndex the index of the term in the collection
   * @param term      the term itself
   */
  void newTerm(int termIndex, MIFSTerm term);

  /**
   * Notifies the implementor of a document score for the
   * term specified in the last call to newTerm.
   *
   * @param docIndex  the index of the document for which this is the term score
   * @param doc       the document itself
   * @param frequency the frequency count of the term in the document
   * @param score     the term score for the document
   * @throws MIFSException if there is an error during the callback.
   */
  void apply(int docIndex, MIFSDocument doc, int frequency, float score) throws MIFSException;
}
