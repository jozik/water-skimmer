package anl.mifs.core;

import anl.mifs.util.DateRange;
import anl.mifs.util.IntIterator;
import anl.mifs.util.MIFSException;

/**
 * Interface for a collection of scored terms.
 *
 * @author Nick Collier
 */
public interface ScoredTermsCollection extends Iterable<MIFSDocument> {

  /**
   * Gets an iterable over all the terms in this collection.
   *
   * @return an iterable over all the terms in this collection.
   */
  Iterable<MIFSTerm> terms();

  /**
   * Gets an iterable over all the documents in this collection.
   *
   * @return an iterable over all the documents in this collection.
   */
  Iterable<MIFSDocument> documents();


  /**
   * Iterates through the collection in document order calling
   * the relevant methods in the callback. In document order,
   * each document is processed in order such that all the term
   * scores for that document are passed to the callback before
   * proceeding to the next document.
   *
   * @param callback methods are called on this obj during iteration
   */
  void iterateByDocument(ScoredDocumentCallback callback);

  /**
   * Iterates through the collection in term order calling
   * the relevant methods in the callback. In term order, all
   * the scores for all the documents for a specific term are
   * passed to the callback before starting the next term.
   *
   * @param callback methods are called on this obj during iteration
   * @throws MIFSException if there is during the callback.
   */
  void iterateByTerm(ScoredTermCallback callback) throws MIFSException;


  /**
   * Gets the number of terms in the collection.
   *
   * @return the number of terms in the collection.
   */
  int getTermCount();

  /**
   * Gets the number of documents in the collection.
   *
   * @return the number of documents in the collection.
   */
  int getDocumentCount();

  /**
   * Gets the document at the specified index.
   *
   * @param index the index of the document
   * @return Gets the document at the specified index.
   */
  MIFSDocument getDocument(int index);

  /**
   * Gets the term at the specified index.
   *
   * @param index the index of the term
   * @return the term at the specified index.
   */
  MIFSTerm getTerm(int index);

  /**
   * Gets the index for the specified term. This can 
   * be slow.
   *
   * @param term the term to get the index for
   * @return the index for the specified term, or -1 if the term
   *         is not found.
   */
  int getTermIndex(String term);

  /**
   * Gets the frequency of the specified term in the specified document.
   *
   * @param docIndex  the index of the document
   * @param termIndex the index of the term
   * @return the frequency of the specified term in the specified document.
   */
  int getFrequency(int docIndex, int termIndex);

  /**
   * Gets the score of the specified term in the specified document.
   *
   * @param docIndex  the index of the document
   * @param termIndex the index of the term
   * @return the score of the specified term in the specified document.
   */
  float getScore(int docIndex, int termIndex);

  /**
   * Gets an iterator of document indices of docs that
   * contain a Term with the specified index.
   *
   * @param termIndex the term index
   * @return n iterator of document indices of docs that
   *         contain a Term with the specified index.
   */
  IntIterator getDocumentsFor(int termIndex);

  /**
   * Gets the date range (inclusive) covered by this
   * ScoredTermsCollection.
   *
   * @return the date range (inclusive) covered by this
   *         ScoredTermsCollection.
   */
  DateRange getRange();


}
