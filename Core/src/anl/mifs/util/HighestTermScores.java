package anl.mifs.util;

import java.util.*;

/**
 * Container that can be used to gather X
 * number of terms with the highest scores.
 *
 * @author Nick Collier
 */
public class HighestTermScores {

  private Map<String, Float> vals = new HashMap<String, Float>();
  private int limit;
  private float minScore = Float.POSITIVE_INFINITY;
  private String minTerm = null;

  /**
   * Creates a HighestTermScores of specified size.
   * The number of ranked terms will be <= to this size.
   *
   * @param size the size of the HighestTermScores collection.
   */
  public HighestTermScores(int size) {
    limit = size;
  }

  /**
   * Gets the true size of this HighestTermScores.
   *
   * @return the true size of this HighestTermScores.
   */
  public int size() {
    return vals.size();
  }

  /**
   * Adds the specified term and score to this HighestTermScores.
   * If the term and score are added, the returns to true, otherwise false.
   * A term will not be added if this HighestTermScores is at its size limit
   * and the score is less than or equal to the currently stored terms.
   *
   * @param term  the term to add
   * @param score the score of the term to add
   * @return true if the term is added, otherwise false.
   */
  public boolean add(String term, float score) {
    Float val = vals.get(term);
    if (val != null) {
      if (val < score) {
        vals.put(term, score);
        return true;
      } else {
        return false;
      }
    }

    if (vals.size() < limit) {
      vals.put(term, score);
      if (score < minScore) {
        minTerm = term;
        minScore = score;
      }
      return true;
    }

    if (score < minScore) return false;

    // score > min score and is not a duplicate
    // remove the minTerm, add the new one
    // and find the new min score, min term.
    vals.remove(minTerm);
    vals.put(term, score);
    minScore = Float.POSITIVE_INFINITY;
    for (Map.Entry<String, Float> entry : vals.entrySet()) {
      float entryScore = entry.getValue().floatValue();
      if (entryScore < minScore) {
        minScore = entryScore;
        minTerm = entry.getKey();
      }
    }
    return true;
  }

  /**
   * Gets an iterable over the term scores in this HighestTermScores.
   * The order of the iteration is from lowest to highest.
   *
   * @return an iterable over the term scores in this HighestTermScores.
   */
  public Iterable<TermScore> termScores() {
    List<TermScore> scores = new ArrayList<TermScore>();
    for (Map.Entry<String, Float> entry : vals.entrySet()) {
      scores.add(new TermScore(entry.getValue(), entry.getKey()));
    }

    Collections.sort(scores);
    return scores;
  }
}
