package anl.mifs.util;

/**
 * @author Nick Collier
 *         Date: Feb 11, 2009 2:06:19 PM
 */
public class ObjScore<T> implements Comparable<ObjScore> {

  private float score;
  private T obj;

  public ObjScore(float score, T obj) {
    this.score = score;
    this.obj = obj;
  }

  public float getScore() {
    return score;
  }

  public T getObject() {
    return obj;
  }

  public String toString() {
    return obj.toString() + ": " + score;
  }

  public int compareTo(ObjScore objScore) {
    return score < objScore.score ? -1 : score == objScore.score ? 0 : 1;
  }
}
