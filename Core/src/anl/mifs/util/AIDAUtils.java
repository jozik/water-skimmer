/**
 * 
 */
package anl.mifs.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * General static utility methods.
 * 
 * @author Nick Collier
 */
public class AIDAUtils {
  
  private static SimpleDateFormat shortFormat = new SimpleDateFormat("MM/dd/yyyy");
  private static SimpleDateFormat longFormat = new SimpleDateFormat("MMMMM dd, yyyy HH:mm:ss z");

  /**
   * Formats the specified data in long format "MMMMM dd, yyyy HH:mm:ss z".
   * 
   * @param date the date to format
   * @return the formatted date
   */
  public static String formatDate(Date date) {
    longFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    return longFormat.format(date);
  }

  /**
   * Formats the specified data in short format "MM/dd/yyyy".
   * 
   * @param date the date to format
   * @return the formatted date
   */
  public static String formatShortDate(Date date) {
    shortFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    return shortFormat.format(date);
  }
  
  /**
   * Converts a term to the internal format used by MIFS. For now this
   * just lowercases the term.
   * 
   * @param term the term to convert.
   * 
   * @return the converted term.
   */
  public static String toInternalFormat(String term) {
    return term.toLowerCase();
  }
  
  /**
   * Gets a date for the specified string. The format must be
   * YYYYMMDD.
   * 
   * @param strDate the date in YYYYMMDD
   * 
   * @return the created Date object.
   */
  public static Date getDate(String strDate) {
    int year = Integer.parseInt(strDate.substring(0, 4));
    int month = Integer.parseInt(strDate.substring(4, 6));
    int day = Integer.parseInt(strDate.substring(6, 8));
    Calendar cal = GregorianCalendar.getInstance();
    cal.set(year, month - 1, day, 0, 0, 0);
    return cal.getTime();
  }
  
  
  /**
   * Creates a hash map from the specified file. Each line is processed such
   * that everything to left of the delimiter becomes the key and everything to
   * right becomes the value. This WILL NOT distinguish between a delimiter 
   * embedded in key (escaped or otherwise) and the true delimiter. 
   * 
   * @param fileName
   * @param delimiter
   * @return
   * @throws IOException
   */
  public static Map<String, String> loadMap(String fileName, String delimiter) throws IOException {
    Map<String, String> map = new HashMap<String, String>();
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    String line = null;
    try {
      while ((line = reader.readLine()) != null) {
	line = line.trim();
	int index = line.indexOf(delimiter);
	String key = line.substring(0, index).trim();
	String val = line.substring(index + delimiter.length(), line.length()).trim();
	map.put(key, val);
      }
    } finally {
      if (reader != null)
	reader.close();
    }

    return map;
  }

}
