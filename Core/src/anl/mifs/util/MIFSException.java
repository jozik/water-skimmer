package anl.mifs.util;

/**
 * Exception wrapper class that we can use to throw from callback type functions.
 *
 * @author Nick Collier
 */
@SuppressWarnings("serial")
public class MIFSException extends Exception {

  public MIFSException() {
  }

  public MIFSException(String s) {
    super(s);
  }

  public MIFSException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public MIFSException(Throwable throwable) {
    super(throwable);
  }
}
