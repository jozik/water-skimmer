package anl.mifs.util;

/**
 * Encapsulates a Term string and a score. Implements Comparable by score.
 *
 * @author Nick Collier
 */
public class TermScore extends ObjScore<String> {

  public TermScore(float score, String obj) {
    super(score, obj);
  }

  public String getTerm() {
    return getObject();
  }


}
