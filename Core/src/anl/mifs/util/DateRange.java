package anl.mifs.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Encapsulates a date range.
 * 
 * @author Nick Collier
 */
public class DateRange {

  private Date start, end;
  private int hashCode;

  public DateRange(DateRange range) {
    this(range.start.getTime(), range.end.getTime());
  }

  public DateRange(long start, long end) {
    this.start = new Date(start);
    this.end = new Date(end);

    hashCode = 17;
    hashCode = 31 * hashCode + this.start.hashCode();
    hashCode = 31 * hashCode + this.end.hashCode();
  }

  public DateRange(Date start, Date end) {
    this(start.getTime(), end.getTime());
  }

  public Date getStart() {
    return start;
  }

  public Date getEnd() {
    return end;
  }

  public DateRange overlap(DateRange other) {
    long start = this.start.getTime();
    long end = this.end.getTime();
    long oStart = other.start.getTime();
    long oEnd = other.end.getTime();
    
    if (start >= oStart && end <= oEnd) {
      // this is within other
      return new DateRange(start, end);
    }

    if (oStart >= start && oEnd <= end) {
      // other is within this
      return new DateRange(other.start, other.end);
    }

    if (start >= oStart && start <= oEnd && end > oEnd) {
      // overlaps between this start and other end
      return new DateRange(start, oEnd);
    }

    if (start <= oStart && end >= oStart && end < oEnd) {
      // overlaps between other.start and this.end
      return new DateRange(oStart, end);
    }

    return null;
  }

  public boolean equals(Object obj) {
    if (obj instanceof DateRange) {
      DateRange other = (DateRange) obj;
      return other.start.equals(start) && other.end.equals(end);
    }

    return false;
  }

  public int hashCode() {
    return hashCode;
  }

  public String toString() {
    return "DateRange[" + start + " - " + end + "]";
  }
  
  /**
	 * Returns a list of Date objects set at Year, Month and the rest as
	 * default, spanning the months of the DateRange dr.
	 * 
	 * @param dr
	 *            the date range
	 * @return list of Date objects
	 */
	public List<Date> getMonthYears() {
		List<Date> result = new ArrayList<Date>();
		Calendar calStart = new GregorianCalendar();
		calStart.setTime(start);
		Calendar cal = new GregorianCalendar(calStart.get(Calendar.YEAR),
				calStart.get(Calendar.MONTH), 1);
		while (cal.getTime().before(end)) {
			result.add(cal.getTime());
			cal.add(Calendar.MONTH, 1);
		}
		return result;
	}
}
