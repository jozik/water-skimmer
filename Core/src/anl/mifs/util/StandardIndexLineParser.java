package anl.mifs.util;

/**
 * Parses a line in the standard index format. The format is:
 * 
 * date_in_millis:::url:::title:::author
 * 
 * with author being optional.
 * 
 * @author Nick Collier
 */
public class StandardIndexLineParser {
  
  private static final String DELIMITER = ":::";
  
  public static final int TIMESTAMP = 0;
  public static final int URL = 1;
  public static final int TITLE = 2;
  public static final int AUTHOR = 3;
  
  /**
   * Parses the line into four strings. One each for the
   * timestamp in milliseconds, url, title, and author.
   * The result is returned in a String[]. This will
   * use the specified String[] if it is not null and 
   * of length 4. The constants TIMESTAMP etc. can be 
   * used as indices into the returned array.
   * 
   * @param line the line to parse
   * @param result an optional array to store the results in
   * @return the parsed results.
   */
  public String[] parse(String line, String[] result) {
    if (result == null || result.length < 4)
      result = new String[4];
    line = line.trim();

    int index = line.indexOf(DELIMITER);
    result[TIMESTAMP] = line.substring(0, index).trim();

    index += 3;
    int end = line.indexOf(DELIMITER, index);
    result[URL] = line.substring(index, end).trim();

    index = end + 3;
    end = line.indexOf(DELIMITER, index);
    result[TITLE] = line.substring(index, end).trim();

    if (end + 3 >= line.length())
      result[AUTHOR] = "";
    else
      result[AUTHOR] = line.substring(end + 3).trim();

    return result;
  }
}
