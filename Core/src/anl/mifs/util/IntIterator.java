package anl.mifs.util;

/**
 * Iterator over a series of ints.
 *
 * @author Nick Collier
 */
public interface IntIterator {

  boolean hasNext();

  int next();
}
