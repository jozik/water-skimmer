package anl.mifs.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Iterates over the lines in standard index, returning the parsed results
 * in a String[].
 * 
 * @author Nick Collier
 */
public class IndexIterator {
  
  protected int index = 0;
  protected List<String> lines = new ArrayList<String>();
  private StandardIndexLineParser lineParser = new StandardIndexLineParser();
  
  public IndexIterator(String indexFile) throws IOException {
    BufferedReader reader = null;
    try {
      String line = null;
      reader = new BufferedReader(new FileReader(indexFile));
      while ((line = reader.readLine()) != null) {
	lines.add(line.trim());
      }
    } finally {
      if (reader != null) reader.close();
    }
  }
  
  public String nextLine() {
    String line = lines.get(index);
    index++;
    return line;
    
  }
  
  public String[] nextParsedLine(String[] result) {
    String line = nextLine();
    return lineParser.parse(line, result);
  }
  
  public boolean hasNext() {
    return index < lines.size();
  }
  
  /**
   * Rewinds the iterator one index line.
   */
  public void rewind() {
    if (index > 0) index--;
  }
}
