
/* First created by JCasGen Tue Jan 20 15:09:39 EST 2009 */
package anl.mifs.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Mar 16 15:58:25 EDT 2009
 * @generated */
public class Name_Type extends Annotation_Type {
  /** @generated */
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Name_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Name_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Name(addr, Name_Type.this);
  			   Name_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Name(addr, Name_Type.this);
  	  }
    };
  /** @generated */
  public final static int typeIndexID = Name.typeIndexID;
  /** @generated 
     @modifiable */
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("anl.mifs.types.Name");
 
  /** @generated */
  final Feature casFeat_lastName;
  /** @generated */
  final int     casFeatCode_lastName;
  /** @generated */ 
  public String getLastName(int addr) {
        if (featOkTst && casFeat_lastName == null)
      jcas.throwFeatMissing("lastName", "anl.mifs.types.Name");
    return ll_cas.ll_getStringValue(addr, casFeatCode_lastName);
  }
  /** @generated */    
  public void setLastName(int addr, String v) {
        if (featOkTst && casFeat_lastName == null)
      jcas.throwFeatMissing("lastName", "anl.mifs.types.Name");
    ll_cas.ll_setStringValue(addr, casFeatCode_lastName, v);}
    
  
 
  /** @generated */
  final Feature casFeat_firstName;
  /** @generated */
  final int     casFeatCode_firstName;
  /** @generated */ 
  public String getFirstName(int addr) {
        if (featOkTst && casFeat_firstName == null)
      jcas.throwFeatMissing("firstName", "anl.mifs.types.Name");
    return ll_cas.ll_getStringValue(addr, casFeatCode_firstName);
  }
  /** @generated */    
  public void setFirstName(int addr, String v) {
        if (featOkTst && casFeat_firstName == null)
      jcas.throwFeatMissing("firstName", "anl.mifs.types.Name");
    ll_cas.ll_setStringValue(addr, casFeatCode_firstName, v);}
    
  
 
  /** @generated */
  final Feature casFeat_initials;
  /** @generated */
  final int     casFeatCode_initials;
  /** @generated */ 
  public String getInitials(int addr) {
        if (featOkTst && casFeat_initials == null)
      jcas.throwFeatMissing("initials", "anl.mifs.types.Name");
    return ll_cas.ll_getStringValue(addr, casFeatCode_initials);
  }
  /** @generated */    
  public void setInitials(int addr, String v) {
        if (featOkTst && casFeat_initials == null)
      jcas.throwFeatMissing("initials", "anl.mifs.types.Name");
    ll_cas.ll_setStringValue(addr, casFeatCode_initials, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Name_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_lastName = jcas.getRequiredFeatureDE(casType, "lastName", "uima.cas.String", featOkTst);
    casFeatCode_lastName  = (null == casFeat_lastName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_lastName).getCode();

 
    casFeat_firstName = jcas.getRequiredFeatureDE(casType, "firstName", "uima.cas.String", featOkTst);
    casFeatCode_firstName  = (null == casFeat_firstName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_firstName).getCode();

 
    casFeat_initials = jcas.getRequiredFeatureDE(casType, "initials", "uima.cas.String", featOkTst);
    casFeatCode_initials  = (null == casFeat_initials) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_initials).getCode();

  }
}



    