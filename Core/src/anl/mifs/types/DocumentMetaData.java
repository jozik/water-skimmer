

/* First created by JCasGen Wed Jan 21 10:17:53 EST 2009 */
package anl.mifs.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.jcas.cas.FSArray;


/** 
 * Updated by JCasGen Mon Mar 16 15:58:25 EDT 2009
 * XML source: /Users/nick/Documents/workspace/fusion/Core/desc/DocumentMetaData.xml
 * @generated */
public class DocumentMetaData extends Annotation {
  /** @generated
   * @ordered 
   */
  public final static int typeIndexID = JCasRegistry.register(DocumentMetaData.class);
  /** @generated
   * @ordered 
   */
  public final static int type = typeIndexID;
  /** @generated  */
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected DocumentMetaData() {}
    
  /** Internal - constructor used by generator 
   * @generated */
  public DocumentMetaData(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public DocumentMetaData(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public DocumentMetaData(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {}
     
 
    
  //*--------------*
  //* Feature: title

  /** getter for title - gets document title
   * @generated */
  public String getTitle() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "anl.mifs.types.DocumentMetaData");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_title);}
    
  /** setter for title - sets document title 
   * @generated */
  public void setTitle(String v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_title == null)
      jcasType.jcas.throwFeatMissing("title", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_title, v);}    
   
    
  //*--------------*
  //* Feature: location

  /** getter for location - gets document location
   * @generated */
  public String getLocation() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "anl.mifs.types.DocumentMetaData");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_location);}
    
  /** setter for location - sets document location 
   * @generated */
  public void setLocation(String v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_location, v);}    
   
    
  //*--------------*
  //* Feature: authors

  /** getter for authors - gets array of authors
   * @generated */
  public FSArray getAuthors() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_authors == null)
      jcasType.jcas.throwFeatMissing("authors", "anl.mifs.types.DocumentMetaData");
    return (FSArray)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors)));}
    
  /** setter for authors - sets array of authors 
   * @generated */
  public void setAuthors(FSArray v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_authors == null)
      jcasType.jcas.throwFeatMissing("authors", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors, jcasType.ll_cas.ll_getFSRef(v));}    
    
  /** indexed getter for authors - gets an indexed value - array of authors
   * @generated */
  public Name getAuthors(int i) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_authors == null)
      jcasType.jcas.throwFeatMissing("authors", "anl.mifs.types.DocumentMetaData");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors), i);
    return (Name)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors), i)));}

  /** indexed setter for authors - sets an indexed value - array of authors
   * @generated */
  public void setAuthors(int i, Name v) { 
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_authors == null)
      jcasType.jcas.throwFeatMissing("authors", "anl.mifs.types.DocumentMetaData");
    jcasType.jcas.checkArrayBounds(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors), i);
    jcasType.ll_cas.ll_setRefArrayValue(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_authors), i, jcasType.ll_cas.ll_getFSRef(v));}
   
    
  //*--------------*
  //* Feature: date

  /** getter for date - gets 
   * @generated */
  public Date getDate() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_date == null)
      jcasType.jcas.throwFeatMissing("date", "anl.mifs.types.DocumentMetaData");
    return (Date)(jcasType.ll_cas.ll_getFSForRef(jcasType.ll_cas.ll_getRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_date)));}
    
  /** setter for date - sets  
   * @generated */
  public void setDate(Date v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_date == null)
      jcasType.jcas.throwFeatMissing("date", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setRefValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_date, jcasType.ll_cas.ll_getFSRef(v));}    
   
    
  //*--------------*
  //* Feature: source

  /** getter for source - gets The source of the document
   * @generated */
  public String getSource() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "anl.mifs.types.DocumentMetaData");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_source);}
    
  /** setter for source - sets The source of the document 
   * @generated */
  public void setSource(String v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_source == null)
      jcasType.jcas.throwFeatMissing("source", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_source, v);}    
   
    
  //*--------------*
  //* Feature: link

  /** getter for link - gets a link to more info (optional)
   * @generated */
  public String getLink() {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_link == null)
      jcasType.jcas.throwFeatMissing("link", "anl.mifs.types.DocumentMetaData");
    return jcasType.ll_cas.ll_getStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_link);}
    
  /** setter for link - sets a link to more info (optional) 
   * @generated */
  public void setLink(String v) {
    if (DocumentMetaData_Type.featOkTst && ((DocumentMetaData_Type)jcasType).casFeat_link == null)
      jcasType.jcas.throwFeatMissing("link", "anl.mifs.types.DocumentMetaData");
    jcasType.ll_cas.ll_setStringValue(addr, ((DocumentMetaData_Type)jcasType).casFeatCode_link, v);}    
  }

    