

/* First created by JCasGen Tue Jan 20 15:09:39 EST 2009 */
package anl.mifs.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Mar 16 15:58:25 EDT 2009
 * XML source: /Users/nick/Documents/workspace/fusion/Core/desc/DocumentMetaData.xml
 * @generated */
public class Name extends Annotation {
  /** @generated
   * @ordered 
   */
  public final static int typeIndexID = JCasRegistry.register(Name.class);
  /** @generated
   * @ordered 
   */
  public final static int type = typeIndexID;
  /** @generated  */
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Name() {}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Name(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Name(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Name(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {}
     
 
    
  //*--------------*
  //* Feature: lastName

  /** getter for lastName - gets 
   * @generated */
  public String getLastName() {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_lastName == null)
      jcasType.jcas.throwFeatMissing("lastName", "anl.mifs.types.Name");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Name_Type)jcasType).casFeatCode_lastName);}
    
  /** setter for lastName - sets  
   * @generated */
  public void setLastName(String v) {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_lastName == null)
      jcasType.jcas.throwFeatMissing("lastName", "anl.mifs.types.Name");
    jcasType.ll_cas.ll_setStringValue(addr, ((Name_Type)jcasType).casFeatCode_lastName, v);}    
   
    
  //*--------------*
  //* Feature: firstName

  /** getter for firstName - gets 
   * @generated */
  public String getFirstName() {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_firstName == null)
      jcasType.jcas.throwFeatMissing("firstName", "anl.mifs.types.Name");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Name_Type)jcasType).casFeatCode_firstName);}
    
  /** setter for firstName - sets  
   * @generated */
  public void setFirstName(String v) {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_firstName == null)
      jcasType.jcas.throwFeatMissing("firstName", "anl.mifs.types.Name");
    jcasType.ll_cas.ll_setStringValue(addr, ((Name_Type)jcasType).casFeatCode_firstName, v);}    
   
    
  //*--------------*
  //* Feature: initials

  /** getter for initials - gets 
   * @generated */
  public String getInitials() {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_initials == null)
      jcasType.jcas.throwFeatMissing("initials", "anl.mifs.types.Name");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Name_Type)jcasType).casFeatCode_initials);}
    
  /** setter for initials - sets  
   * @generated */
  public void setInitials(String v) {
    if (Name_Type.featOkTst && ((Name_Type)jcasType).casFeat_initials == null)
      jcasType.jcas.throwFeatMissing("initials", "anl.mifs.types.Name");
    jcasType.ll_cas.ll_setStringValue(addr, ((Name_Type)jcasType).casFeatCode_initials, v);}    
  }

    