package anl.wordnet.reader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import anl.mifs.reader.DocumentProcessor;
import anl.mifs.reader.StringExtractor;

/**
 * Basic class to read WordNet data from word net site.
 * The user provides the path to the .lst files and wordnet will read the data and 
 * find matching keyterms, modifying the .lst files.
 * 
 * @author Mark Altaweel
 *
 */
public class WordNetReader {
	/**Tracks the history of wordnet data read so repeats are not made in .lst files**/
	private Map<String,String>history=new HashMap<String,String>();
	
	/**
	 * Class to read from the WordNet site.
	 * @author Mark Altaweel
	 *
	 */
	private static class ArchiveMaker implements DocumentProcessor {
		    private String word;
		    
		    /**List of words that are similar to the keyterm that are stored in the class**/
		    public List<String> similarWords=new ArrayList<String>();
		    
		    /**
		     * Archive maker constructor.
		     * @param file a file to read from
		     * @param word the keytearm searched
		     * @throws IOException
		     */
		    public ArchiveMaker(String word) throws IOException {
		      this.word=word;
		    }
		   
		    @Override
		    public void processLine(String line) {
		    	
		        if (line.contains("S:<> (n)") || line.contains("S:<> (v)") || line.contains("S:<> (adj)")) {
		        	String[] split=null;
		        	
		        	if(line.contains("S:<>"))
		        		split=line.split("S:<>");
		        
		        	if(split.length>1){
		        		String second=split[1];
		        		String[]commas=second.split(",");
		        		if(commas.length==1)
		        			return;
		        		for(int i=0; i < commas.length;i++){
		        			String part=commas[i];
		        			if(part.contains(">")){
		        				int loc1=part.indexOf(">");
		        				int loc2=part.indexOf("(", loc1);
		        				if(loc2==-1)
		        					loc2=part.length();
		        				String pWord=part.substring(loc1+1, loc2);
		        				if(pWord.contains("WordNet home page"))
		        					continue;
		        				pWord=ltrim(pWord);
		        				pWord=rtrim(pWord);
		        		//		System.out.println(pWord);
		        				similarWords.add(pWord);
		        			}
		        		}
		        	}
		        
				  }

		    }

		    /**
		     * Method to remov leading whitespace.
		     * @param source
		     * @return
		     */
		    public static String ltrim(String source) {
		        return source.replaceAll("^\\s+", "");
		    }

		   /**
		    * Method to remove trailing whitespace.
		    * @param source
		    * @return
		    */
		    public static String rtrim(String source) {
		        return source.replaceAll("\\s+$", "");
		    }

		    @Override
		    public boolean done() {
		      return false;
		    }

		    public void close() {
		    	
		    }

		    /*
		     * (non-Javadoc)
		     * 
		     * @see anl.mifs.reader.DocumentProcessor#reset()
		     */
		    @Override
		    public void reset() {
		      // TODO Auto-generated method stub

		    }
		  }

	/**
	 * Method to read an .lst file and returns the terms to search
	 * for similarities in WordNet.
	 * @param file a .lst file to read
	 * @return a map of matched terms 
	 */
	public Map<String,String> returnWords(String file){
		  Map<String,String> words = new HashMap<String,String>();
		  
		    try {
		     
		      BufferedReader input =  new BufferedReader(new FileReader(file));
		      try {
		        String line = null; //not declared within while loop
		        
		        while (( line = input.readLine()) != null){
		          String[] split = line.split("%cName=");
		          String word=split[0];
		          String word2=split[1];
		          words.put(word,word2);
		        }
		      }
		      finally {
		        input.close();
		      }
		    }
		    catch (IOException ex){
		      ex.printStackTrace();
		    }
		   
		    return words;
 	}
	
	/**
	 * Method writes out matched words and recreating the .lst file
	 * with the old searched terms as well as the new terms similar to the original 
	 * searched terms.
	 * @param mappedT a map of the original keyterm and list of new similar terms
	 * @param original map of the original terms
	 * @param newFile a .lst file
	 */
	public void mapWords(Map<String,List<String>> mappedT, Map<String,String>original,String newFile){

	    try {
	     
	      PrintWriter input =  new PrintWriter(new FileWriter(newFile));
	      try {
	        Set<String> keys= mappedT.keySet();
	        Iterator<String> ki = keys.iterator();
	        while(ki.hasNext()){
	        	String k = ki.next();
	        	k.trim();
	        	List<String>list=mappedT.get(k);
	        	String refName=original.get(k);
	        	String sContent="%cName=";
	        	if(!history.containsKey(k))
	        		input.println(k+sContent+refName);
	        	if(history.containsKey(k) &&!history.get(k).equalsIgnoreCase(refName))
	        		input.println(k+sContent+refName);
	        	history.put(k, refName);
	        	for(String s: list){
	        		s.trim();
	        		String lineW=s+sContent+refName;
	        		if(!history.containsKey(s))
	        			input.println(lineW);
	        		if(history.containsKey(s) && !history.get(s).equalsIgnoreCase(refName))
	        			input.println(lineW);
	        		history.put(s,refName);
	        	}
	        }
	      }
	      finally {
	        input.close();
	      }
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    
	}
	
	/**
	 * Method conducts the entire parsing, search, and recreating the .lst file process
	 * @param urlBase the key url for the WordNet site
	 * @param addOn additional url info to link to the keyterm search
	 * @param directory a directory to read files from
	 * @throws IOException
	 * @throws ParseException
	 */
	public void run(String urlBase,String addOn, File directory) throws IOException, ParseException {
		String[] files = directory.list();
		
		StringExtractor extractor = new StringExtractor();
		
		for(int i=0; i < files.length; i++) {
			String newFile = files[i];
			
			if(newFile.endsWith(".lst")) {
				Map<String,String> words=returnWords(directory.getAbsolutePath()+"/"+newFile);
				Iterator<String>wi=words.keySet().iterator();
				Map<String,List<String>>similarMap=new HashMap<String,List<String>>();
				while(wi.hasNext()){
					String word = wi.next();
					ArchiveMaker maker = new ArchiveMaker(word);
		
					String[] subs = word.split(" ");
					String searchWord="";
					for(int t=0; t < subs.length; t++){
						if(t<subs.length-1)
							searchWord+=subs[t]+"+";
						else
							searchWord+=subs[t];
					}
					String url=urlBase+searchWord+addOn;
					System.out.println(url);
					extractor.run(url, maker);
					List<String>similar=maker.similarWords;
					similarMap.put(word, similar);
	//				maker.close();
				}
				mapWords(similarMap,words,directory.getAbsolutePath()+"/"+newFile);
			}
		}
	} 
	
	/**
	 * Main method to launch the search and word matching process.
	 * @param args a string array argument with arg[0] should be set to the directory
	 * location of the .lst files
	 */
	  public static void main(String[] args){
		  String baseUrl="http://wordnetweb.princeton.edu/perl/webwn?s=";
		  String addOn="&sub=Search+WordNet&o2=&o0=1&o7=&o5=&o1=1&o6=&o4=&o3=&h=";
		  File directory = new File(args[0]);
		  WordNetReader wnr = new WordNetReader();
		  try {
			wnr.run(baseUrl,addOn,directory);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
}