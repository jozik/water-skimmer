package cnh.mongodb;

/**
 * Contains mongo DB related constants, such as document field names etc.
 * 
 * @author Nick Collier
 */
public interface DBConstants {
	
	public static String DOCUMENT_WATER = "documentWater";
	public static String DOCUMENT_LOCATION = "documentLocation";
	public static String DOCUMENT_INSTITUTION = "documentInstitution";
	public static String DOCUMENT_SENTIMENT = "documentSentiment";
	public static String DOCUMENT_SENTENCES = "documentSentences";
	
	public static String TOKEN_COUNT = "tokenCount";
	
	public static String START = "start";
	public static String END = "end";
	
	public static String TITLE_START = "titleStart";
	public static String TITLE_END = "titleEnd";
	
	public static String TF_IDF = "tf_idf";
	public static String TOPICALITY = "topicality";
	public static String KIND = "kind";
	public static String DOCUMENT_WATER_KIND = DOCUMENT_WATER + "." + KIND;
	public static String WATER = "WATER";
	
	public static String INSTITUTION_LOCATION_CATEGORY = "institutionLocation";
	public static String INSTITUTION_NAME_CATEGORY = "institutionName";
	public static String DOCUMENT_INSTITUTION_LOCATION_CATEGORY = DOCUMENT_INSTITUTION + "." + INSTITUTION_LOCATION_CATEGORY;
	public static String DOCUMENT_INSTITUTION_NAME_CATEGORY = DOCUMENT_INSTITUTION + "." + INSTITUTION_NAME_CATEGORY;
	
	public static String LOCATION_CATEGORY = "locationCategory";
	public static String SUB_LOCATION_CATEGORY = "subLocationCategory";
	public static String DOCUMENT_LOCATION_CATEGORY = DOCUMENT_LOCATION + "." + LOCATION_CATEGORY;
	public static String DOCUMENT_SUBLOCATION_CATEGORY = DOCUMENT_LOCATION + "." + SUB_LOCATION_CATEGORY;
	
	public static String TITLE = "title";
	public static String LOCATION = "location";
	public static String DATE = "date";
	public static String SOURCE = "source";
	public static String LENGTH = "length";
	public static String CONTENTS = "contents";
	
	public static String FIRST_NAME = "firstName";
	public static String LAST_NAME = "lastName";
	public static String INITIALS = "initials";
	public static String AUTHORS = "authors";
	
	public static String TOKEN_INDEX = "tokenIndex";
	public static String START_TOKEN_INDEX = "startTokenIndex";
	public static String END_TOKEN_INDEX = "endTokenIndex";
	public static String TEXT = "text";
	
	public static String SUBJECTIVITY_TYPE = "subjType";
	public static String PRIOR_POLARITY = "priorP";
	
	
	
	
	

}
