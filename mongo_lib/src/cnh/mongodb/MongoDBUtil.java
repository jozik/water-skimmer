package cnh.mongodb;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.mongodb.DB;

public class MongoDBUtil {

	private static String user = null;
	private static char[] pwd = null;

	public static boolean authenticate(DB db, String message) {
		if (user == null || pwd == null) {
			// Using a JPanel as the message for the JOptionPane
			JPanel userPanel = new JPanel();
			userPanel.setLayout(new GridLayout(2, 2));

			// Labels for the textfield components
			JLabel usernameLbl = new JLabel("Username:");
			JLabel passwordLbl = new JLabel("Password:");

			JTextField username = new JTextField();
			JPasswordField passwordFld = new JPasswordField();

			// Add the components to the JPanel
			userPanel.add(usernameLbl);
			userPanel.add(username);
			userPanel.add(passwordLbl);
			userPanel.add(passwordFld);
			username.requestFocus();

			// As the JOptionPane accepts an object as the message
			// it allows us to use any component we like - in this case
			// a JPanel containing the dialog components we want
			JOptionPane.showConfirmDialog(null, userPanel, message, JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.PLAIN_MESSAGE);
			user = username.getText();
			pwd = passwordFld.getPassword();
		}

		return db.authenticate(user, pwd);
	}
}
