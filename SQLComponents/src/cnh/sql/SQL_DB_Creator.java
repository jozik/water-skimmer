package cnh.sql;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class SQL_DB_Creator {

	static String pathToSpecs = "./SQLComponents/sql/";
	
	public SQL_DB_Creator(String path){
		if(path.length() > 0) pathToSpecs = path;
	}
	
	public static Vector<String> readSQL(String fileName){
		Vector<String> results = new Vector<String>();
		String SQL = "";
		try{
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			String line = null;
			while((line = br.readLine()) != null){
				if(!line.startsWith("#") && (line.trim().length() > 0)) SQL += line + System.getProperty("line.separator");
				if(line.trim().startsWith("END;")){
					results.add(SQL);
					SQL = "";
				}
			}
			if(SQL.length() > 0) results.add(SQL);
			br.close();
		}
		catch(Exception e){ e.printStackTrace(); }
		return results;
	}
	
	public void createDB(Connection connect) throws Exception{
		try{
			connect.setAutoCommit(false);
			BufferedReader br = new BufferedReader(new FileReader(pathToSpecs + "/sql_create.txt"));
			String line = null;
			while((line = br.readLine()) != null){
				Vector<String> sqlStatements = readSQL(pathToSpecs + line);
				for(String sql : sqlStatements){
					System.out.println("EXECUTING: " + sql);
					Statement st = connect.createStatement();
					st.execute(sql);
				}
			}
			br.close();
			connect.commit();
			connect.setAutoCommit(true);
		}
		catch(Exception e){ 
			connect.rollback();
			throw(e);
		}
	}
	
	public void buildDB(Connection connect) throws Exception{
		try{
			connect.setAutoCommit(false);
			BufferedReader br = new BufferedReader(new FileReader(pathToSpecs + "/sql_build.txt"));
			String line = null;
			while((line = br.readLine()) != null){
				if(!line.startsWith("#")){
					DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date d1 = new Date();
					System.out.print("Calling: " + line + " at " + df.format(d1) + " ... ");
					CallableStatement st = connect.prepareCall("{call " + line + "}");
					st.executeQuery();
					Date d2 = new Date();
					System.out.println(" done at " + df.format(d2));
				}
			}
			br.close();
			connect.commit();
			connect.setAutoCommit(true);
		}
		catch(Exception e){ 
			connect.rollback();
			throw(e);
		}
	}
}
