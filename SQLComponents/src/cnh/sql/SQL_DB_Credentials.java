package cnh.sql;

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class SQL_DB_Credentials {

	private static String[] hostlist = { "//localhost/", "//255.255.255.255/" };
	
	private static String host = "";
	private static String prfx = "";
	private static String sffx = "";
	private static String user = "";
	private static String pwd  = "";
	
	public static void clear(){
		host = "";
		prfx = "";
		sffx = "";
		user = "";
		pwd  = "";
	}
	
	public static void establish(){
		establish(true);
	}
	
	public static void establish(boolean usePrefix){
		if(user.length() == 0 || pwd.length() == 0){
			  JPanel userPanel = new JPanel();
			  userPanel.setLayout(new GridLayout((usePrefix ? 5 : 3), 2));
	
			  JComboBox      hostList    = new JComboBox(hostlist);
			  JTextField     prefix      = new JTextField();
			  JTextField     suffix      = new JTextField();
			  JTextField     username    = new JTextField();
			  JPasswordField passwordFld = new JPasswordField();
	
			  // Add the components to the JPanel
			  userPanel.add(new JLabel("Host:"));
			  userPanel.add(hostList);
			  if(usePrefix){
				  userPanel.add(new JLabel("Collection Prefix:"));
				  userPanel.add(prefix);
				  userPanel.add(new JLabel("Collection Suffix:"));
				  userPanel.add(suffix);
			  }
			  userPanel.add(new JLabel("Username:"));
			  userPanel.add(username);
			  userPanel.add(new JLabel("Password:"));
			  userPanel.add(passwordFld);
			  username.requestFocus();
	
			  JOptionPane.showConfirmDialog(null, userPanel, "Provide host, username and password for SQL DB:", JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.PLAIN_MESSAGE);
			  host = (String)hostList.getSelectedItem();
			  prfx = prefix.getText();                        if(prfx == null) prfx = "";
			  sffx = suffix.getText();                        if(sffx == null) sffx = "";
			  user = username.getText();                      if(user == null) user = ""; 
			  pwd  = new String(passwordFld.getPassword());   if(pwd  == null) pwd  = "";
		}
	}

	public static String getHost(){     return host; }
	public static String getPrefix(){   return prfx; }
	public static String getSuffix(){   return sffx; }
	public static String getUsername(){ return user; }
	public static String getPwd(){      return pwd;  }
	
}
