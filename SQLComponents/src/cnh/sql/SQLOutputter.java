package cnh.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import opennlp.uima.Sentence;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import anl.mifs.types.Date;
import anl.mifs.types.DocumentMetaData;
import cnh.annotator.authorities.WaterAuthorityCandidate;
import cnh.annotator.sentiment.Sentiment;
import cnh.annotator.types.CNHDocument;
import cnh.annotator.types.Title;
import cnh.water.annotation.LocationAnnotation;
import cnh.water.annotation.WaterAnnotation;


public class SQLOutputter extends JCasAnnotator_ImplBase{
  int cDone = 0;
  Connection connect;
  
  SQL_DB_Creator dbCreator;
  
  public void initialize(UimaContext aContext) throws ResourceInitializationException {
	  SQL_DB_Credentials.establish();
	  
	  String host = SQL_DB_Credentials.getHost();
	  String prfx = SQL_DB_Credentials.getPrefix();
	  String sffx = SQL_DB_Credentials.getSuffix();
	  String user = SQL_DB_Credentials.getUsername();
	  String pwd  = SQL_DB_Credentials.getPwd();
	  
	  String collectionName = (String) aContext.getConfigParameterValue("collectionName");
	  String SQLPath        = (String) aContext.getConfigParameterValue("sqlpath");
	  String SQLDriver      = (String) aContext.getConfigParameterValue("sqldriver");
	  String SQLConnType    = (String) aContext.getConfigParameterValue("sqlconnectiontype");
	  
	  try {
		dbCreator = new SQL_DB_Creator(SQLPath);
        Class.forName(SQLDriver);
        connect = DriverManager.getConnection(SQLConnType + ":" + host, user, pwd);
        Statement stmt = connect.createStatement();
        
        // DESTROYS the original database (if it exists) and creates the new one!        
        String dbName = prfx + collectionName + sffx;
        stmt.execute("DROP DATABASE IF EXISTS " + dbName + ";");
        stmt.execute("CREATE DATABASE " + dbName + ";");
        connect.setCatalog(dbName);

        dbCreator.createDB(connect);
      }
	  catch(Exception e){
		e.printStackTrace();
	  }
  }

	
  public void process(JCas jcas) throws AnalysisEngineProcessException {
		try {
	        long documentID = insertDocumentMetaData(jcas, connect);
			
	        insertSentenceData(jcas, connect, documentID);
	        insertWaterKeywordData(jcas, connect, documentID);
	        insertWaterAuthorityCandidateData(jcas, connect, documentID);
	        insertLocationData(jcas, connect, documentID);
	        insertSentimentData(jcas, connect, documentID);
	        
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
  
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		try {
			// Run the 'rebuild' process:
			dbCreator.buildDB(connect);
	        connect.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}


  private long insertDocumentMetaData(JCas jcas, Connection connect) throws AnalysisEngineProcessException, SQLException {
	    FSIterator<Annotation> iter = jcas.getAnnotationIndex(DocumentMetaData.type).iterator();
		if (!iter.hasNext()) throw new AnalysisEngineProcessException("SQL", "doc_metadata_not_found_error", new Object[0]);
		DocumentMetaData annot = (DocumentMetaData)iter.next();

        Statement stmt = connect.createStatement();

        // Source
        String source = annot.getSource();
        
        // Location
		String location = annot.getLocation();
		if(location.length() > 399) location = location.substring(199);
		
		// Publication Date
        Date mifsDate = annot.getDate();
        String dateString = mifsDate.getYear() + "-" + mifsDate.getMonth() + "-" + mifsDate.getDay();
        
        // Title
        String title = annot.getTitle();
        title = title.replaceAll("\"", "`");
        
        if(title.length() > 399) title = title.substring(399);
      
		// Article Length
		int length = annot.getEnd() - annot.getBegin();
        
		// Start of title, End of Title
		FSIterator<Annotation> tIter = jcas.getAnnotationIndex(Title.type).iterator();
		int titleStart = -1;
		int titleEnd   = -1;
		if (tIter.hasNext()) {
			Title T = (Title) tIter.next();
			titleStart =  T.getBegin();
			titleEnd   = T.getEnd();
		}
		
        // Count of tokens
		int tokenCount = -1;
		FSIterator<Annotation> tkIter = jcas.getAnnotationIndex(CNHDocument.type).iterator(); 
		if (tkIter.hasNext()) {
			CNHDocument cnhDoc = (CNHDocument) tkIter.next();
			tokenCount = cnhDoc.getTokenCount();
		}
        
        String query = "INSERT INTO articles (source, location, pubDate, title, length, titleStart, titleEnd, tokenCount) " +
        		                     "VALUES (\"" + source     + "\"," +
        		                             "\"" + location   + "\"," +
        		                             "\"" + dateString + "\"," +
        		                             "\"" + title      + "\"," +
        		                                    length     + "," +
        		                                    titleStart + "," +
        		                                    titleEnd   + "," +
        		                                    tokenCount + ");";
        System.out.println(cDone + ": " + title);
        cDone++;
        stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
        long key = -1;
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) key=rs.getInt(1);
        rs.close();
        stmt.close();
        return key;

//		doc.put(TITLE, annot.getTitle());
//		doc.put(LOCATION, annot.getLocation());
//		Calendar cal = new GregorianCalendar(mifsDate.getYear(), mifsDate.getMonth() - 1, mifsDate.getDay());
//		doc.put(DATE, cal.getTime());
//		doc.put(SOURCE, annot.getSource());
//		doc.put(LENGTH, annot.getEnd() - annot.getBegin());

  }

  private void insertSentenceData(JCas jcas, Connection connect, long docID) throws AnalysisEngineProcessException, SQLException {
		for (FSIterator<Annotation> sentenceIter = jcas.getAnnotationIndex(Sentence.type).iterator(); sentenceIter
				.hasNext();) {

			Sentence sent = (Sentence) sentenceIter.next();
			int startTokenIndex = sent.getStartTokenIndex();
			int endTokenIndex = sent.getEndTokenIndex();
			int start = sent.getBegin();
			int end = sent.getEnd();

		
			String query = "INSERT INTO sentences (documentID, startTokenIndex, endTokenIndex, start, end) " +
			                   "VALUES ( " + docID           + ", " +
					                         startTokenIndex + ", " +
			                                 endTokenIndex   + ", " + 
					                         start           + ", " + 
			                                 end             + ");";
			Statement stmt = connect.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			System.out.print(".");
		}	
		System.out.println();
  }

  private void insertWaterKeywordData(JCas jcas, Connection connect, long docID) throws AnalysisEngineProcessException, SQLException {
		// put water annotations
		FSIterator<Annotation> waterIterator = jcas.getAnnotationIndex(WaterAnnotation.type).iterator();
		while (waterIterator.hasNext()) {
			WaterAnnotation water = (WaterAnnotation) waterIterator.next();
			String category        = water.getWaterCategory(); // We define these; no need to clean up text
			String key             =  water.getCoveredText();
			long   startTokenIndex = water.getStartTokenIndex();
			long   endTokenIndex   = water.getEndTokenIndex();
			int    start           = water.getBegin();
			int    end             = water.getEnd();
			String query = "INSERT INTO water_category_tokens (documentID, waterCategory, waterCategoryTokenText, startTokenIndex, endTokenIndex, start, end) " +
	                   "VALUES ( " + docID           + ", " +
					             "\"" + category     + "\"," +
	                             "\"" + key          + "\"," + 
			                         startTokenIndex + ", " +
	                                 endTokenIndex   + ", " + 
			                         start           + ", " + 
	                                 end             + ");";
            Statement stmt = connect.createStatement();
	        stmt.executeUpdate(query);
	        stmt.close();
	        System.out.print("~");
		}
		System.out.println();
		
  }
  
  
  private void insertWaterAuthorityCandidateData(JCas jcas, Connection connect, long docID) throws AnalysisEngineProcessException, SQLException {
	  String query = "";
	  try{
		  FSIterator<Annotation> waterAuthorityCandidateIterator = jcas.getAnnotationIndex(WaterAuthorityCandidate.type).iterator();
		  while (waterAuthorityCandidateIterator.hasNext()) {
				WaterAuthorityCandidate waterAuthCand = (WaterAuthorityCandidate) waterAuthorityCandidateIterator.next();
				String candidate = waterAuthCand.getFullText();
				candidate = candidate.replaceAll("\"", "`");
				candidate = candidate.replaceAll("\\\\", "");
				if(candidate.length() > 98) candidate = candidate.substring(0, 98);
				int startTokenIndex            = waterAuthCand.getStartTokenIndex();
				int endTokenIndex              = waterAuthCand.getEndTokenIndex();
				int start                      = waterAuthCand.getStart();
				int end                        = waterAuthCand.getEnd();
				int countOfTerms               = waterAuthCand.getCountOfTerms();
				int countOfNonNNPTerms         = waterAuthCand.getCountOfNonNNPTerms();
				int countOfInitialGeoTerms     = waterAuthCand.getCountOfInitialGeoTerms();
				int endsWithAct                = waterAuthCand.getEndsWithAct();
				int endsWithRoad               = waterAuthCand.getEndsWithRoad();
				int countOfEndGeoFeats         = waterAuthCand.getCountOfEndGeoFeatures();
				int matchesName1               = waterAuthCand.getMatchesName1();
				int matchesName2               = waterAuthCand.getMatchesName2();
				int precededByDefArt           = waterAuthCand.getPrecededByDefiniteArticle();
				int precededByPersTitle        = waterAuthCand.getPrecededByPersonalTitle();
				int hasRepresentative          = waterAuthCand.getHasRepresentative();
				int includesPlace              = waterAuthCand.getIncludesLocationKeyword();
				int includesWater              = waterAuthCand.getIncludesWaterKeyword();
				int includesAdmin              = waterAuthCand.getIncludesAdminTerm();
				int followedByPersTitle        = waterAuthCand.getFollowedByPersonalTitle();
				int followedByAcronym          = waterAuthCand.getFollowedByAcronym();
				double iScore                  = waterAuthCand.getInstitutionScore(); // Default
				double pScore                  = waterAuthCand.getPersonScore();
				double wScore                  = waterAuthCand.getWaterScore();
	            query = "INSERT INTO candidate_water_authorities (documentID, " +
                               "origCandidateWaterAuthority, origCountOfTerms, origCountOfNonNNPTerms, " +
                               "origIncludesLocationKeyword, origIncludesWaterKeyword, origIncludesAdminTerm, origCountOfInitialGeoTerms, origEndsWithRoad, origEndsWithAct, origCountOfEndGeoFeatures, " +
                               "origMatchesNamePattern1, origMatchesNamePattern2, " +
	            		       "candidateWaterAuthority, countOfTerms, countOfNonNNPTerms, " +
	            		       "includesLocationKeyword, includesWaterKeyword, includesAdminTerm, countOfInitialGeoTerms, endsWithRoad, endsWithAct, countOfEndGeoFeatures, " +
	            		       "matchesNamePattern1, matchesNamePattern2, " +
				               "precededByDefiniteArticle, precededByPersonalTitle, hasRepresentative, followedByPersonalTitle, followedByAcronym, " +
				               "institutionScore, personScore, waterScore, startTokenIndex, endTokenIndex, start, end) " +
		                    "VALUES ( " + docID                       + "," +
		                              "\"" + candidate                + "\"," +
		                                     countOfTerms             + "," +
	                                         countOfNonNNPTerms       + "," +
	                                         includesPlace            + "," +
	                                         includesWater            + "," +
	                                         includesAdmin            + "," +
	                                         countOfInitialGeoTerms   + "," +
	                                         endsWithRoad             + "," +
	                                         endsWithAct              + "," +
	                                         countOfEndGeoFeats       + "," +
	                                         matchesName1             + "," + 
	                                         matchesName2             + "," +
	                                  "\"" + candidate                + "\"," +
		                                     countOfTerms             + "," +
		                                     countOfNonNNPTerms       + "," +
		                                     includesPlace            + "," +
		                                     includesWater            + "," +
	                                         includesAdmin            + "," +
		                                     countOfInitialGeoTerms   + "," +
		                                     endsWithRoad             + "," +
		                                     endsWithAct              + "," +
		                                     countOfEndGeoFeats       + "," +
	                                         matchesName1             + "," + 
	                                         matchesName2             + "," +
		                                     precededByDefArt         + "," +
		                                     precededByPersTitle      + "," +
		                                     hasRepresentative        + "," +
		                                     followedByPersTitle      + "," +
		                                     followedByAcronym        + "," +
		                                     iScore                   + "," +
						                     pScore                   + "," +
		                                     wScore                   + "," +
		                                     startTokenIndex          + "," + 
						                     endTokenIndex            + "," +
		                                     start                    + "," +
						                     end                      + ");";
	            
	          Statement stmt = connect.createStatement();
	          stmt.executeUpdate(query);
		      stmt.close();
		      System.out.print("@");
			}
			System.out.println();
	  }
	  catch(Exception e){
		  System.out.println();
		  System.out.println(query);
		  System.out.println();
	  }
  }

  private void insertLocationData(JCas jcas, Connection connect, long docID) throws AnalysisEngineProcessException, SQLException {
	  String query = "";
	  try{
		  FSIterator<Annotation> locationIterator = jcas.getAnnotationIndex(LocationAnnotation.type).iterator();
		  while (locationIterator.hasNext()) {
				LocationAnnotation locAnnotation = (LocationAnnotation) locationIterator.next();
				
				String location      = sqlClean(locAnnotation.getLocation(), 98);
				String subLocation   = sqlClean(locAnnotation.getSublocation(), 98);
				String locText       = sqlClean(locAnnotation.getCoveredText(), 98);
				long startTokenIndex = locAnnotation.getStartTokenIndex();
				long endTokenIndex   = locAnnotation.getEndTokenIndex();
				long start           = locAnnotation.getBegin();
				long end             = locAnnotation.getEnd();

				query = "INSERT INTO locations (documentID, location, sublocation, locationText, startTokenIndex, endTokenIndex, start, end) " +
		                    "VALUES ( " + docID               + "," +
		                              "\"" + location         + "\"," +
		                              "\"" + subLocation      + "\"," +
		                              "\"" + locText          + "\"," +
		                                     startTokenIndex  + "," + 
						                     endTokenIndex    + "," +
		                                     start            + "," +
						                     end              + ");";
	            
	          Statement stmt = connect.createStatement();
		      stmt.executeUpdate(query);
		      stmt.close();
		      System.out.print("x");
			}
			System.out.println();
	  }
	  catch(Exception e){
		  System.out.println();
		  System.out.println(query);
		  System.out.println();
	  }
  }
  
  private void insertSentimentData(JCas jcas, Connection connect, long docID) throws AnalysisEngineProcessException, SQLException {
	  String query = "";
	  try{
		  FSIterator<Annotation> sentimentIterator = jcas.getAnnotationIndex(Sentiment.type).iterator();
		  while (sentimentIterator.hasNext()) {
				Sentiment sentimentAnnotation = (Sentiment) sentimentIterator.next();
				
				String subjectivityType   = sqlClean(sentimentAnnotation.getSubjType(), 98);
				String priorPolarity      = sqlClean(sentimentAnnotation.getPriorP(), 98);
				String sentimentText      = sqlClean(sentimentAnnotation.getCoveredText(), 98);
				long tokenIndex           = sentimentAnnotation.getTokenIndex();
				long start                = sentimentAnnotation.getBegin();
				long end                  = sentimentAnnotation.getEnd();

				query = "INSERT INTO sentiment (documentID, subjectivityType, priorPolarity, sentimentText, tokenIndex, start, end) " +
		                    "VALUES ( " + docID               + "," +
		                              "\"" + subjectivityType + "\"," +
		                              "\"" + priorPolarity    + "\"," +
		                              "\"" + sentimentText    + "\"," +
		                                     tokenIndex       + "," + 
						                     start            + "," +
						                     end              + ");";
	            
	          Statement stmt = connect.createStatement();
		      stmt.executeUpdate(query);
		      stmt.close();
		      System.out.print(":) ");
			}
			System.out.println();
	  }
	  catch(Exception e){
		  System.out.println();
		  System.out.println(query);
		  System.out.println();
	  }
  }
 
  
  private String sqlClean(String source, int fieldLength){
		String ret = source.replaceAll("\"", "`");
		ret = ret.replaceAll("\\\\", "");
		if(ret.length() > 98) ret = ret.substring(0, 98);
		return ret;

  }
}
