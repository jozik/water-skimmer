CREATE PROCEDURE clear_monthly_aggregate_on_topicality()

BEGIN

  DROP TABLE IF EXISTS tmp_monthly_aggregator;

  DROP VIEW IF EXISTS view_monthly_aggregator;
   
END;

CREATE PROCEDURE monthly_aggregate_on_topicality(IN category VARCHAR(40))

BEGIN
  
  CALL clear_monthly_aggregate_on_topicality();
    
  CREATE TABLE tmp_monthly_aggregator
      SELECT articles.documentID, 
             EXTRACT(YEAR_MONTH FROM articles.pubDate) AS PUB_MONTH, 
             articles.title, 
             category AS CAT,
             tmp_tf_idf_and_topicality.TOPICALITY AS VAL
      FROM articles 
          INNER JOIN tmp_tf_idf_and_topicality ON 
              articles.documentID = tmp_tf_idf_and_topicality.documentID
      WHERE tmp_tf_idf_and_topicality.waterCategory=category;
   
   
 
   CREATE VIEW view_monthly_aggregator AS
     SELECT articles.documentID, 
            EXTRACT(YEAR_MONTH FROM articles.pubDate) AS PUB_MONTH, 
            articles.title,
            CAT,
            tmp_monthly_aggregator.VAL AS VAL,
            "TOPICALITY" AS SOURCE 
     FROM articles 
       INNER JOIN tmp_monthly_aggregator ON 
           articles.documentID = tmp_monthly_aggregator.documentID 
     ORDER BY PUB_MONTH, VAL DESC;
   
   
END;