CREATE PROCEDURE clear_basic()

BEGIN
  DROP TABLE IF EXISTS tmp_tf_idf_and_topicality;
  DROP TABLE IF EXISTS tmp_summed_tf_idf;
  DROP TABLE IF EXISTS tmp_water_tf_idf_addends;
  DROP TABLE IF EXISTS tmp_document_sentence_counts;
  DROP TABLE IF EXISTS tmp_topicality;
  DROP TABLE IF EXISTS tmp_water_tf_idf_max_and_min;
  DROP TABLE IF EXISTS tmp_cosine_similarity;
  DROP TABLE IF EXISTS tmp_axes;
  DROP TABLE IF EXISTS tmp_tf_idf_short;
  DROP TABLE IF EXISTS tmp_normalization_short;
  DROP TABLE IF EXISTS tmp_normalization_long;
  DROP TABLE IF EXISTS tmp_tf_short;
  DROP TABLE IF EXISTS tmp_idf_short;
  DROP TABLE IF EXISTS tmp_tf_long;
  DROP TABLE IF EXISTS tmp_idf_long;
  DROP TABLE IF EXISTS tmp_title_boost;
  DROP TABLE IF EXISTS tmp_term_freqs;
  DROP TABLE IF EXISTS tmp_article_counts;
  DROP TABLE IF EXISTS tmp_local_frequency;
  DROP TABLE IF EXISTS tmp_global_frequency;
  DROP TABLE IF EXISTS tmp_valid_sentiment_tkns;
  DROP TABLE IF EXISTS tmp_valid_location_tkns;
  DROP TABLE IF EXISTS tmp_valid_water_category_tkns;
  DROP TABLE IF EXISTS tmp_valid_arts;

END;


CREATE PROCEDURE rebuild(IN G_WEIGHT INT(4), IN L_WEIGHT INT(4), IN NORMALIZATION_METHOD INT(4), IN TITLE_BOOST_VAL DOUBLE, IN MIN_SENTENCE_COUNT INT(4), IN QUERY_ANGLE_FRACTION DOUBLE)

BEGIN

  CALL clear_basic();

# First-cut filter: if there are classes of articles that are to be omitted, omit them here
#     - Cull documents with fewer than min sentences
  CREATE TABLE tmp_valid_arts (INDEX (documentID))
     SELECT articles.documentID,
            titleEnd,
            tokenCount,
            COUNT(sentenceID) AS SENTENCE_COUNT
     FROM articles
       INNER JOIN sentences ON
          articles.documentID = sentences.documentID
     GROUP BY documentID
     HAVING SENTENCE_COUNT > MIN_SENTENCE_COUNT;
            
# Likewise, cull the water category tokens
  CREATE TABLE tmp_valid_water_category_tkns (INDEX (waterCategoryTokenID))
      SELECT waterCategoryTokenID,
             water_category_tokens.documentID,
             waterCategory,
             start
      FROM water_category_tokens
         INNER JOIN tmp_valid_arts
            ON water_category_tokens.documentID = tmp_valid_arts.documentID;

# TODO: Only relevant fields (see valid_water_category_tkns)?
# And all of the location tokens
  CREATE TABLE tmp_valid_location_tkns (INDEX (locationID))
      SELECT locationID,
             locations.documentID,
             location,
             subLocation,
             locationText,
             startTokenIndex,
             endTokenIndex,
             start,
             end
      FROM locations
         INNER JOIN tmp_valid_arts
            ON locations.documentID = tmp_valid_arts.documentID;
             
# And all of the sentiment tokens
  CREATE TABLE tmp_valid_sentiment_tkns (INDEX (sentimentID))
      SELECT sentimentID,
             sentiment.documentID,
             subjectivityType,
             priorPolarity,
             sentimentText,
             tokenIndex,
             start,
             end
      FROM sentiment
         INNER JOIN tmp_valid_arts
            ON sentiment.documentID = tmp_valid_arts.documentID;




# Basic statistics

# Global frequency for each water category
  CREATE TABLE tmp_global_frequency (INDEX (waterCategory))
      SELECT (SELECT COUNT(*) FROM tmp_valid_arts) AS N,
             waterCategory,
             COUNT(waterCategoryTokenID) AS GLOBAL_FREQ
      FROM tmp_valid_water_category_tkns
      GROUP BY waterCategory;

# Local frequency of each water category
  CREATE TABLE tmp_local_frequency (INDEX (waterCategory, documentID))
      SELECT waterCategory,
             documentID,
             COUNT(waterCategoryTokenID) AS LOCAL_FREQ
      FROM tmp_valid_water_category_tkns
      GROUP BY waterCategory, documentID;

# Number of articles in which each term appears
  CREATE TABLE tmp_article_counts (INDEX (waterCategory))
      SELECT waterCategory,
             COUNT(documentID) AS Ni
      FROM tmp_local_frequency
      GROUP BY waterCategory;

# Summary values of term frequencies      
  CREATE TABLE tmp_term_freqs (INDEX (documentID))
      SELECT documentID,
             MAX(LOCAL_FREQ)                                             AS MAX_FREQ,
             SUM(LOCAL_FREQ) / (SELECT COUNT(*) FROM tmp_article_counts) AS AVERAGE_FREQ
      FROM tmp_local_frequency
      GROUP BY documentID;
      
# Amount by which we 'boost' the title for CNH TF calculations
  CREATE TABLE tmp_title_boost (INDEX (documentID, waterCategory))
      SELECT tmp_valid_arts.documentID,
             waterCategory, 
             GREATEST(1,SUM(IF(tmp_valid_water_category_tkns.start < titleEnd, TITLE_BOOST_VAL, 0))) AS TITLE_BOOST
      FROM tmp_valid_water_category_tkns 
         INNER JOIN tmp_valid_arts ON
            tmp_valid_water_category_tkns.documentID = tmp_valid_arts.documentID
      GROUP BY documentID, waterCategory;
             
             
             

# Calculate Global and Local weights (IDF and TF)
      
  
# Calculate the IDF for each document, for each water term
  CREATE TABLE tmp_idf_long (INDEX (waterCategory))
      SELECT tmp_global_frequency.waterCategory, 
             N,
             Ni,
             GLOBAL_FREQ,
             LOG( N / (Ni + 1)) + 1                                                              AS CNH_IDF,
             SQRT( 1 / (SUM(LOCAL_FREQ * LOCAL_FREQ)) )                                          AS DUMAIS_NORMAL,
             GLOBAL_FREQ / Ni                                                                    AS DUMAIS_GFIDF,
             LOG( N / Ni) + 1                                                                    AS DUMAIS_IDF,
             1 - SUM( (LOCAL_FREQ / GLOBAL_FREQ) * LOG(LOCAL_FREQ/GLOBAL_FREQ) / LOG(N) )        AS DUMAIS_ENTROPY,
             LOG( N / (Ni))                                                                      AS CK_IDFB,
             LOG( ( N - Ni ) / Ni )                                                              AS CK_IDFP,
             1 + SUM( (LOCAL_FREQ / GLOBAL_FREQ) * LOG(LOCAL_FREQ/GLOBAL_FREQ) / LOG(N) )        AS CK_ENPY,
             GLOBAL_FREQ / Ni                                                                    AS CK_IGFF,
             LOG( (GLOBAL_FREQ / LOCAL_FREQ) + 1)                                                AS CK_IGFL,
             GLOBAL_FREQ / Ni + 1                                                                AS CK_IGFI,
             SQRT( ( GLOBAL_FREQ / Ni ) - 0.9)                                                   AS CK_IGFS
      FROM tmp_global_frequency
         INNER JOIN tmp_local_frequency ON
            tmp_global_frequency.waterCategory = tmp_local_frequency.waterCategory
         INNER JOIN tmp_article_counts ON
            tmp_global_frequency.waterCategory = tmp_article_counts.waterCategory 
      GROUP BY waterCategory;



  CREATE TABLE tmp_tf_long (INDEX (documentID, waterCategory))
      SELECT tmp_local_frequency.documentID, 
             tmp_local_frequency.waterCategory, 
             SQRT(LOCAL_FREQ) * TITLE_BOOST                                                      AS CNH_TF,
             1                                                                                   AS DUMAIS_BINARY,
             LOCAL_FREQ                                                                          AS DUMAIS_FREQ,
             LOG(LOCAL_FREQ + 1)                                                                 AS DUMAIS_LOG,
             1                                                                                   AS CK_BINARY,
             LOCAL_FREQ                                                                          AS CK_FREQ,
             LOG(LOCAL_FREQ)                                                                     AS CK_LOG,
             (1 + LOG(LOCAL_FREQ))/( 1 + LOG(AVERAGE_FREQ) )                                     AS CK_LOGN,
             0.5 + 0.5 * LOCAL_FREQ / AVERAGE_FREQ                                               AS CK_AUGNORM
      FROM tmp_local_frequency 
         INNER JOIN tmp_title_boost ON
            tmp_local_frequency.documentID = tmp_title_boost.documentID
           AND
            tmp_local_frequency.waterCategory = tmp_title_boost.waterCategory
         INNER JOIN tmp_term_freqs ON
            tmp_local_frequency.documentID = tmp_term_freqs.documentID;


# Select one of the global weights
IF     ( G_WEIGHT = 0  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CNH_IDF          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 1  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, DUMAIS_NORMAL    AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 2  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, DUMAIS_GFIDF     AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 3  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, DUMAIS_IDF       AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 4  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, DUMAIS_ENTROPY   AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 5  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IDFB          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 6  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IDFP          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 7  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_ENPY          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 8  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IGFF          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 9  ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IGFL          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 10 ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IGFI          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSEIF ( G_WEIGHT = 11 ) THEN  CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, CK_IGFs          AS GLOBAL_WEIGHT  FROM tmp_idf_long;
ELSE                           CREATE TABLE tmp_idf_short (INDEX (waterCategory)) SELECT waterCategory, 1                AS GLOBAL_WEIGHT  FROM tmp_idf_long;
END IF;



# Select one of the local weights
IF     ( L_WEIGHT = 0  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CNH_TF        AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 1  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, DUMAIS_BINARY AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 2  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, DUMAIS_FREQ   AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 3  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, DUMAIS_LOG    AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 4  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CK_BINARY     AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 5  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CK_FREQ       AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 6  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CK_LOG        AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 7  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CK_LOGN       AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSEIF ( L_WEIGHT = 8  ) THEN  CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, CK_AUGNORM    AS LOCAL_WEIGHT FROM tmp_tf_long;
ELSE                           CREATE TABLE tmp_tf_short (INDEX (documentID, waterCategory)) SELECT documentID, waterCategory, 1             AS LOCAL_WEIGHT FROM tmp_tf_long;
END IF;



# Normalization Factors
  CREATE TABLE tmp_normalization_long
      SELECT tmp_tf_short.documentID,
             1/ SQRT(tokenCount)                                                              AS CNH_NORMALIZATION,                                            
             1/ SQRT(SUM( (LOCAL_WEIGHT * GLOBAL_WEIGHT) * (LOCAL_WEIGHT * GLOBAL_WEIGHT) ) ) AS COSINE_NORMALIZATION
      FROM tmp_tf_short
         INNER JOIN tmp_valid_arts
             ON tmp_tf_short.documentID = tmp_valid_arts.documentID
         INNER JOIN tmp_idf_short 
             ON tmp_tf_short.waterCategory = tmp_idf_short.waterCategory
      GROUP BY documentID;



# And select a normalization
IF     ( NORMALIZATION_METHOD = 0) THEN CREATE TABLE tmp_normalization_short (INDEX (documentID)) SELECT documentID, CNH_NORMALIZATION    AS NORMALIZATION FROM tmp_normalization_long;
ELSEIF ( NORMALIZATION_METHOD = 1) THEN CREATE TABLE tmp_normalization_short (INDEX (documentID)) SELECT documentID, COSINE_NORMALIZATION AS NORMALIZATION FROM tmp_normalization_long;
ELSE                                    CREATE TABLE tmp_normalization_short (INDEX (documentID)) SELECT documentID, 1                    AS NORMALIZATION FROM tmp_normalization_long;
END IF;



# Calculate the base TF_IDF for each document, for each water term
  CREATE TABLE tmp_tf_idf_short (INDEX (documentID, waterCategory)) 
      SELECT tmp_tf_short.documentID, 
             tmp_tf_short.waterCategory,
             LOCAL_WEIGHT * GLOBAL_WEIGHT * NORMALIZATION AS TF_IDF
      FROM tmp_tf_short 
          INNER JOIN tmp_normalization_short 
              ON tmp_tf_short.documentID = tmp_normalization_short.documentID 
          INNER JOIN tmp_idf_short
              ON tmp_tf_short.waterCategory = tmp_idf_short.waterCategory;




#   
#   # Canonical IR Approaches
#   
#   # For two-axis comparisons:
#     CREATE TABLE tmp_axes (INDEX (documentID))
#         SELECT WATER.documentID, 
#                WATER.waterCategory, 
#                WATER.TF_IDF             AS WATER_TF_IDF, 
#                OTHER.waterCategory      AS OTHER_CAT, 
#                OTHER.TF_IDF             AS OTHER_TF_IDF 
#         FROM tmp_tf_idf_short AS WATER 
#             INNER JOIN tmp_tf_idf_short AS OTHER 
#                 ON WATER.documentID = OTHER.documentID 
#         HAVING WATER.waterCategory="WATER" AND OTHER.waterCategory!="WATER";
#   
#   
#   
#   # Cosine similarity
#     CREATE TABLE tmp_cosine_similarity (INDEX (documentID, waterCategory))
#         SELECT documentID,
#                OTHER_CAT                                                                                              AS waterCategory,  
#                (WATER_TF_IDF * COS(QUERY_ANGLE_FRACTION * PI()) + OTHER_TF_IDF * SIN(QUERY_ANGLE_FRACTION * PI())) 
#                  / SQRT( WATER_TF_IDF * WATER_TF_IDF + OTHER_TF_IDF * OTHER_TF_IDF)                                   AS COS_ANGLE
#         FROM tmp_axes;
#   
#   
#   
#   
#   # TOPICALITY 2.0 
#   # The difference between topicality 2.0 and 'original topicality' is the filtering of documents;
#   # In original topicality, the whole corpus was used to generate TF and IDF values, but then
#   # some documents were eliminated because they were too short. Topicality 2.0 uses 'valid articles',
#   # which may cull documents, but maintains a consistent corpus once the valid articles are established.
#   
#   # Determines the max and min of the tf_idf values for 'Water'
#     CREATE TABLE tmp_water_tf_idf_max_and_min (INDEX (waterCategory))
#         SELECT waterCategory, 
#                MAX(TF_IDF) AS MAX_WATER_TF_IDF, 
#                MIN(TF_IDF) AS MIN_WATER_TF_IDF
#         FROM tmp_tf_idf_short
#         GROUP BY waterCategory
#         HAVING waterCategory = "WATER";
#   
#   
#   
#   # CNH "Topicality"
#     CREATE TABLE tmp_topicality (INDEX (documentID, waterCategory))
#         SELECT documentID, 
#                WATER_TF_IDF,
#                OTHER_CAT                                                                                                 AS waterCategory,
#                OTHER_TF_IDF,
#                MAX_WATER_TF_IDF,
#                MIN_WATER_TF_IDF,
#                (WATER_TF_IDF + OTHER_TF_IDF) * ((WATER_TF_IDF - MIN_WATER_TF_IDF)/(MAX_WATER_TF_IDF - MIN_WATER_TF_IDF)) AS TOPICALITY
#         FROM tmp_axes
#           JOIN tmp_water_tf_idf_max_and_min
#         WHERE WATER_TF_IDF > MIN_WATER_TF_IDF;
#   
#   
#   
#   # ORIGINAL TOPICALITY
#   
#   # Get count of sentences in each document
#      CREATE TABLE tmp_document_sentence_counts (INDEX (documentID)) 
#          SELECT articles.documentID, 
#                 COUNT(Sentences.documentID) AS SentenceCount 
#          FROM articles 
#             INNER JOIN sentences ON 
#                 articles.documentID = sentences.documentID 
#          GROUP BY sentences.documentID;
#   
#   
#   # The 'water' tf_idf value will be added to the other terms' tf_idf scores, but only if sentence count > 3
#      CREATE TABLE tmp_water_tf_idf_addends
#          SELECT tmp_tf_idf_short.*, 
#                 SentenceCount 
#          FROM tmp_tf_idf_short 
#              INNER JOIN tmp_document_sentence_counts ON
#                 tmp_tf_idf_short.documentID = tmp_document_sentence_counts.documentID
#          WHERE waterCategory="WATER" AND SentenceCount > 3;
#   
#   
#   
#      CREATE TABLE tmp_summed_tf_idf (INDEX (documentID, waterCategory))
#          SELECT tmp_tf_idf_short.documentID, 
#                 tmp_tf_idf_short.waterCategory, 
#                 tmp_tf_idf_short.TF_IDF AS TF_IDF,
#                 tmp_water_tf_idf_addends.TF_IDF AS WATER_TF_IDF,
#                 (tmp_tf_idf_short.TF_IDF + tmp_water_tf_idf_addends.TF_IDF) AS SUMMED_TF_IDF 
#          FROM tmp_tf_idf_short 
#              INNER JOIN tmp_water_tf_idf_addends ON 
#                 tmp_tf_idf_short.documentID = tmp_water_tf_idf_addends.documentID 
#          WHERE tmp_tf_idf_short.waterCategory != "WATER";
#   
#   
#   
#      CREATE TABLE tmp_tf_idf_and_topicality
#          SELECT tmp_summed_tf_idf.documentID, 
#                 tmp_summed_tf_idf.waterCategory, 
#                 TF_IDF, 
#                 WATER_TF_IDF, 
#                 SUMMED_TF_IDF, 
#                 MAX_WATER_TF_IDF, 
#                 MIN_WATER_TF_IDF,
#                 SUMMED_TF_IDF * ((WATER_TF_IDF - MIN_WATER_TF_IDF)/(MAX_WATER_TF_IDF - MIN_WATER_TF_IDF)) AS TOPICALITY
#          FROM tmp_summed_tf_idf 
#              INNER JOIN tmp_water_tf_idf_max_and_min ON
#                  tmp_water_tf_idf_max_and_min.waterCategory = "WATER"
#          WHERE WATER_TF_IDF > MIN_WATER_TF_IDF;
#   
#            
END;
