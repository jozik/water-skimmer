CREATE PROCEDURE clear_find_synonyms()
BEGIN

  DROP TABLE IF EXISTS tmp_org_words_list;
  DROP TABLE IF EXISTS tmp_cwa_list;
  DROP TABLE IF EXISTS tmp_synonym_candidates;
  
END;


CREATE PROCEDURE find_synonyms(IN WATER_ONLY INT(4))

BEGIN

  call clear_find_synonyms();

  CREATE TABLE tmp_org_words_list( 
      orgWord VARCHAR(30) NOT NULL, 
      INDEX wordIndex(orgWord));


  INSERT INTO tmp_org_words_list (orgWord) SELECT "Administration";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Authority";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Bureau";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Co.";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Company";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Department";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Dept.";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Division";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Service";
  INSERT INTO tmp_org_words_list (orgWord) SELECT "Society";


  CREATE TABLE tmp_cwa_list (INDEX(FIRST_WORD)) 
     SELECT DISTINCT 
         LEFT(candidateWaterAuthority, INSTR(candidateWaterAuthority," ")) AS FIRST_WORD, 
         candidateWaterAuthority 
     FROM candidate_water_authorities
     WHERE includesWaterKeyword >= WATER_ONLY;


  CREATE TABLE tmp_synonym_candidates
     SELECT DISTINCT 
         A.candidateWaterAuthority AS original, 
         B.candidateWaterAuthority AS possibleSynonym 
     FROM tmp_org_words_list AS C, 
          tmp_cwa_list AS A 
              INNER JOIN tmp_cwa_list AS B 
                  ON A.FIRST_WORD = B.FIRST_WORD 
     WHERE B.candidateWaterAuthority = CONCAT(A.candidateWaterAuthority, CONCAT(" ",C.orgWord)) ORDER BY original;

END;