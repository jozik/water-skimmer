CREATE PROCEDURE clear_water_authorities()

BEGIN
  DROP TABLE IF EXISTS tmp_full_network;
  DROP TABLE IF EXISTS tmp_institutions_network;
  DROP TABLE IF EXISTS tmp_institutions_co_location;
  DROP TABLE IF EXISTS tmp_institutions_with_documents;
  DROP TABLE IF EXISTS tmp_top_ranked_institutions;
  DROP TABLE IF EXISTS tmp_institutions;
  DROP TABLE IF EXISTS tmp_candidate_institution_scores;
  DROP TABLE IF EXISTS tmp_calc_scores_with_docs;
  DROP TABLE IF EXISTS tmp_calc_scores;
  DROP TABLE IF EXISTS tmp_valid_candidates_and_documents_ranked;
  DROP TABLE IF EXISTS tmp_valid_candidates_and_documents_with_score;
  DROP TABLE IF EXISTS tmp_valid_candidates_and_documents;
  DROP TABLE IF EXISTS tmp_candidates_in_minimum_docs;
  DROP TABLE IF EXISTS tmp_candidates_and_valid_scored_docs;
  DROP TABLE IF EXISTS tmp_candidates_and_docs_unique;
  DROP TABLE IF EXISTS tmp_water_authority_status;
END;


CREATE PROCEDURE water_authorities(IN CATEGORY VARCHAR(40), IN USE_TOPICALITY BOOLEAN, IN MIN_DOCS INT(4), IN NUM_CALC_DOCS INT(4), IN NUM_NET_DOCS INT(4), IN INCLUDE_WATER INT(4))

BEGIN

  CALL clear_water_authorities();

# RECORD
  CREATE TABLE tmp_water_authority_status
      SELECT
         CATEGORY as category,
         USE_TOPICALITY as use_topicality,
         MIN_DOCS as min_docs,
         NUM_CALC_DOCS as num_calc_docs,
         NUM_NET_DOCS as num_net_docs,
         INCLUDE_WATER as include_water;

# STEP 1
# Create a list of unique combinations of candidate water
# authorities and document IDs
  CREATE TABLE tmp_candidates_and_docs_unique (INDEX (candidateWaterAuthority, documentID))
      SELECT DISTINCT candidate_water_authorities.candidateWaterAuthority,
                      candidate_water_authorities.documentID
      FROM candidate_water_authorities
      WHERE includeInAnalysis;



# STEP 2
# List of candidate water authorities and document IDs, including
# ONLY those documents that have a tf_idf or topicality score for the specified
# category
# Note that category can only be 'WATER' if topicality is NOT being used
IF NOT USE_TOPICALITY THEN
  CREATE TABLE tmp_candidates_and_valid_scored_docs (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_candidates_and_docs_unique.candidateWaterAuthority,
             tmp_candidates_and_docs_unique.documentID
      FROM tmp_candidates_and_docs_unique
          INNER JOIN tmp_tf_idf_short
              ON tmp_candidates_and_docs_unique.documentID = tmp_tf_idf_short.documentID 
      WHERE tmp_tf_idf_short.waterCategory = CATEGORY;
ELSE
  CREATE TABLE tmp_candidates_and_valid_scored_docs (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_candidates_and_docs_unique.candidateWaterAuthority,
             tmp_candidates_and_docs_unique.documentID
      FROM tmp_candidates_and_docs_unique
          INNER JOIN tmp_tf_idf_and_topicality
              ON tmp_candidates_and_docs_unique.documentID = tmp_tf_idf_and_topicality.documentID 
      WHERE tmp_tf_idf_and_topicality.waterCategory = CATEGORY;
END IF;



# STEP 3
# Reduce the list of candidates by including only those that occur in
# at least the specified number of valid documents (with tf_idf scores
# for the specified category)
  CREATE TABLE tmp_candidates_in_minimum_docs (INDEX (candidateWaterAuthority))
      SELECT tmp_candidates_and_valid_scored_docs.candidateWaterAuthority,
             COUNT(tmp_candidates_and_valid_scored_docs.documentID) AS CountOfValidDocs
      FROM tmp_candidates_and_valid_scored_docs
      GROUP BY tmp_candidates_and_valid_scored_docs.candidateWaterAuthority
      HAVING CountOfValidDocs >= MIN_DOCS;



# STEP 4
# For the list of valid candidates, get the valid
# documents in which they appear
  CREATE TABLE tmp_valid_candidates_and_documents (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_candidates_in_minimum_docs.candidateWaterAuthority,
             tmp_candidates_and_valid_scored_docs.documentID
      FROM tmp_candidates_in_minimum_docs
          INNER JOIN tmp_candidates_and_valid_scored_docs
              ON tmp_candidates_in_minimum_docs.candidateWaterAuthority = tmp_candidates_and_valid_scored_docs.candidateWaterAuthority;



# STEP 5
# For all of these documents, retrieve the score
IF NOT USE_TOPICALITY THEN
  CREATE TABLE tmp_valid_candidates_and_documents_with_score (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_valid_candidates_and_documents.candidateWaterAuthority,
             tmp_valid_candidates_and_documents.documentID,
             TF_IDF AS SCORE
      FROM tmp_valid_candidates_and_documents
          INNER JOIN tmp_tf_idf_short
              ON tmp_valid_candidates_and_documents.documentID = tmp_tf_idf_short.documentID
      WHERE tmp_tf_idf_short.waterCategory = CATEGORY;
ELSE
  CREATE TABLE tmp_valid_candidates_and_documents_with_score (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_valid_candidates_and_documents.candidateWaterAuthority,
             tmp_valid_candidates_and_documents.documentID,
             TOPICALITY AS SCORE
      FROM tmp_valid_candidates_and_documents
          INNER JOIN tmp_tf_idf_and_topicality
              ON tmp_valid_candidates_and_documents.documentID = tmp_tf_idf_and_topicality.documentID
      WHERE tmp_tf_idf_and_topicality.waterCategory = CATEGORY;
END IF;



# STEP 6
# Rank each candidate's documents
  CREATE TABLE tmp_valid_candidates_and_documents_ranked (INDEX (candidateWaterAuthority, documentID))
    SELECT candidateWaterAuthority, 
           documentID, 
           SCORE, 
           @docRank:=CASE WHEN @waterAuth != candidateWaterAuthority THEN 1 ELSE @docRank+1 END AS DOC_RANK, 
           @waterAuth:=candidateWaterAuthority AS WATER_AUTH 
    FROM 
      (SELECT @docRank:= 1) s1, 
      (SELECT @waterAuth:= 'NULL') s2, 
      (SELECT * FROM tmp_valid_candidates_and_documents_with_score ORDER BY candidateWaterAuthority, SCORE DESC) t;



# STEP 7
# Create a list with calculated TF_IDF as a function of the
# TF_IDF scores of the documents in which they occur for the specified category
  CREATE TABLE tmp_calc_scores (INDEX (candidateWaterAuthority))
      SELECT candidateWaterAuthority, 
             AVG(SCORE) AS CALC_SCORE 
      FROM tmp_valid_candidates_and_documents_ranked 
      WHERE DOC_RANK <= NUM_CALC_DOCS 
      GROUP BY candidateWaterAuthority;



# STEP 8
# Create a list of candidate water authorities and calculated 'institution' scores
  CREATE TABLE tmp_candidate_institution_scores (INDEX (candidateWaterAuthority))
      SELECT tmp_candidates_in_minimum_docs.candidateWaterAuthority,
             IF(((MAX(countOfTerms)              > 3) OR 
                 (MAX(countOfNonNNPTerms)        > 0) OR
                 (MAX(includesAdminTerm)         > 0) OR
                 (MAX(hasRepresentative)         > 0) OR
                 (MAX(precededByDefiniteArticle) > 0) OR
                 (MAX(followedByAcronym)         > 0)) 
                 AND
                 ((MAX(precededByPersonalTitle)   =  0) AND
                  (MAX(endsWithRoad)              =  0) AND
                  (MAX(endsWithAct)               =  0) AND
                  (MAX(includesWaterKeyword)      >= INCLUDE_WATER) AND
                  (NOT 
                    ( 
                      ( 
                        ( (MAX(countOfEndGeoFeatures) > 0) AND 
                          (MAX(countOfNonNNPTerms)    = 0) 
                        ) 
                        OR 
                        ( MAX(countOfTerms) - MAX(countOfInitialGeoTerms) = 1 
                        )
                     )
                     AND 
                       (MAX(hasRepresentative) = 0) ))), 1, 0) AS INST_SCORE
      FROM tmp_candidates_in_minimum_docs
          INNER JOIN candidate_water_authorities
              ON tmp_candidates_in_minimum_docs.candidateWaterAuthority = candidate_water_authorities.candidateWaterAuthority 
      GROUP BY candidateWaterAuthority;



# STEP 9
# Filter the Step 6 list with the Step 7 list using a cutoff for institution score
  CREATE TABLE tmp_institutions (INDEX (candidateWaterAuthority))
      SELECT tmp_calc_scores.candidateWaterAuthority,
             tmp_calc_scores.CALC_SCORE
      FROM tmp_calc_scores
          INNER JOIN tmp_candidate_institution_scores
              ON tmp_calc_scores.candidateWaterAuthority = tmp_candidate_institution_scores.candidateWaterAuthority
      WHERE INST_SCORE > 0;



# STEP 10
  CREATE TABLE tmp_top_ranked_institutions (INDEX (candidateWaterAuthority))
     SELECT candidateWaterAuthority,
            CALC_SCORE
     FROM
        tmp_institutions
     ORDER BY CALC_SCORE DESC
     LIMIT NUM_NET_DOCS;



# STEP 11      
# Create a list of the documents
  CREATE TABLE tmp_institutions_with_documents (INDEX (candidateWaterAuthority, documentID))
      SELECT tmp_top_ranked_institutions.candidateWaterAuthority,
             tmp_top_ranked_institutions.CALC_SCORE,
             tmp_valid_candidates_and_documents.documentID
      FROM tmp_top_ranked_institutions
          INNER JOIN tmp_valid_candidates_and_documents
              ON tmp_top_ranked_institutions.candidateWaterAuthority = tmp_valid_candidates_and_documents.candidateWaterAuthority; 

             

# STEP 12
# Establish Co-Locations
  CREATE TABLE tmp_institutions_co_location
        SELECT parentNodes.candidateWaterAuthority AS parent,
               parentNodes.documentID,
               childNodes.candidateWaterAuthority AS child
        FROM tmp_institutions_with_documents AS parentNodes
            INNER JOIN tmp_institutions_with_documents AS childNodes
                ON parentNodes.documentID = childNodes.documentID
        WHERE parentNodes.candidateWaterAuthority < childNodes.candidateWaterAuthority
        ORDER BY parent, child;




# STEP 13
# Develop Network 
  CREATE TABLE tmp_institutions_network
      SELECT parent,
             child,
             COUNT(tmp_institutions_co_location.documentID) AS COUNT_OF_COMMON_DOCS,
             SUM(TF_IDF) AS SUM_OF_TF_IDF,
             SUM(TF_IDF)/COUNT(tmp_institutions_co_location.documentID) AS AVG_OF_TF_IDF
      FROM tmp_institutions_co_location
          INNER JOIN tmp_tf_idf_short ON tmp_institutions_co_location.documentID = tmp_tf_idf_short.documentID
      WHERE tmp_tf_idf_short.waterCategory=CATEGORY
      GROUP BY parent, child;

# Analysis
# Include the individual documents' scores
#  CREATE TABLE tmp_calc_scores_with_docs
#      SELECT tmp_calc_scores.candidateWaterAuthority,
#             tmp_calc_scores.CALC_SCORE,
#             tmp_valid_candidates_and_documents_with_score.SCORE
#      FROM tmp_calc_scores
#          INNER JOIN tmp_valid_candidates_and_documents_with_score
#              ON tmp_calc_scores.candidateWaterAuthority = tmp_valid_candidates_and_documents_with_score.candidateWaterAuthority
#      ORDER BY CALC_SCORE DESC, SCORE DESC; 

             

END;



CREATE PROCEDURE synonymize(IN ORIGINAL_WATER_AUTH VARCHAR(100), IN NEW_WATER_AUTH VARCHAR(100))

BEGIN
  DECLARE newCOT          DOUBLE;
  DECLARE newNonNNP       INT;
  DECLARE newLocKWD       INT;
  DECLARE newWaterKWD     INT;
  DECLARE newAdminTRM     INT;
  DECLARE newGeoStartTRM  INT;
  DECLARE newEndWRoad     INT;
  DECLARE newEndWAct      INT;
  DECLARE newCountEndGeo  INT;
  DECLARE newMatchName1   INT;
  DECLARE newMatchName2   INT;

  SET newCOT         = (SELECT MAX(countOfTerms)              FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newNonNNP      = (SELECT MAX(countOfNonNNPTerms)        FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newLocKWD      = (SELECT MAX(includesLocationKeyword)   FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newWaterKWD    = (SELECT MAX(includesWaterKeyword)      FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newAdminTRM    = (SELECT MAX(includesAdminTerm)         FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newGeoStartTRM = (SELECT MAX(countOfInitialGeoTerms)    FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newEndWRoad    = (SELECT MAX(endsWithRoad)              FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newEndWAct     = (SELECT MAX(endsWithAct)               FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newCountEndGeo = (SELECT MAX(countOfEndGeoFeatures)     FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newMatchName1  = (SELECT MAX(matchesNamePattern1)       FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  SET newMatchName2  = (SELECT MAX(matchesNamePattern2)       FROM candidate_water_authorities GROUP BY origCandidateWaterAuthority HAVING origCandidateWaterAuthority = NEW_WATER_AUTH);
  
  
  UPDATE candidate_water_authorities 
      SET 
          candidateWaterAuthority   = NEW_WATER_AUTH,
          countOfTerms              = newCOT,
          countOfNonNNPTerms        = newNonNNP,
          includesLocationKeyword   = newLocKWD,
          includesWaterKeyword      = newWaterKWD,
          includesAdminTerm         = newAdminTRM,
          countOfInitialGeoTerms    = newGeoStartTRM,
          endsWithRoad              = newEndWRoad,
          endsWithAct               = newEndWAct,
          countOfEndGeoFeatures     = newCountEndGeo,
          matchesNamePattern1       = newMatchName1,
          matchesNamePattern1       = newMatchName2
  WHERE origCandidateWaterAuthority = ORIGINAL_WATER_AUTH;
END;


CREATE PROCEDURE desynonymize(IN ORIGINAL_WATER_AUTH VARCHAR(100))
BEGIN
  UPDATE candidate_water_authorities 
      SET 
          candidateWaterAuthority   = origCandidateWaterAuthority,
          countOfTerms              = origCountOfTerms,
          countOfNonNNPTerms        = origCountOfNonNNPTerms,
          includesLocationKeyword   = origIncludesLocationKeyword,
          includesWaterKeyword      = origIncludesWaterKeyword,
          includesAdminTerm         = origIncludesAdminTerm,
          countOfInitialGeoTerms    = origCountOfInitialGeoTerms,
          endsWithRoad              = origEndsWithRoad,
          endsWithAct               = origEndsWithAct,
          countOfEndGeoFeatures     = origCountOfEndGeoFeatures,
          matchesNamePattern1       = origMatchesNamePattern1,
          matchesNamePattern1       = origMatchesNamePattern2
  WHERE origCandidateWaterAuthority = ORIGINAL_WATER_AUTH;
END;

CREATE PROCEDURE clear_synonymy()
BEGIN
  UPDATE candidate_water_authorities 
      SET 
          candidateWaterAuthority   = origCandidateWaterAuthority,
          countOfTerms              = origCountOfTerms,
          countOfNonNNPTerms        = origCountOfNonNNPTerms,
          includesLocationKeyword   = origIncludesLocationKeyword,
          includesWaterKeyword      = origIncludesWaterKeyword,
          includesAdminTerm         = origIncludesAdminTerm,
          countOfInitialGeoTerms    = origCountOfInitialGeoTerms,
          endsWithRoad              = origEndsWithRoad,
          endsWithAct               = origEndsWithAct,
          countOfEndGeoFeatures     = origCountOfEndGeoFeatures,
          matchesNamePattern1       = origMatchesNamePattern1,
          matchesNamePattern1       = origMatchesNamePattern2;
END;

CREATE PROCEDURE ignoreCandidate(IN WATER_AUTH VARCHAR(100))
BEGIN
  UPDATE candidate_water_authorities
      SET includeInAnalysis = 0
  WHERE candidateWaterAuthority = WATER_AUTH;
END;

CREATE PROCEDURE includeCandidate(IN WATER_AUTH VARCHAR(100))
BEGIN
  UPDATE candidate_water_authorities
      SET includeInAnalysis = 1
  WHERE candidateWaterAuthority = WATER_AUTH;
END; 

CREATE PROCEDURE includeAllCandidates()
BEGIN
  UPDATE candidate_water_authorities
      SET includeInAnalysis = 1;
END;

CREATE PROCEDURE specifyHasWaterKeyword(IN WATER_AUTH VARCHAR(100))
BEGIN
  UPDATE candidate_water_authorities
      SET includesWaterKeyword = 1
  WHERE candidateWaterAuthority = WATER_AUTH;
END;

CREATE PROCEDURE specifyHasNoWaterKeyword(IN WATER_AUTH VARCHAR(100))
BEGIN
  UPDATE candidate_water_authorities
      SET includesWaterKeyword = 0
  WHERE candidateWaterAuthority = WATER_AUTH;
END;

CREATE PROCEDURE clearSpecifiedWaterKeywords()
BEGIN
  UPDATE candidate_water_authorities
      SET includesWaterKeyword = origIncludesWaterKeyword
  WHERE origCandidateWaterAuthority = candidateWaterAuthority;
END;

CREATE PROCEDURE showSynonyms()
BEGIN
  SELECT DISTINCT 
     origCandidateWaterAuthority AS Original, 
     "HIDDEN BY" AS Status, 
     candidateWaterAuthority AS Other 
  FROM candidate_water_authorities 
  WHERE origCandidateWaterAuthority <> candidateWaterAuthority 
  ORDER BY origCandidateWaterAuthority;
END;

CREATE PROCEDURE showIgnored()
BEGIN
  SELECT DISTINCT
     origCandidateWaterAuthority AS candidate,
     IF(origCandidateWaterAuthority <> candidateWaterAuthority, "HIDDEN BY","") AS Status, 
     IF(origCandidateWaterAuthority <> candidateWaterAuthority, candidateWaterAuthority, "") AS Other
  FROM candidate_water_authorities
  WHERE NOT includeInAnalysis; 
END;


CREATE PROCEDURE baseline()
BEGIN
  call includeAllCandidates();
  call clear_synonymy();
  call clearSpecifiedWaterKeywords();
  call clear_water_authorities();
END;