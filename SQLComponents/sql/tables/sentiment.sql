#=====================================================================================================
# CNH
#
# Table for Sentiment annotations
#=====================================================================================================

CREATE TABLE sentiment(
     sentimentID INT AUTO_INCREMENT PRIMARY KEY,
     documentID INT(11) NOT NULL,
     subjectivityType VARCHAR(100) NOT NULL,
     priorPolarity VARCHAR(100) NOT NULL,
     sentimentText VARCHAR(100) NOT NULL,
     tokenIndex INT(11),
     start INT(11),
     end INT(11),
INDEX documentIDIndex (documentID),
FOREIGN KEY (documentID) REFERENCES articles(documentID) ON DELETE CASCADE);
