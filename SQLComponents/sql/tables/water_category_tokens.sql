#=====================================================================================================
# CNH
#
# Table for Water Category Tokens
#=====================================================================================================

CREATE TABLE water_category_tokens( 
     waterCategoryTokenID   INT AUTO_INCREMENT PRIMARY KEY, 
     documentID             INT(11) NOT NULL, 
     waterCategory          VARCHAR(40) NOT NULL, 
     waterCategoryTokenText VARCHAR(60), 
     startTokenIndex        INT(11), 
     endTokenIndex          INT(11), 
     start                  INT(11), 
     end                    INT(11), 
INDEX documentIDIndex (documentID), 
INDEX waterCategoryTokenIndex (waterCategory),
FOREIGN KEY (documentID) REFERENCES articles(documentID) 
ON DELETE CASCADE);
