#=====================================================================================================
# CNH
#
# Table for Sentences
#=====================================================================================================

CREATE TABLE sentences ( 
     sentenceID INT AUTO_INCREMENT PRIMARY KEY, 
     documentID INT(11) NOT NULL, 
     startTokenIndex INT(11), 
     endTokenIndex INT(11), 
     start INT(11), 
     end INT(11), 
INDEX documentIDIndex (documentID), 
FOREIGN KEY (documentID) REFERENCES articles(documentID) ON DELETE CASCADE);
