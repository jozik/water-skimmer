#=====================================================================================================
# CNH
#
# Table for Location annotations
#=====================================================================================================

CREATE TABLE locations(
     locationID INT AUTO_INCREMENT PRIMARY KEY,
     documentID INT(11) NOT NULL,
     location VARCHAR(100) NOT NULL,
     sublocation VARCHAR(100) NOT NULL,
     locationText VARCHAR(100) NOT NULL,
     startTokenIndex INT(11),
     endTokenIndex INT(11),
     start INT(11),
     end INT(11),
INDEX documentIDIndex (documentID),
FOREIGN KEY (documentID) REFERENCES articles(documentID) ON DELETE CASCADE);
