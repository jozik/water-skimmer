#=====================================================================================================
# CNH
#
# Table for Articles
#=====================================================================================================

CREATE TABLE articles ( 
     documentID      INT AUTO_INCREMENT PRIMARY KEY, 
     source          VARCHAR (200), 
     location        VARCHAR(400), 
     pubDate         DATE, 
     title           VARCHAR(500), 
     length          INT(11), 
     titleStart      INT(11), 
     titleEnd        INT(11), 
     tokenCount      INT(11));
