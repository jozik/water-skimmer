/**
 * 
 */
package cnh.annotator;

import static org.uimafit.factory.JCasFactory.createJCas;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.metadata.TypePriorities;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.junit.Before;
import org.uimafit.factory.TypeSystemDescriptionFactory;

/**
 * Base class for UIMA tests.
 * 
 * @author Nick Collier
 */
public class UIMATestBase {

	private static ThreadLocal<JCas> JCAS = new ThreadLocal<JCas>();

	private static ThreadLocal<TypeSystemDescription> TYPE_SYSTEM_DESCRIPTION = new ThreadLocal<TypeSystemDescription>();

	private static ThreadLocal<TypePriorities> TYPE_PRIORITIES = new ThreadLocal<TypePriorities>();

	static {
		try {
			List<String> descriptors = new ArrayList<String>();
			for (File file : new File("./desc").listFiles()) {
				if (file.getName().contains("TypeSystem")) {
					// descriptors.add("desc." + file.getName().substring(0,
					// file.getName().length() - 4));
					descriptors.add(file.toURI().toString());
				}
			}

			descriptors.add(new File("../Core/desc/DocumentMetaData.xml").toURI().toString());
			
			TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescriptionFromPath(descriptors.toArray(new String[0]));
			//TypeSystemDescription tsd = TypeSystemDescriptionFactory.createTypeSystemDescription("desc.CNHTypeSystem");
			//System.out.println(tsd);
			TYPE_SYSTEM_DESCRIPTION.set(tsd);

			// TypePriorities tp = createTypePriorities(DateAnnotation.class,
			// DateTimeAnnotation.class, Meeting.class, RoomNumber.class,
			// TimeAnnotation.class, UimaAcronym.class, UimaMeeting.class);
			// TYPE_PRIORITIES.set(tp);

			JCas jCas = createJCas(tsd);
			JCAS.set(jCas);
		} catch (Exception e) {
			throw new IllegalStateException(e);
		}
	}

	/*
	public static TypeSystemDescription createTypeSystemDescriptionFromPath(String... descriptorURIs) {
		TypeSystemDescription typeSystem = new TypeSystemDescription_impl();
		List<Import> imports = new ArrayList<Import>();
		for (String descriptorURI : descriptorURIs) {
			Import imp = new Import_impl();
			imp.setLocation(descriptorURI);
			imports.add(imp);
		}
		Import[] importArray = new Import[imports.size()];
		typeSystem.setImports(imports.toArray(importArray));
		return typeSystem;
	}
	*/

	protected JCas jCas;

	protected TypeSystemDescription typeSystemDescription;
	protected TypePriorities typeSystemPriorities;

	/**
	 * we do not want to create a new JCas object every time we run a test
	 * because it is expensive (~100ms on my laptop). Instead, we will have one
	 * JCas per thread sitting around that we will reset everytime a new test is
	 * called.
	 */
	@Before
	public void setUp() {
		jCas = JCAS.get();
		jCas.reset();
		typeSystemDescription = TYPE_SYSTEM_DESCRIPTION.get();
		typeSystemPriorities = TYPE_PRIORITIES.get();
	}
}
