package cnh.annotator.title;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.junit.Test;

import anl.mifs.types.DocumentMetaData;
import cnh.annotator.UIMATestBase;
import cnh.annotator.types.Title;

/**
 * Tests for title annotations.
 * 
 * @author Nick Collier
 */
public class TitleTest extends UIMATestBase {
	
	private final String TEXT = "This is the first line of the text.";
	private final String TITLE = "This is the title";
	private final String EXP_TITLE = TITLE + ". ";
	private final String EMPTY_TITLE = "";
	
	@Test 
	public void testTitle() throws UIMAException, IOException {
		DocumentMetaData data = new DocumentMetaData(jCas, 0, TEXT.length());
		data.setTitle(TITLE);
		
		TitleMerger merger = new TitleMerger();
		merger.mergeTitle(jCas, data, TEXT);
		
		Title title = (Title) jCas.getAnnotationIndex(Title.type).iterator().next();
		assertEquals(EXP_TITLE, title.getCoveredText());
		assertEquals(EXP_TITLE.length(), title.getEnd());
		assertEquals(0, title.getBegin());
		assertEquals(EXP_TITLE + TEXT, jCas.getDocumentText());
	}
	
	@Test 
	public void testEmptyTitle() throws UIMAException, IOException {
		DocumentMetaData data = new DocumentMetaData(jCas, 0, TEXT.length());
		data.setTitle(EMPTY_TITLE);
		
		TitleMerger merger = new TitleMerger();
		merger.mergeTitle(jCas, data, TEXT);
		
		Title title = (Title) jCas.getAnnotationIndex(Title.type).iterator().next();
		assertEquals("", title.getCoveredText());
		assertEquals(0, title.getEnd());
		assertEquals(0, title.getBegin());
		assertEquals(TEXT, jCas.getDocumentText());
	}

}
