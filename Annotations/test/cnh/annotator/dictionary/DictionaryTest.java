package cnh.annotator.dictionary;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import opennlp.uima.Token;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.annotator.dict_annot.dictionary.impl.DictionaryMatchImpl;
import org.apache.uima.annotator.dict_annot.dictionary.impl.EntryMetaDataImpl;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.tcas.Annotation;
import org.junit.Test;
import org.uimafit.factory.AnalysisEngineFactory;

import cnh.annotator.UIMATestBase;
import cnh.annotator.dictionary.water.Categories;
import cnh.water.annotation.InstitutionAnnotation;
import cnh.water.annotation.LocationAnnotation;
import cnh.water.annotation.WaterAnnotation;

public class DictionaryTest extends UIMATestBase {
	
	private static final String[] NOUN_TAGS = {"NN", "NNS", "NNP", "NNPS"};
	private static final String[] PROPER_NOUN_TAGS = {"NNP", "NNPS"};
	private static final String[] VERB_TAGS = {"VB", "VBD", "VBG", "VBN", "VBP", "VBZ"};
	
	private static String TEXT = "I and Mr. Copper pivot on our left feet. They found copper in the water, and brook no dissent.";
	private static String TEXT2 = "Mr. Black Water produces lots of black water";
	private static String TEXT3 = "The Central Arizona Project is near Kannah Creek";
	private static String TEXT4 = "The City of Grand Junction Public Works Dept. is fighting with the 521 Drainage Authority and" +
			" the Gunnison Basin & Grand Valley Selenium Task Forces.";
	
	private Token createToken(String posTag) {
		Token token = new Token(jCas);
		token.setPos(posTag);
		return token;
	}
	
	private boolean contains(String[] array, String val) {
		for (String str : array) {
			if (str.equals(val)) return true;
		}
		return false;
	}
	
	private void testProperNoun(TokenPredicate predicate) {
		for (String pos : PROPER_NOUN_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertFalse(predicate.apply(createToken(pos)));
		}
	}
	
	private void testNoun(TokenPredicate predicate) {
		for (String pos : NOUN_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertFalse(predicate.apply(createToken(pos)));
		}
	}
	
	@Test
	public void testCategories() {
		Categories cats = Categories.getInstance();
		assertEquals("WATER", cats.getPrimaryCategory());
		List<String> list = cats.getCategories(Categories.WATER_CATEGORIES);
		assertTrue(list.size() > 0);
		list = cats.getCategories(Categories.INSTITUTION_CATEGORIES);
		assertTrue(list.size() > 0);
		list = cats.getCategories(Categories.LOCATION_CATEGORIES);
		assertTrue(list.size() > 0);
	}
	
	@Test
	public void testPredicates() {
		TokenPredicate predicate = TokenPredicateFactory.getInstance().parseTokenString("noun");
		testNoun(predicate);
		
		predicate = TokenPredicateFactory.getInstance().parseTokenString("verb");
		for (String pos : NOUN_TAGS) {
			assertFalse(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		predicate = TokenPredicateFactory.getInstance().parseTokenString("!noun");
		for (String pos : NOUN_TAGS) {
			assertFalse(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		predicate = TokenPredicateFactory.getInstance().parseTokenString("proper_noun");
		testProperNoun(predicate);
		
		predicate = TokenPredicateFactory.getInstance().parseTokenString("noun !proper_noun");
		for (String pos : NOUN_TAGS) {
			if (contains(PROPER_NOUN_TAGS, pos)) assertFalse(predicate.apply(createToken(pos)));
			else assertTrue(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertFalse(predicate.apply(createToken(pos)));
		}
		
		predicate = TokenPredicateFactory.getInstance().parseTokenString("");
		for (String pos : PROPER_NOUN_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		for (String pos : NOUN_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
		
		for (String pos : VERB_TAGS) {
			assertTrue(predicate.apply(createToken(pos)));
		}
	}

	@Test
	public void testParser() throws FileNotFoundException, XMLStreamException {
		String file = "./test_data/test_dictionary.xml";
		DictionaryFileParser parser = new DictionaryFileParser();
		List<Dictionary> dictionaries = parser.parse(new FileInputStream(file));
		assertThat(dictionaries.size(), equalTo(2));
		Dictionary dictionary = dictionaries.get(0);

		assertThat(dictionary.getTypeName(), equalTo("cnh.water.annotation.WaterAnnotation"));
		//assertThat(dictionary.getFeatureName(), equalTo("waterCategory"));

		DictionaryMatchImpl match = new DictionaryMatchImpl();
		match.storeMatch(new EntryMetaDataImpl(1), true);
		//assertThat(dictionary.getFeatureValue(match), equalTo("FLOOD"));
		TokenPredicate predicate = dictionary.getPredicate(match);
		testProperNoun(predicate);

		match = new DictionaryMatchImpl();
		match.storeMatch(new EntryMetaDataImpl(2), true);
		//assertThat(dictionary.getFeatureValue(match), equalTo("FLOOD"));
		predicate = dictionary.getPredicate(match);
		testNoun(predicate);

		match = new DictionaryMatchImpl();
		match.storeMatch(new EntryMetaDataImpl(3), true);
		//assertThat(dictionary.getFeatureValue(match), equalTo("FLOOD"));
		predicate = dictionary.getPredicate(match);
		assertTrue(predicate instanceof TrueTokenPredicate);

		assertThat(dictionary.getEntryCount(), equalTo(3));

		String[] keys = {"cloudburst", "deluge", "downpour"};
		for (String key : keys) {
			String[] words = key.split(" ");
			assertThat(dictionary.contains(words), equalTo(true));
		}
		
		assertFalse(dictionary.contains(new String[]{"hamburger"}));
	}
	
	@Test
	public void testInstitutionAnnotation() throws UIMAException, IOException {
		AnalysisEngine eng = AnalysisEngineFactory.createAnalysisEngineFromPath("./test_desc/DictionaryTest.xml");
		jCas.setDocumentLanguage("en");
		jCas.setDocumentText(TEXT4);
		eng.process(jCas);
		
		List<InstitutionAnnotation> annotations = new ArrayList<InstitutionAnnotation>();
		for (FSIterator<Annotation> it = jCas.getAnnotationIndex(InstitutionAnnotation.type).iterator(); it.hasNext();) {
			InstitutionAnnotation annot = (InstitutionAnnotation) it.next();
			annotations.add(annot);
		}
		
	
		assertEquals(3, annotations.size());
		assertEquals(1L, annotations.get(0).getStartTokenIndex());
		assertEquals(7L, annotations.get(0).getEndTokenIndex());
		assertEquals("GRANDJUNCTION_INSTITUTIONS", annotations.get(0).getLocation());
		assertEquals("CITY OF GRAND JUNCTION PUBLIC WORKS DEPT.", annotations.get(0).getName());
		
		assertEquals(13L, annotations.get(1).getStartTokenIndex());
		assertEquals(15L, annotations.get(1).getEndTokenIndex());
		assertEquals("GRANDJUNCTION_INSTITUTIONS", annotations.get(1).getLocation());
		assertEquals("521 DRAINAGE AUTHORITY", annotations.get(1).getName());
		
		assertEquals(18L, annotations.get(2).getStartTokenIndex());
		assertEquals(25L, annotations.get(2).getEndTokenIndex());
		assertEquals("GRANDJUNCTION_INSTITUTIONS", annotations.get(2).getLocation());
		assertEquals("GUNNISON BASIN & GRAND VALLEY SELENIUM TASK FORCES", annotations.get(2).getName());
	}
	
	@Test
	public void testLocationAnnotation() throws UIMAException, IOException {
		AnalysisEngine eng = AnalysisEngineFactory.createAnalysisEngineFromPath("./test_desc/DictionaryTest.xml");
		jCas.setDocumentLanguage("en");
		jCas.setDocumentText(TEXT3);
		eng.process(jCas);
		
		List<LocationAnnotation> annotations = new ArrayList<LocationAnnotation>();
		for (FSIterator<Annotation> it = jCas.getAnnotationIndex(LocationAnnotation.type).iterator(); it.hasNext();) {
			LocationAnnotation annot = (LocationAnnotation) it.next();
			annotations.add(annot);
		}
		
		assertEquals(3, annotations.size());
		assertEquals(1L, annotations.get(0).getStartTokenIndex());
		assertEquals(3L, annotations.get(0).getEndTokenIndex());
		assertEquals("PHOENIX_LOCATIONS", annotations.get(0).getLocation());
		assertEquals("CENTRAL ARIZONA PROJECT", annotations.get(0).getSublocation());
		
		assertEquals(1L, annotations.get(1).getStartTokenIndex());
		assertEquals(3L, annotations.get(1).getEndTokenIndex());
		assertEquals("TUCSON_LOCATIONS", annotations.get(1).getLocation());
		assertEquals("CENTRAL ARIZONA PROJECT", annotations.get(1).getSublocation());
		
		assertEquals(6L, annotations.get(2).getStartTokenIndex());
		assertEquals(7L, annotations.get(2).getEndTokenIndex());
		assertEquals("GRANDJUNCTION_LOCATIONS", annotations.get(2).getLocation());
		assertEquals("KANNAH CREEK", annotations.get(2).getSublocation());
	}
	
	@Test
	public void testDictionaryAnnotatoryMultiToken()  throws UIMAException, IOException {
		AnalysisEngine eng = AnalysisEngineFactory.createAnalysisEngineFromPath("./test_desc/DictionaryTest.xml");
		jCas.setDocumentLanguage("en");
		jCas.setDocumentText(TEXT2);
		eng.process(jCas);
		
		List<WaterAnnotation> annotations = new ArrayList<WaterAnnotation>();
		for (FSIterator<Annotation> it = jCas.getAnnotationIndex(WaterAnnotation.type).iterator(); it.hasNext();) {
			WaterAnnotation annot = (WaterAnnotation) it.next();
			annotations.add(annot);
		}
		
		// Black Water should not be tagged as WQUALITY or WATER
		// so all just have the two at the end of the text.
		assertEquals(2, annotations.size());
		WaterAnnotation waterA = annotations.get(0);
		assertEquals(6L, waterA.getStartTokenIndex());
		assertEquals(7L, waterA.getEndTokenIndex());
		assertEquals("WQUALITY", waterA.getWaterCategory());
		
		waterA = annotations.get(1);
		assertEquals(7L, waterA.getStartTokenIndex());
		assertEquals(7L, waterA.getEndTokenIndex());
		assertEquals("WATER", waterA.getWaterCategory());
	}
	
	@Test
	public void testDictionaryAnnotator() throws UIMAException, IOException {
		AnalysisEngine eng = AnalysisEngineFactory.createAnalysisEngineFromPath("./test_desc/DictionaryTest.xml");
		jCas.setDocumentLanguage("en");
		jCas.setDocumentText(TEXT);
		eng.process(jCas);
		
		List<WaterAnnotation> annotations = new ArrayList<WaterAnnotation>();
		for (FSIterator<Annotation> it = jCas.getAnnotationIndex(WaterAnnotation.type).iterator(); it.hasNext();) {
			WaterAnnotation annot = (WaterAnnotation) it.next();
			annotations.add(annot);
		}
		
		// we are testing that Mr. Copper doesn't show up as keyword
		// because of the !proper_noun constraint, and also for pivot
		// and brook as not verbs
		assertEquals(3, annotations.size());
		assertEquals(12L, annotations.get(0).getStartTokenIndex());
		assertEquals("METALS", annotations.get(0).getWaterCategory());
		assertEquals(12L, annotations.get(1).getStartTokenIndex());
		assertEquals("WQUALITY", annotations.get(1).getWaterCategory());
		assertEquals(15L, annotations.get(2).getStartTokenIndex());
		assertEquals("WATER", annotations.get(2).getWaterCategory());
	}
}
