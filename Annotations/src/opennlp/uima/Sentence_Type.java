
/* First created by JCasGen Mon Jul 16 13:24:04 EDT 2012 */
package opennlp.uima;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Wed Jul 18 10:47:28 EDT 2012
 * @generated */
public class Sentence_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Sentence_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Sentence_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Sentence(addr, Sentence_Type.this);
  			   Sentence_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Sentence(addr, Sentence_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Sentence.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("opennlp.uima.Sentence");



  /** @generated */
  final Feature casFeat_startTokenIndex;
  /** @generated */
  final int     casFeatCode_startTokenIndex;
  /** @generated */ 
  public int getStartTokenIndex(int addr) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "opennlp.uima.Sentence");
    return ll_cas.ll_getIntValue(addr, casFeatCode_startTokenIndex);
  }
  /** @generated */    
  public void setStartTokenIndex(int addr, int v) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "opennlp.uima.Sentence");
    ll_cas.ll_setIntValue(addr, casFeatCode_startTokenIndex, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endTokenIndex;
  /** @generated */
  final int     casFeatCode_endTokenIndex;
  /** @generated */ 
  public int getEndTokenIndex(int addr) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "opennlp.uima.Sentence");
    return ll_cas.ll_getIntValue(addr, casFeatCode_endTokenIndex);
  }
  /** @generated */    
  public void setEndTokenIndex(int addr, int v) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "opennlp.uima.Sentence");
    ll_cas.ll_setIntValue(addr, casFeatCode_endTokenIndex, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Sentence_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_startTokenIndex = jcas.getRequiredFeatureDE(casType, "startTokenIndex", "uima.cas.Integer", featOkTst);
    casFeatCode_startTokenIndex  = (null == casFeat_startTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_startTokenIndex).getCode();

 
    casFeat_endTokenIndex = jcas.getRequiredFeatureDE(casType, "endTokenIndex", "uima.cas.Integer", featOkTst);
    casFeatCode_endTokenIndex  = (null == casFeat_endTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endTokenIndex).getCode();

  }
}



    