

/* First created by JCasGen Mon Jul 16 13:24:04 EDT 2012 */
package opennlp.uima;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed Jul 18 10:47:28 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/OpenNLPTypeSystem.xml
 * @generated */
public class Sentence extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Sentence.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Sentence() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Sentence(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Sentence(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Sentence(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: startTokenIndex

  /** getter for startTokenIndex - gets 
   * @generated */
  public int getStartTokenIndex() {
    if (Sentence_Type.featOkTst && ((Sentence_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "opennlp.uima.Sentence");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Sentence_Type)jcasType).casFeatCode_startTokenIndex);}
    
  /** setter for startTokenIndex - sets  
   * @generated */
  public void setStartTokenIndex(int v) {
    if (Sentence_Type.featOkTst && ((Sentence_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "opennlp.uima.Sentence");
    jcasType.ll_cas.ll_setIntValue(addr, ((Sentence_Type)jcasType).casFeatCode_startTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: endTokenIndex

  /** getter for endTokenIndex - gets 
   * @generated */
  public int getEndTokenIndex() {
    if (Sentence_Type.featOkTst && ((Sentence_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "opennlp.uima.Sentence");
    return jcasType.ll_cas.ll_getIntValue(addr, ((Sentence_Type)jcasType).casFeatCode_endTokenIndex);}
    
  /** setter for endTokenIndex - sets  
   * @generated */
  public void setEndTokenIndex(int v) {
    if (Sentence_Type.featOkTst && ((Sentence_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "opennlp.uima.Sentence");
    jcasType.ll_cas.ll_setIntValue(addr, ((Sentence_Type)jcasType).casFeatCode_endTokenIndex, v);}    
  }

    