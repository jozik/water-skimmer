
/* First created by JCasGen Thu Oct 11 14:46:58 EDT 2012 */
package cnh.water.annotation;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Thu Oct 11 15:17:54 EDT 2012
 * @generated */
public class InstitutionAnnotation_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (InstitutionAnnotation_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = InstitutionAnnotation_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new InstitutionAnnotation(addr, InstitutionAnnotation_Type.this);
  			   InstitutionAnnotation_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new InstitutionAnnotation(addr, InstitutionAnnotation_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = InstitutionAnnotation.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.water.annotation.InstitutionAnnotation");
 
  /** @generated */
  final Feature casFeat_location;
  /** @generated */
  final int     casFeatCode_location;
  /** @generated */ 
  public String getLocation(int addr) {
        if (featOkTst && casFeat_location == null)
      jcas.throwFeatMissing("location", "cnh.water.annotation.InstitutionAnnotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_location);
  }
  /** @generated */    
  public void setLocation(int addr, String v) {
        if (featOkTst && casFeat_location == null)
      jcas.throwFeatMissing("location", "cnh.water.annotation.InstitutionAnnotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_location, v);}
    
  
 
  /** @generated */
  final Feature casFeat_name;
  /** @generated */
  final int     casFeatCode_name;
  /** @generated */ 
  public String getName(int addr) {
        if (featOkTst && casFeat_name == null)
      jcas.throwFeatMissing("name", "cnh.water.annotation.InstitutionAnnotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_name);
  }
  /** @generated */    
  public void setName(int addr, String v) {
        if (featOkTst && casFeat_name == null)
      jcas.throwFeatMissing("name", "cnh.water.annotation.InstitutionAnnotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_name, v);}
    
  
 
  /** @generated */
  final Feature casFeat_startTokenIndex;
  /** @generated */
  final int     casFeatCode_startTokenIndex;
  /** @generated */ 
  public long getStartTokenIndex(int addr) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    return ll_cas.ll_getLongValue(addr, casFeatCode_startTokenIndex);
  }
  /** @generated */    
  public void setStartTokenIndex(int addr, long v) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    ll_cas.ll_setLongValue(addr, casFeatCode_startTokenIndex, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endTokenIndex;
  /** @generated */
  final int     casFeatCode_endTokenIndex;
  /** @generated */ 
  public long getEndTokenIndex(int addr) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    return ll_cas.ll_getLongValue(addr, casFeatCode_endTokenIndex);
  }
  /** @generated */    
  public void setEndTokenIndex(int addr, long v) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    ll_cas.ll_setLongValue(addr, casFeatCode_endTokenIndex, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public InstitutionAnnotation_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_location = jcas.getRequiredFeatureDE(casType, "location", "uima.cas.String", featOkTst);
    casFeatCode_location  = (null == casFeat_location) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_location).getCode();

 
    casFeat_name = jcas.getRequiredFeatureDE(casType, "name", "uima.cas.String", featOkTst);
    casFeatCode_name  = (null == casFeat_name) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_name).getCode();

 
    casFeat_startTokenIndex = jcas.getRequiredFeatureDE(casType, "startTokenIndex", "uima.cas.Long", featOkTst);
    casFeatCode_startTokenIndex  = (null == casFeat_startTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_startTokenIndex).getCode();

 
    casFeat_endTokenIndex = jcas.getRequiredFeatureDE(casType, "endTokenIndex", "uima.cas.Long", featOkTst);
    casFeatCode_endTokenIndex  = (null == casFeat_endTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endTokenIndex).getCode();

  }
}



    