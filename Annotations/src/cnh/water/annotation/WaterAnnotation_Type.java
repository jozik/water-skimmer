
/* First created by JCasGen Mon Jul 16 17:10:33 EDT 2012 */
package cnh.water.annotation;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Thu Oct 11 15:17:54 EDT 2012
 * @generated */
public class WaterAnnotation_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (WaterAnnotation_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = WaterAnnotation_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new WaterAnnotation(addr, WaterAnnotation_Type.this);
  			   WaterAnnotation_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new WaterAnnotation(addr, WaterAnnotation_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = WaterAnnotation.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.water.annotation.WaterAnnotation");
 
  /** @generated */
  final Feature casFeat_waterCategory;
  /** @generated */
  final int     casFeatCode_waterCategory;
  /** @generated */ 
  public String getWaterCategory(int addr) {
        if (featOkTst && casFeat_waterCategory == null)
      jcas.throwFeatMissing("waterCategory", "cnh.water.annotation.WaterAnnotation");
    return ll_cas.ll_getStringValue(addr, casFeatCode_waterCategory);
  }
  /** @generated */    
  public void setWaterCategory(int addr, String v) {
        if (featOkTst && casFeat_waterCategory == null)
      jcas.throwFeatMissing("waterCategory", "cnh.water.annotation.WaterAnnotation");
    ll_cas.ll_setStringValue(addr, casFeatCode_waterCategory, v);}
    
  
 
  /** @generated */
  final Feature casFeat_startTokenIndex;
  /** @generated */
  final int     casFeatCode_startTokenIndex;
  /** @generated */ 
  public long getStartTokenIndex(int addr) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.WaterAnnotation");
    return ll_cas.ll_getLongValue(addr, casFeatCode_startTokenIndex);
  }
  /** @generated */    
  public void setStartTokenIndex(int addr, long v) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.WaterAnnotation");
    ll_cas.ll_setLongValue(addr, casFeatCode_startTokenIndex, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endTokenIndex;
  /** @generated */
  final int     casFeatCode_endTokenIndex;
  /** @generated */ 
  public long getEndTokenIndex(int addr) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.WaterAnnotation");
    return ll_cas.ll_getLongValue(addr, casFeatCode_endTokenIndex);
  }
  /** @generated */    
  public void setEndTokenIndex(int addr, long v) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.WaterAnnotation");
    ll_cas.ll_setLongValue(addr, casFeatCode_endTokenIndex, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public WaterAnnotation_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_waterCategory = jcas.getRequiredFeatureDE(casType, "waterCategory", "uima.cas.String", featOkTst);
    casFeatCode_waterCategory  = (null == casFeat_waterCategory) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_waterCategory).getCode();

 
    casFeat_startTokenIndex = jcas.getRequiredFeatureDE(casType, "startTokenIndex", "uima.cas.Long", featOkTst);
    casFeatCode_startTokenIndex  = (null == casFeat_startTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_startTokenIndex).getCode();

 
    casFeat_endTokenIndex = jcas.getRequiredFeatureDE(casType, "endTokenIndex", "uima.cas.Long", featOkTst);
    casFeatCode_endTokenIndex  = (null == casFeat_endTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endTokenIndex).getCode();

  }
}



    