

/* First created by JCasGen Thu Sep 27 16:19:34 EDT 2012 */
package cnh.water.annotation;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Oct 11 15:17:54 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/WaterAnnotationTypeSystem.xml
 * @generated */
public class LocationAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(LocationAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected LocationAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public LocationAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public LocationAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public LocationAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: location

  /** getter for location - gets 
   * @generated */
  public String getLocation() {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "cnh.water.annotation.LocationAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_location);}
    
  /** setter for location - sets  
   * @generated */
  public void setLocation(String v) {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "cnh.water.annotation.LocationAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_location, v);}    
   
    
  //*--------------*
  //* Feature: sublocation

  /** getter for sublocation - gets 
   * @generated */
  public String getSublocation() {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_sublocation == null)
      jcasType.jcas.throwFeatMissing("sublocation", "cnh.water.annotation.LocationAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_sublocation);}
    
  /** setter for sublocation - sets  
   * @generated */
  public void setSublocation(String v) {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_sublocation == null)
      jcasType.jcas.throwFeatMissing("sublocation", "cnh.water.annotation.LocationAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_sublocation, v);}    
   
    
  //*--------------*
  //* Feature: startTokenIndex

  /** getter for startTokenIndex - gets 
   * @generated */
  public long getStartTokenIndex() {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.LocationAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_startTokenIndex);}
    
  /** setter for startTokenIndex - sets  
   * @generated */
  public void setStartTokenIndex(long v) {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.LocationAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_startTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: endTokenIndex

  /** getter for endTokenIndex - gets 
   * @generated */
  public long getEndTokenIndex() {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.LocationAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_endTokenIndex);}
    
  /** setter for endTokenIndex - sets  
   * @generated */
  public void setEndTokenIndex(long v) {
    if (LocationAnnotation_Type.featOkTst && ((LocationAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.LocationAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((LocationAnnotation_Type)jcasType).casFeatCode_endTokenIndex, v);}    
  }

    