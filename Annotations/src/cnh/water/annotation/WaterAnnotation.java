

/* First created by JCasGen Mon Jul 16 17:10:33 EDT 2012 */
package cnh.water.annotation;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Oct 11 15:17:54 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/WaterAnnotationTypeSystem.xml
 * @generated */
public class WaterAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(WaterAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected WaterAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public WaterAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public WaterAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public WaterAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: waterCategory

  /** getter for waterCategory - gets 
   * @generated */
  public String getWaterCategory() {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_waterCategory == null)
      jcasType.jcas.throwFeatMissing("waterCategory", "cnh.water.annotation.WaterAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_waterCategory);}
    
  /** setter for waterCategory - sets  
   * @generated */
  public void setWaterCategory(String v) {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_waterCategory == null)
      jcasType.jcas.throwFeatMissing("waterCategory", "cnh.water.annotation.WaterAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_waterCategory, v);}    
   
    
  //*--------------*
  //* Feature: startTokenIndex

  /** getter for startTokenIndex - gets The index of the first token in the annotation.
   * @generated */
  public long getStartTokenIndex() {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.WaterAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_startTokenIndex);}
    
  /** setter for startTokenIndex - sets The index of the first token in the annotation. 
   * @generated */
  public void setStartTokenIndex(long v) {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.WaterAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_startTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: endTokenIndex

  /** getter for endTokenIndex - gets The index of the last token in the annotation.
   * @generated */
  public long getEndTokenIndex() {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.WaterAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_endTokenIndex);}
    
  /** setter for endTokenIndex - sets The index of the last token in the annotation. 
   * @generated */
  public void setEndTokenIndex(long v) {
    if (WaterAnnotation_Type.featOkTst && ((WaterAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.WaterAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((WaterAnnotation_Type)jcasType).casFeatCode_endTokenIndex, v);}    
  }

    