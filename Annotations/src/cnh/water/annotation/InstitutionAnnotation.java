

/* First created by JCasGen Thu Oct 11 14:46:58 EDT 2012 */
package cnh.water.annotation;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Oct 11 15:17:54 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/WaterAnnotationTypeSystem.xml
 * @generated */
public class InstitutionAnnotation extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(InstitutionAnnotation.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected InstitutionAnnotation() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public InstitutionAnnotation(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public InstitutionAnnotation(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public InstitutionAnnotation(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: location

  /** getter for location - gets 
   * @generated */
  public String getLocation() {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "cnh.water.annotation.InstitutionAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_location);}
    
  /** setter for location - sets  
   * @generated */
  public void setLocation(String v) {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_location == null)
      jcasType.jcas.throwFeatMissing("location", "cnh.water.annotation.InstitutionAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_location, v);}    
   
    
  //*--------------*
  //* Feature: name

  /** getter for name - gets 
   * @generated */
  public String getName() {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_name == null)
      jcasType.jcas.throwFeatMissing("name", "cnh.water.annotation.InstitutionAnnotation");
    return jcasType.ll_cas.ll_getStringValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_name);}
    
  /** setter for name - sets  
   * @generated */
  public void setName(String v) {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_name == null)
      jcasType.jcas.throwFeatMissing("name", "cnh.water.annotation.InstitutionAnnotation");
    jcasType.ll_cas.ll_setStringValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_name, v);}    
   
    
  //*--------------*
  //* Feature: startTokenIndex

  /** getter for startTokenIndex - gets 
   * @generated */
  public long getStartTokenIndex() {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_startTokenIndex);}
    
  /** setter for startTokenIndex - sets  
   * @generated */
  public void setStartTokenIndex(long v) {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_startTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: endTokenIndex

  /** getter for endTokenIndex - gets 
   * @generated */
  public long getEndTokenIndex() {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    return jcasType.ll_cas.ll_getLongValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_endTokenIndex);}
    
  /** setter for endTokenIndex - sets  
   * @generated */
  public void setEndTokenIndex(long v) {
    if (InstitutionAnnotation_Type.featOkTst && ((InstitutionAnnotation_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.water.annotation.InstitutionAnnotation");
    jcasType.ll_cas.ll_setLongValue(addr, ((InstitutionAnnotation_Type)jcasType).casFeatCode_endTokenIndex, v);}    
  }

    