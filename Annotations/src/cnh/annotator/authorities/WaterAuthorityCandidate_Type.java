
/* First created by JCasGen Mon Nov 12 14:14:29 AKST 2012 */
package cnh.annotator.authorities;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Wed May 22 09:13:43 CDT 2013
 * @generated */
public class WaterAuthorityCandidate_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (WaterAuthorityCandidate_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = WaterAuthorityCandidate_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new WaterAuthorityCandidate(addr, WaterAuthorityCandidate_Type.this);
  			   WaterAuthorityCandidate_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new WaterAuthorityCandidate(addr, WaterAuthorityCandidate_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = WaterAuthorityCandidate.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.annotator.authorities.WaterAuthorityCandidate");



  /** @generated */
  final Feature casFeat_start;
  /** @generated */
  final int     casFeatCode_start;
  /** @generated */ 
  public int getStart(int addr) {
        if (featOkTst && casFeat_start == null)
      jcas.throwFeatMissing("start", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_start);
  }
  /** @generated */    
  public void setStart(int addr, int v) {
        if (featOkTst && casFeat_start == null)
      jcas.throwFeatMissing("start", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_start, v);}
    
  
 
  /** @generated */
  final Feature casFeat_end;
  /** @generated */
  final int     casFeatCode_end;
  /** @generated */ 
  public int getEnd(int addr) {
        if (featOkTst && casFeat_end == null)
      jcas.throwFeatMissing("end", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_end);
  }
  /** @generated */    
  public void setEnd(int addr, int v) {
        if (featOkTst && casFeat_end == null)
      jcas.throwFeatMissing("end", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_end, v);}
    
  
 
  /** @generated */
  final Feature casFeat_startTokenIndex;
  /** @generated */
  final int     casFeatCode_startTokenIndex;
  /** @generated */ 
  public int getStartTokenIndex(int addr) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_startTokenIndex);
  }
  /** @generated */    
  public void setStartTokenIndex(int addr, int v) {
        if (featOkTst && casFeat_startTokenIndex == null)
      jcas.throwFeatMissing("startTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_startTokenIndex, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endTokenIndex;
  /** @generated */
  final int     casFeatCode_endTokenIndex;
  /** @generated */ 
  public int getEndTokenIndex(int addr) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_endTokenIndex);
  }
  /** @generated */    
  public void setEndTokenIndex(int addr, int v) {
        if (featOkTst && casFeat_endTokenIndex == null)
      jcas.throwFeatMissing("endTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_endTokenIndex, v);}
    
  
 
  /** @generated */
  final Feature casFeat_fullText;
  /** @generated */
  final int     casFeatCode_fullText;
  /** @generated */ 
  public String getFullText(int addr) {
        if (featOkTst && casFeat_fullText == null)
      jcas.throwFeatMissing("fullText", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getStringValue(addr, casFeatCode_fullText);
  }
  /** @generated */    
  public void setFullText(int addr, String v) {
        if (featOkTst && casFeat_fullText == null)
      jcas.throwFeatMissing("fullText", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setStringValue(addr, casFeatCode_fullText, v);}
    
  
 
  /** @generated */
  final Feature casFeat_personScore;
  /** @generated */
  final int     casFeatCode_personScore;
  /** @generated */ 
  public double getPersonScore(int addr) {
        if (featOkTst && casFeat_personScore == null)
      jcas.throwFeatMissing("personScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_personScore);
  }
  /** @generated */    
  public void setPersonScore(int addr, double v) {
        if (featOkTst && casFeat_personScore == null)
      jcas.throwFeatMissing("personScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_personScore, v);}
    
  
 
  /** @generated */
  final Feature casFeat_waterScore;
  /** @generated */
  final int     casFeatCode_waterScore;
  /** @generated */ 
  public double getWaterScore(int addr) {
        if (featOkTst && casFeat_waterScore == null)
      jcas.throwFeatMissing("waterScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_waterScore);
  }
  /** @generated */    
  public void setWaterScore(int addr, double v) {
        if (featOkTst && casFeat_waterScore == null)
      jcas.throwFeatMissing("waterScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_waterScore, v);}
    
  



  /** @generated */
  final Feature casFeat_countOfTerms;
  /** @generated */
  final int     casFeatCode_countOfTerms;
  /** @generated */ 
  public int getCountOfTerms(int addr) {
        if (featOkTst && casFeat_countOfTerms == null)
      jcas.throwFeatMissing("countOfTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_countOfTerms);
  }
  /** @generated */    
  public void setCountOfTerms(int addr, int v) {
        if (featOkTst && casFeat_countOfTerms == null)
      jcas.throwFeatMissing("countOfTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_countOfTerms, v);}
    
  
 
  /** @generated */
  final Feature casFeat_countOfNonNNPTerms;
  /** @generated */
  final int     casFeatCode_countOfNonNNPTerms;
  /** @generated */ 
  public int getCountOfNonNNPTerms(int addr) {
        if (featOkTst && casFeat_countOfNonNNPTerms == null)
      jcas.throwFeatMissing("countOfNonNNPTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_countOfNonNNPTerms);
  }
  /** @generated */    
  public void setCountOfNonNNPTerms(int addr, int v) {
        if (featOkTst && casFeat_countOfNonNNPTerms == null)
      jcas.throwFeatMissing("countOfNonNNPTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_countOfNonNNPTerms, v);}
    
  
 
  /** @generated */
  final Feature casFeat_precededByDefiniteArticle;
  /** @generated */
  final int     casFeatCode_precededByDefiniteArticle;
  /** @generated */ 
  public int getPrecededByDefiniteArticle(int addr) {
        if (featOkTst && casFeat_precededByDefiniteArticle == null)
      jcas.throwFeatMissing("precededByDefiniteArticle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_precededByDefiniteArticle);
  }
  /** @generated */    
  public void setPrecededByDefiniteArticle(int addr, int v) {
        if (featOkTst && casFeat_precededByDefiniteArticle == null)
      jcas.throwFeatMissing("precededByDefiniteArticle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_precededByDefiniteArticle, v);}
    
  
 
  /** @generated */
  final Feature casFeat_precededByPersonalTitle;
  /** @generated */
  final int     casFeatCode_precededByPersonalTitle;
  /** @generated */ 
  public int getPrecededByPersonalTitle(int addr) {
        if (featOkTst && casFeat_precededByPersonalTitle == null)
      jcas.throwFeatMissing("precededByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_precededByPersonalTitle);
  }
  /** @generated */    
  public void setPrecededByPersonalTitle(int addr, int v) {
        if (featOkTst && casFeat_precededByPersonalTitle == null)
      jcas.throwFeatMissing("precededByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_precededByPersonalTitle, v);}
    
  
 
  /** @generated */
  final Feature casFeat_hasRepresentative;
  /** @generated */
  final int     casFeatCode_hasRepresentative;
  /** @generated */ 
  public int getHasRepresentative(int addr) {
        if (featOkTst && casFeat_hasRepresentative == null)
      jcas.throwFeatMissing("hasRepresentative", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_hasRepresentative);
  }
  /** @generated */    
  public void setHasRepresentative(int addr, int v) {
        if (featOkTst && casFeat_hasRepresentative == null)
      jcas.throwFeatMissing("hasRepresentative", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_hasRepresentative, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endsWithAct;
  /** @generated */
  final int     casFeatCode_endsWithAct;
  /** @generated */ 
  public int getEndsWithAct(int addr) {
        if (featOkTst && casFeat_endsWithAct == null)
      jcas.throwFeatMissing("endsWithAct", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_endsWithAct);
  }
  /** @generated */    
  public void setEndsWithAct(int addr, int v) {
        if (featOkTst && casFeat_endsWithAct == null)
      jcas.throwFeatMissing("endsWithAct", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_endsWithAct, v);}
    
  
 
  /** @generated */
  final Feature casFeat_includesLocationKeyword;
  /** @generated */
  final int     casFeatCode_includesLocationKeyword;
  /** @generated */ 
  public int getIncludesLocationKeyword(int addr) {
        if (featOkTst && casFeat_includesLocationKeyword == null)
      jcas.throwFeatMissing("includesLocationKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_includesLocationKeyword);
  }
  /** @generated */    
  public void setIncludesLocationKeyword(int addr, int v) {
        if (featOkTst && casFeat_includesLocationKeyword == null)
      jcas.throwFeatMissing("includesLocationKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_includesLocationKeyword, v);}
    
  
 
  /** @generated */
  final Feature casFeat_includesWaterKeyword;
  /** @generated */
  final int     casFeatCode_includesWaterKeyword;
  /** @generated */ 
  public int getIncludesWaterKeyword(int addr) {
        if (featOkTst && casFeat_includesWaterKeyword == null)
      jcas.throwFeatMissing("includesWaterKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_includesWaterKeyword);
  }
  /** @generated */    
  public void setIncludesWaterKeyword(int addr, int v) {
        if (featOkTst && casFeat_includesWaterKeyword == null)
      jcas.throwFeatMissing("includesWaterKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_includesWaterKeyword, v);}
    
  
 
  /** @generated */
  final Feature casFeat_includesAdminTerm;
  /** @generated */
  final int     casFeatCode_includesAdminTerm;
  /** @generated */ 
  public int getIncludesAdminTerm(int addr) {
        if (featOkTst && casFeat_includesAdminTerm == null)
      jcas.throwFeatMissing("includesAdminTerm", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_includesAdminTerm);
  }
  /** @generated */    
  public void setIncludesAdminTerm(int addr, int v) {
        if (featOkTst && casFeat_includesAdminTerm == null)
      jcas.throwFeatMissing("includesAdminTerm", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_includesAdminTerm, v);}
    
  
 
  /** @generated */
  final Feature casFeat_countOfInitialGeoTerms;
  /** @generated */
  final int     casFeatCode_countOfInitialGeoTerms;
  /** @generated */ 
  public int getCountOfInitialGeoTerms(int addr) {
        if (featOkTst && casFeat_countOfInitialGeoTerms == null)
      jcas.throwFeatMissing("countOfInitialGeoTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_countOfInitialGeoTerms);
  }
  /** @generated */    
  public void setCountOfInitialGeoTerms(int addr, int v) {
        if (featOkTst && casFeat_countOfInitialGeoTerms == null)
      jcas.throwFeatMissing("countOfInitialGeoTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_countOfInitialGeoTerms, v);}
    
  
 
  /** @generated */
  final Feature casFeat_endsWithRoad;
  /** @generated */
  final int     casFeatCode_endsWithRoad;
  /** @generated */ 
  public int getEndsWithRoad(int addr) {
        if (featOkTst && casFeat_endsWithRoad == null)
      jcas.throwFeatMissing("endsWithRoad", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_endsWithRoad);
  }
  /** @generated */    
  public void setEndsWithRoad(int addr, int v) {
        if (featOkTst && casFeat_endsWithRoad == null)
      jcas.throwFeatMissing("endsWithRoad", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_endsWithRoad, v);}
    
  
 
  /** @generated */
  final Feature casFeat_countOfEndGeoFeatures;
  /** @generated */
  final int     casFeatCode_countOfEndGeoFeatures;
  /** @generated */ 
  public int getCountOfEndGeoFeatures(int addr) {
        if (featOkTst && casFeat_countOfEndGeoFeatures == null)
      jcas.throwFeatMissing("countOfEndGeoFeatures", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_countOfEndGeoFeatures);
  }
  /** @generated */    
  public void setCountOfEndGeoFeatures(int addr, int v) {
        if (featOkTst && casFeat_countOfEndGeoFeatures == null)
      jcas.throwFeatMissing("countOfEndGeoFeatures", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_countOfEndGeoFeatures, v);}
    
  
 
  /** @generated */
  final Feature casFeat_matchesName1;
  /** @generated */
  final int     casFeatCode_matchesName1;
  /** @generated */ 
  public int getMatchesName1(int addr) {
        if (featOkTst && casFeat_matchesName1 == null)
      jcas.throwFeatMissing("matchesName1", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_matchesName1);
  }
  /** @generated */    
  public void setMatchesName1(int addr, int v) {
        if (featOkTst && casFeat_matchesName1 == null)
      jcas.throwFeatMissing("matchesName1", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_matchesName1, v);}
    
  
 
  /** @generated */
  final Feature casFeat_matchesName2;
  /** @generated */
  final int     casFeatCode_matchesName2;
  /** @generated */ 
  public int getMatchesName2(int addr) {
        if (featOkTst && casFeat_matchesName2 == null)
      jcas.throwFeatMissing("matchesName2", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_matchesName2);
  }
  /** @generated */    
  public void setMatchesName2(int addr, int v) {
        if (featOkTst && casFeat_matchesName2 == null)
      jcas.throwFeatMissing("matchesName2", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_matchesName2, v);}
    
  
 
  /** @generated */
  final Feature casFeat_followedByAcronym;
  /** @generated */
  final int     casFeatCode_followedByAcronym;
  /** @generated */ 
  public int getFollowedByAcronym(int addr) {
        if (featOkTst && casFeat_followedByAcronym == null)
      jcas.throwFeatMissing("followedByAcronym", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_followedByAcronym);
  }
  /** @generated */    
  public void setFollowedByAcronym(int addr, int v) {
        if (featOkTst && casFeat_followedByAcronym == null)
      jcas.throwFeatMissing("followedByAcronym", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_followedByAcronym, v);}
    
  
 
  /** @generated */
  final Feature casFeat_followedByPersonalTitle;
  /** @generated */
  final int     casFeatCode_followedByPersonalTitle;
  /** @generated */ 
  public int getFollowedByPersonalTitle(int addr) {
        if (featOkTst && casFeat_followedByPersonalTitle == null)
      jcas.throwFeatMissing("followedByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getIntValue(addr, casFeatCode_followedByPersonalTitle);
  }
  /** @generated */    
  public void setFollowedByPersonalTitle(int addr, int v) {
        if (featOkTst && casFeat_followedByPersonalTitle == null)
      jcas.throwFeatMissing("followedByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setIntValue(addr, casFeatCode_followedByPersonalTitle, v);}
    
  
 
  /** @generated */
  final Feature casFeat_institutionScore;
  /** @generated */
  final int     casFeatCode_institutionScore;
  /** @generated */ 
  public double getInstitutionScore(int addr) {
        if (featOkTst && casFeat_institutionScore == null)
      jcas.throwFeatMissing("institutionScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return ll_cas.ll_getDoubleValue(addr, casFeatCode_institutionScore);
  }
  /** @generated */    
  public void setInstitutionScore(int addr, double v) {
        if (featOkTst && casFeat_institutionScore == null)
      jcas.throwFeatMissing("institutionScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    ll_cas.ll_setDoubleValue(addr, casFeatCode_institutionScore, v);}
    
  
 
  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public WaterAuthorityCandidate_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_start = jcas.getRequiredFeatureDE(casType, "start", "uima.cas.Integer", featOkTst);
    casFeatCode_start  = (null == casFeat_start) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_start).getCode();

 
    casFeat_end = jcas.getRequiredFeatureDE(casType, "end", "uima.cas.Integer", featOkTst);
    casFeatCode_end  = (null == casFeat_end) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_end).getCode();

 
    casFeat_startTokenIndex = jcas.getRequiredFeatureDE(casType, "startTokenIndex", "uima.cas.Integer", featOkTst);
    casFeatCode_startTokenIndex  = (null == casFeat_startTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_startTokenIndex).getCode();

 
    casFeat_endTokenIndex = jcas.getRequiredFeatureDE(casType, "endTokenIndex", "uima.cas.Integer", featOkTst);
    casFeatCode_endTokenIndex  = (null == casFeat_endTokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endTokenIndex).getCode();

 
    casFeat_fullText = jcas.getRequiredFeatureDE(casType, "fullText", "uima.cas.String", featOkTst);
    casFeatCode_fullText  = (null == casFeat_fullText) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_fullText).getCode();

 
    casFeat_countOfTerms = jcas.getRequiredFeatureDE(casType, "countOfTerms", "uima.cas.Integer", featOkTst);
    casFeatCode_countOfTerms  = (null == casFeat_countOfTerms) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_countOfTerms).getCode();

 
    casFeat_countOfNonNNPTerms = jcas.getRequiredFeatureDE(casType, "countOfNonNNPTerms", "uima.cas.Integer", featOkTst);
    casFeatCode_countOfNonNNPTerms  = (null == casFeat_countOfNonNNPTerms) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_countOfNonNNPTerms).getCode();

 
    casFeat_includesLocationKeyword = jcas.getRequiredFeatureDE(casType, "includesLocationKeyword", "uima.cas.Integer", featOkTst);
    casFeatCode_includesLocationKeyword  = (null == casFeat_includesLocationKeyword) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_includesLocationKeyword).getCode();

 
    casFeat_includesWaterKeyword = jcas.getRequiredFeatureDE(casType, "includesWaterKeyword", "uima.cas.Integer", featOkTst);
    casFeatCode_includesWaterKeyword  = (null == casFeat_includesWaterKeyword) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_includesWaterKeyword).getCode();

 
    casFeat_includesAdminTerm = jcas.getRequiredFeatureDE(casType, "includesAdminTerm", "uima.cas.Integer", featOkTst);
    casFeatCode_includesAdminTerm  = (null == casFeat_includesAdminTerm) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_includesAdminTerm).getCode();

 
    casFeat_countOfInitialGeoTerms = jcas.getRequiredFeatureDE(casType, "countOfInitialGeoTerms", "uima.cas.Integer", featOkTst);
    casFeatCode_countOfInitialGeoTerms  = (null == casFeat_countOfInitialGeoTerms) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_countOfInitialGeoTerms).getCode();

 
    casFeat_endsWithRoad = jcas.getRequiredFeatureDE(casType, "endsWithRoad", "uima.cas.Integer", featOkTst);
    casFeatCode_endsWithRoad  = (null == casFeat_endsWithRoad) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endsWithRoad).getCode();

 
    casFeat_endsWithAct = jcas.getRequiredFeatureDE(casType, "endsWithAct", "uima.cas.Integer", featOkTst);
    casFeatCode_endsWithAct  = (null == casFeat_endsWithAct) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_endsWithAct).getCode();

 
    casFeat_countOfEndGeoFeatures = jcas.getRequiredFeatureDE(casType, "countOfEndGeoFeatures", "uima.cas.Integer", featOkTst);
    casFeatCode_countOfEndGeoFeatures  = (null == casFeat_countOfEndGeoFeatures) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_countOfEndGeoFeatures).getCode();

 
    casFeat_matchesName1 = jcas.getRequiredFeatureDE(casType, "matchesName1", "uima.cas.Integer", featOkTst);
    casFeatCode_matchesName1  = (null == casFeat_matchesName1) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_matchesName1).getCode();

 
    casFeat_matchesName2 = jcas.getRequiredFeatureDE(casType, "matchesName2", "uima.cas.Integer", featOkTst);
    casFeatCode_matchesName2  = (null == casFeat_matchesName2) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_matchesName2).getCode();

 
    casFeat_precededByDefiniteArticle = jcas.getRequiredFeatureDE(casType, "precededByDefiniteArticle", "uima.cas.Integer", featOkTst);
    casFeatCode_precededByDefiniteArticle  = (null == casFeat_precededByDefiniteArticle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_precededByDefiniteArticle).getCode();

 
    casFeat_precededByPersonalTitle = jcas.getRequiredFeatureDE(casType, "precededByPersonalTitle", "uima.cas.Integer", featOkTst);
    casFeatCode_precededByPersonalTitle  = (null == casFeat_precededByPersonalTitle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_precededByPersonalTitle).getCode();

 
    casFeat_hasRepresentative = jcas.getRequiredFeatureDE(casType, "hasRepresentative", "uima.cas.Integer", featOkTst);
    casFeatCode_hasRepresentative  = (null == casFeat_hasRepresentative) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_hasRepresentative).getCode();

 
    casFeat_followedByPersonalTitle = jcas.getRequiredFeatureDE(casType, "followedByPersonalTitle", "uima.cas.Integer", featOkTst);
    casFeatCode_followedByPersonalTitle  = (null == casFeat_followedByPersonalTitle) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_followedByPersonalTitle).getCode();

 
    casFeat_followedByAcronym = jcas.getRequiredFeatureDE(casType, "followedByAcronym", "uima.cas.Integer", featOkTst);
    casFeatCode_followedByAcronym  = (null == casFeat_followedByAcronym) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_followedByAcronym).getCode();

 
    casFeat_institutionScore = jcas.getRequiredFeatureDE(casType, "institutionScore", "uima.cas.Double", featOkTst);
    casFeatCode_institutionScore  = (null == casFeat_institutionScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_institutionScore).getCode();

 
    casFeat_personScore = jcas.getRequiredFeatureDE(casType, "personScore", "uima.cas.Double", featOkTst);
    casFeatCode_personScore  = (null == casFeat_personScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_personScore).getCode();

 
    casFeat_waterScore = jcas.getRequiredFeatureDE(casType, "waterScore", "uima.cas.Double", featOkTst);
    casFeatCode_waterScore  = (null == casFeat_waterScore) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_waterScore).getCode();

  }
}



    