

/* First created by JCasGen Mon Nov 12 14:14:29 AKST 2012 */
package cnh.annotator.authorities;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed May 22 09:13:43 CDT 2013
 * XML source: /Users/murphy/work/CNH_GIT/CNH/Annotations/desc/WaterAuthorityTypeSystem.xml
 * @generated */
public class WaterAuthorityCandidate extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(WaterAuthorityCandidate.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected WaterAuthorityCandidate() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public WaterAuthorityCandidate(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public WaterAuthorityCandidate(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public WaterAuthorityCandidate(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: start

  /** getter for start - gets Index of first character
   * @generated */
  public int getStart() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_start == null)
      jcasType.jcas.throwFeatMissing("start", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_start);}
    
  /** setter for start - sets Index of first character 
   * @generated */
  public void setStart(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_start == null)
      jcasType.jcas.throwFeatMissing("start", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_start, v);}    
   
    
  //*--------------*
  //* Feature: end

  /** getter for end - gets 
   * @generated */
  public int getEnd() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_end == null)
      jcasType.jcas.throwFeatMissing("end", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_end);}
    
  /** setter for end - sets  
   * @generated */
  public void setEnd(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_end == null)
      jcasType.jcas.throwFeatMissing("end", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_end, v);}    
   
    
  //*--------------*
  //* Feature: startTokenIndex

  /** getter for startTokenIndex - gets 
   * @generated */
  public int getStartTokenIndex() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_startTokenIndex);}
    
  /** setter for startTokenIndex - sets  
   * @generated */
  public void setStartTokenIndex(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_startTokenIndex == null)
      jcasType.jcas.throwFeatMissing("startTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_startTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: endTokenIndex

  /** getter for endTokenIndex - gets 
   * @generated */
  public int getEndTokenIndex() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endTokenIndex);}
    
  /** setter for endTokenIndex - sets  
   * @generated */
  public void setEndTokenIndex(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endTokenIndex == null)
      jcasType.jcas.throwFeatMissing("endTokenIndex", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endTokenIndex, v);}    
   
    
  //*--------------*
  //* Feature: fullText

  /** getter for fullText - gets 
   * @generated */
  public String getFullText() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_fullText == null)
      jcasType.jcas.throwFeatMissing("fullText", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getStringValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_fullText);}
    
  /** setter for fullText - sets  
   * @generated */
  public void setFullText(String v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_fullText == null)
      jcasType.jcas.throwFeatMissing("fullText", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setStringValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_fullText, v);}    
   
    
  //*--------------*
  //* Feature: personScore

  /** getter for personScore - gets 
   * @generated */
  public double getPersonScore() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_personScore == null)
      jcasType.jcas.throwFeatMissing("personScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_personScore);}
    
  /** setter for personScore - sets  
   * @generated */
  public void setPersonScore(double v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_personScore == null)
      jcasType.jcas.throwFeatMissing("personScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_personScore, v);}    
   
    
  //*--------------*
  //* Feature: waterScore

  /** getter for waterScore - gets 
   * @generated */
  public double getWaterScore() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_waterScore == null)
      jcasType.jcas.throwFeatMissing("waterScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_waterScore);}
    
  /** setter for waterScore - sets  
   * @generated */
  public void setWaterScore(double v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_waterScore == null)
      jcasType.jcas.throwFeatMissing("waterScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_waterScore, v);}    
                  //*--------------*
  //* Feature: countOfTerms

  /** getter for countOfTerms - gets 
   * @generated */
  public int getCountOfTerms() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfTerms == null)
      jcasType.jcas.throwFeatMissing("countOfTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfTerms);}
    
  /** setter for countOfTerms - sets  
   * @generated */
  public void setCountOfTerms(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfTerms == null)
      jcasType.jcas.throwFeatMissing("countOfTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfTerms, v);}    
   
    
  //*--------------*
  //* Feature: countOfNonNNPTerms

  /** getter for countOfNonNNPTerms - gets 
   * @generated */
  public int getCountOfNonNNPTerms() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfNonNNPTerms == null)
      jcasType.jcas.throwFeatMissing("countOfNonNNPTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfNonNNPTerms);}
    
  /** setter for countOfNonNNPTerms - sets  
   * @generated */
  public void setCountOfNonNNPTerms(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfNonNNPTerms == null)
      jcasType.jcas.throwFeatMissing("countOfNonNNPTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfNonNNPTerms, v);}    
   
    
  //*--------------*
  //* Feature: precededByDefiniteArticle

  /** getter for precededByDefiniteArticle - gets 
   * @generated */
  public int getPrecededByDefiniteArticle() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_precededByDefiniteArticle == null)
      jcasType.jcas.throwFeatMissing("precededByDefiniteArticle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_precededByDefiniteArticle);}
    
  /** setter for precededByDefiniteArticle - sets  
   * @generated */
  public void setPrecededByDefiniteArticle(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_precededByDefiniteArticle == null)
      jcasType.jcas.throwFeatMissing("precededByDefiniteArticle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_precededByDefiniteArticle, v);}    
   
    
  //*--------------*
  //* Feature: precededByPersonalTitle

  /** getter for precededByPersonalTitle - gets 
   * @generated */
  public int getPrecededByPersonalTitle() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_precededByPersonalTitle == null)
      jcasType.jcas.throwFeatMissing("precededByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_precededByPersonalTitle);}
    
  /** setter for precededByPersonalTitle - sets  
   * @generated */
  public void setPrecededByPersonalTitle(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_precededByPersonalTitle == null)
      jcasType.jcas.throwFeatMissing("precededByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_precededByPersonalTitle, v);}    
   
    
  //*--------------*
  //* Feature: hasRepresentative

  /** getter for hasRepresentative - gets 
   * @generated */
  public int getHasRepresentative() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_hasRepresentative == null)
      jcasType.jcas.throwFeatMissing("hasRepresentative", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_hasRepresentative);}
    
  /** setter for hasRepresentative - sets  
   * @generated */
  public void setHasRepresentative(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_hasRepresentative == null)
      jcasType.jcas.throwFeatMissing("hasRepresentative", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_hasRepresentative, v);}    
   
    
  //*--------------*
  //* Feature: endsWithAct

  /** getter for endsWithAct - gets 
   * @generated */
  public int getEndsWithAct() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endsWithAct == null)
      jcasType.jcas.throwFeatMissing("endsWithAct", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endsWithAct);}
    
  /** setter for endsWithAct - sets  
   * @generated */
  public void setEndsWithAct(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endsWithAct == null)
      jcasType.jcas.throwFeatMissing("endsWithAct", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endsWithAct, v);}    
   
    
  //*--------------*
  //* Feature: includesLocationKeyword

  /** getter for includesLocationKeyword - gets 
   * @generated */
  public int getIncludesLocationKeyword() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesLocationKeyword == null)
      jcasType.jcas.throwFeatMissing("includesLocationKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesLocationKeyword);}
    
  /** setter for includesLocationKeyword - sets  
   * @generated */
  public void setIncludesLocationKeyword(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesLocationKeyword == null)
      jcasType.jcas.throwFeatMissing("includesLocationKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesLocationKeyword, v);}    
   
    
  //*--------------*
  //* Feature: includesWaterKeyword

  /** getter for includesWaterKeyword - gets 
   * @generated */
  public int getIncludesWaterKeyword() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesWaterKeyword == null)
      jcasType.jcas.throwFeatMissing("includesWaterKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesWaterKeyword);}
    
  /** setter for includesWaterKeyword - sets  
   * @generated */
  public void setIncludesWaterKeyword(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesWaterKeyword == null)
      jcasType.jcas.throwFeatMissing("includesWaterKeyword", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesWaterKeyword, v);}    
   
    
  //*--------------*
  //* Feature: includesAdminTerm

  /** getter for includesAdminTerm - gets 
   * @generated */
  public int getIncludesAdminTerm() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesAdminTerm == null)
      jcasType.jcas.throwFeatMissing("includesAdminTerm", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesAdminTerm);}
    
  /** setter for includesAdminTerm - sets  
   * @generated */
  public void setIncludesAdminTerm(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_includesAdminTerm == null)
      jcasType.jcas.throwFeatMissing("includesAdminTerm", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_includesAdminTerm, v);}    
   
    
  //*--------------*
  //* Feature: countOfInitialGeoTerms

  /** getter for countOfInitialGeoTerms - gets 
   * @generated */
  public int getCountOfInitialGeoTerms() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfInitialGeoTerms == null)
      jcasType.jcas.throwFeatMissing("countOfInitialGeoTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfInitialGeoTerms);}
    
  /** setter for countOfInitialGeoTerms - sets  
   * @generated */
  public void setCountOfInitialGeoTerms(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfInitialGeoTerms == null)
      jcasType.jcas.throwFeatMissing("countOfInitialGeoTerms", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfInitialGeoTerms, v);}    
   
    
  //*--------------*
  //* Feature: endsWithRoad

  /** getter for endsWithRoad - gets 
   * @generated */
  public int getEndsWithRoad() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endsWithRoad == null)
      jcasType.jcas.throwFeatMissing("endsWithRoad", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endsWithRoad);}
    
  /** setter for endsWithRoad - sets  
   * @generated */
  public void setEndsWithRoad(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_endsWithRoad == null)
      jcasType.jcas.throwFeatMissing("endsWithRoad", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_endsWithRoad, v);}    
   
    
  //*--------------*
  //* Feature: countOfEndGeoFeatures

  /** getter for countOfEndGeoFeatures - gets 
   * @generated */
  public int getCountOfEndGeoFeatures() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfEndGeoFeatures == null)
      jcasType.jcas.throwFeatMissing("countOfEndGeoFeatures", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfEndGeoFeatures);}
    
  /** setter for countOfEndGeoFeatures - sets  
   * @generated */
  public void setCountOfEndGeoFeatures(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_countOfEndGeoFeatures == null)
      jcasType.jcas.throwFeatMissing("countOfEndGeoFeatures", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_countOfEndGeoFeatures, v);}    
   
    
  //*--------------*
  //* Feature: matchesName1

  /** getter for matchesName1 - gets 
   * @generated */
  public int getMatchesName1() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_matchesName1 == null)
      jcasType.jcas.throwFeatMissing("matchesName1", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_matchesName1);}
    
  /** setter for matchesName1 - sets  
   * @generated */
  public void setMatchesName1(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_matchesName1 == null)
      jcasType.jcas.throwFeatMissing("matchesName1", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_matchesName1, v);}    
   
    
  //*--------------*
  //* Feature: matchesName2

  /** getter for matchesName2 - gets 
   * @generated */
  public int getMatchesName2() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_matchesName2 == null)
      jcasType.jcas.throwFeatMissing("matchesName2", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_matchesName2);}
    
  /** setter for matchesName2 - sets  
   * @generated */
  public void setMatchesName2(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_matchesName2 == null)
      jcasType.jcas.throwFeatMissing("matchesName2", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_matchesName2, v);}    
   
    
  //*--------------*
  //* Feature: followedByAcronym

  /** getter for followedByAcronym - gets 
   * @generated */
  public int getFollowedByAcronym() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_followedByAcronym == null)
      jcasType.jcas.throwFeatMissing("followedByAcronym", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_followedByAcronym);}
    
  /** setter for followedByAcronym - sets  
   * @generated */
  public void setFollowedByAcronym(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_followedByAcronym == null)
      jcasType.jcas.throwFeatMissing("followedByAcronym", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_followedByAcronym, v);}    
   
    
  //*--------------*
  //* Feature: followedByPersonalTitle

  /** getter for followedByPersonalTitle - gets 
   * @generated */
  public int getFollowedByPersonalTitle() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_followedByPersonalTitle == null)
      jcasType.jcas.throwFeatMissing("followedByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_followedByPersonalTitle);}
    
  /** setter for followedByPersonalTitle - sets  
   * @generated */
  public void setFollowedByPersonalTitle(int v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_followedByPersonalTitle == null)
      jcasType.jcas.throwFeatMissing("followedByPersonalTitle", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setIntValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_followedByPersonalTitle, v);}    
   
    
  //*--------------*
  //* Feature: institutionScore

  /** getter for institutionScore - gets 
   * @generated */
  public double getInstitutionScore() {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_institutionScore == null)
      jcasType.jcas.throwFeatMissing("institutionScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    return jcasType.ll_cas.ll_getDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_institutionScore);}
    
  /** setter for institutionScore - sets  
   * @generated */
  public void setInstitutionScore(double v) {
    if (WaterAuthorityCandidate_Type.featOkTst && ((WaterAuthorityCandidate_Type)jcasType).casFeat_institutionScore == null)
      jcasType.jcas.throwFeatMissing("institutionScore", "cnh.annotator.authorities.WaterAuthorityCandidate");
    jcasType.ll_cas.ll_setDoubleValue(addr, ((WaterAuthorityCandidate_Type)jcasType).casFeatCode_institutionScore, v);}    
   
    
}

    