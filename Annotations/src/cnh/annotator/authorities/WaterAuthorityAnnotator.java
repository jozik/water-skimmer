package cnh.annotator.authorities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import opennlp.uima.Sentence;
import opennlp.uima.Token;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;

import cnh.water.annotation.LocationAnnotation;
import cnh.water.annotation.WaterAnnotation;

public class WaterAuthorityAnnotator  extends JCasAnnotator_ImplBase{

	private static boolean verbose = false;
	private static int count = 0;
	
	public static String cleanToken(String orig){
		String ret = orig;
		ret = ret.replaceAll("[���_��ԥ\\]\\*\\\"]", "").trim();
		return ret;
	}
	
	private static String[] readFile(String fileName){
		Vector<String> data = new Vector<String>();
		BufferedReader br = null;
		try{
			String currentLine;
			br = new BufferedReader(new FileReader(fileName));
			while((currentLine = br.readLine()) != null) data.add(currentLine);
			
		}
		catch(IOException e){
			System.out.println(e.getStackTrace());
		}
		finally{
			try{
				br.close();
			}
			catch(IOException e){
				System.out.println(e.getStackTrace());
			}
		}
		String[] ret = {};
		return data.toArray(ret);
	}
	
	private String[] personalTitles     = {};
	private String[] daysOfTheWeek      = {};
	private String[] representatives    = {};
	private String[] months             = {};
	private String[] specialDays        = {};
	private String[] roads              = {};
	private String[] nonNNPs            = {};
	private String[] geoTerms           = {};
	private String[] geoFeatures        = {};
	private String[] initialGeoFeatures = {};
	private String[] adminTerms         = {};
	private String[] legislationTerms   = {};
	
	
	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		String supportDir = (String) this.getContext().getConfigParameterValue("SupportDirectory");
		personalTitles     = readFile(supportDir + "/personalTitles.txt");
		daysOfTheWeek      = readFile(supportDir + "/daysOfTheWeek.txt");
		representatives    = readFile(supportDir + "/representatives.txt");
		months             = readFile(supportDir + "/months.txt");
		specialDays        = readFile(supportDir + "/specialDays.txt");
		roads              = readFile(supportDir + "/roads.txt");
		nonNNPs            = readFile(supportDir + "/nonNNPs.txt");
		geoTerms           = readFile(supportDir + "/geoTerms.txt");
		geoFeatures        = readFile(supportDir + "/geoFeatures.txt");
		initialGeoFeatures = readFile(supportDir + "/initialGeoFeatures.txt");
		adminTerms         = readFile(supportDir + "/adminTerms.txt");
		legislationTerms   = readFile(supportDir + "/legislationTerms.txt");
	}
	
	// Get useful arrays (lists) of tokens, sentences, and tokens within sentences
	private AnnotationFS[] getTokenList(JCas cas){
		ArrayList<AnnotationFS> tokenAnnotations = new ArrayList<AnnotationFS>();
		for (FSIterator<Annotation> it = cas.getAnnotationIndex(cas.getTypeSystem().getType("opennlp.uima.Token")).iterator(); it.hasNext();) {
			tokenAnnotations.add(it.next());
		}
		return tokenAnnotations.toArray(new AnnotationFS[] {});
	}

	private AnnotationFS[] getSentenceList(JCas cas){
		ArrayList<AnnotationFS> sentenceAnnotations = new ArrayList<AnnotationFS>();
		for (FSIterator<Annotation> it = cas.getAnnotationIndex(cas.getTypeSystem().getType("opennlp.uima.Sentence")).iterator(); it.hasNext();) {
			sentenceAnnotations.add(it.next());
		}
		return sentenceAnnotations.toArray(new AnnotationFS[] {});
	}
	
	private AnnotationFS[] getSentenceTokenList(AnnotationFS[] tokens, Sentence sentenceToken){
		ArrayList<AnnotationFS> sentence = new ArrayList<AnnotationFS>();
		if(sentenceToken.getCoveredText().length() > 1){
			int sentenceBegin = sentenceToken.getStartTokenIndex();
			int sentenceEnd   = sentenceToken.getEndTokenIndex();
			for(int i = sentenceBegin; i <= sentenceEnd; i++) sentence.add(tokens[i]);
		}
		return sentence.toArray(new AnnotationFS[]{});
	}
	
	private AnnotationFS[] getLocationList(JCas cas){
		ArrayList<AnnotationFS> locationAnnotations = new ArrayList<AnnotationFS>();
		for (FSIterator<Annotation> it = cas.getAnnotationIndex(LocationAnnotation.type).iterator(); it.hasNext(); it.next()) {
			locationAnnotations.add(it.get());
		}
		return locationAnnotations.toArray(new AnnotationFS[] {});
	}
	
	private AnnotationFS[] getWaterKeywordList(JCas cas){
		ArrayList<AnnotationFS> waterAnnotations = new ArrayList<AnnotationFS>();
		for (FSIterator<Annotation> it = cas.getAnnotationIndex(WaterAnnotation.type).iterator(); it.hasNext(); it.next()) {
			if(((WaterAnnotation)it.get()).getWaterCategory().compareToIgnoreCase("WATER") == 0) waterAnnotations.add(it.get());
		}
		return waterAnnotations.toArray(new AnnotationFS[] {});
	}
	
	// Comparisons on sets of possibilities
	private boolean match(String text, String[] comparisons){
		return match(text, comparisons, false);
	}
	
	private boolean match(String text, String[] comparisons, boolean ignoreCase){
		if(ignoreCase == false)	{ for(String comp: comparisons) if(text.compareTo(comp)           == 0) return true; }
		else                    { for(String comp: comparisons) if(text.compareToIgnoreCase(comp) == 0) return true; }
		return false;
	}
	
	private boolean isPersonalTitle(String text){	return match(text, personalTitles); 	    }
	private boolean isRepresentative(String text){	return match(text, representatives, true);	}
	private boolean isDayOfWeek(String text){		return match(text, daysOfTheWeek, true);	}
	private boolean isMonth(String text){		    return match(text, months);                 }
	private boolean isDay(String text){             return match(text, specialDays);            }
	private boolean isRoad(String text){            return match(text, roads);                  }
		
	
	// Convert POS to 'adjusted' POS, which will NOT mark quotation marks as NNP, etc.
	private String getAdjPOS(Token t){
		String origPOS  = t.getPos();
		String origText = t.getCoveredText();
		
		if(origPOS.compareTo("NNP") == 0){
			if(origText.compareTo("\"") == 0) return "QUOTE";
			if(origText.compareTo("�") == 0)  return "QUOTE";
			if(origText.compareTo("�") == 0)  return "EMDASH";
			if(origText.compareTo("�") == 0)  return "DOT";
			if(isPersonalTitle(origText))     return "PERSONAL_TITLE";
			if(isDayOfWeek(origText))         return "DAY_OF_WEEK";
			if(isMonth(origText))             return "MONTH_OF_YEAR";
			if(isDay(origText))               return "DAY";
		}
		
		if((origPOS.compareTo("DT") == 0) && (origText.compareTo("a") == 0)){
//			System.out.println("FOUND AN 'a' MARKED AS A DT");
			return "INDEF";   
		}
		
        if(match(origText, nonNNPs))                             return "NonNNP"; 
		if(origText.startsWith("�") &&  origText.length() > 1)   return "UNK_";
		
		return origPOS;
	}
	
	
	// Reduce POS to 'Parse Type': either noun, 'skip', or stop
	public enum PARSE_TYPE { NNP, SKIP, STOP }
	
	private PARSE_TYPE parse(String adjPOS){
		if(adjPOS.startsWith("NNP"))    return PARSE_TYPE.NNP;
		if(adjPOS.compareTo("DT") == 0) return PARSE_TYPE.SKIP;
		if(adjPOS.compareTo("IN") == 0) return PARSE_TYPE.SKIP;
		return PARSE_TYPE.STOP;
	}
	
	
	private String getPhrase(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		String phrase = "";
		int sti = Math.max(startTokenIndex,0);
		int eti = Math.min(endTokenIndex,tokens.length - 1);
		for(int i = sti; i <= eti; i++)	phrase = phrase + (phrase.length() > 0 ? " " : "") + cleanToken(tokens[i].getCoveredText());
		return phrase;
	}

	// Detecting signs of 'institution'-ness
	// Internal
	
	private int countNonNNPTerms(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		int score = 0;
		for(int i = startTokenIndex; i <= endTokenIndex; i++)  if(!((Token)tokens[i]).getPos().startsWith("NNP"))	score++;
		return score;
	}
	
	private boolean precededByDefArticle(AnnotationFS[] tokens, int startTokenIndex){
		return((startTokenIndex > 0) && (getAdjPOS((Token)tokens[startTokenIndex-1]).compareTo("DT") == 0));
	}
	
	private boolean precededByPersonalTitle(AnnotationFS[] tokens, int startTokenIndex){
		return((startTokenIndex > 0) && isPersonalTitle( ((Token)tokens[startTokenIndex-1]).getCoveredText()));
	}
	
	private boolean endsWithAct(AnnotationFS[] tokens, int endTokenIndex){
		return match(((Token)tokens[endTokenIndex]).getCoveredText(), legislationTerms);
//		return (
//				(((Token)tokens[endTokenIndex]).getCoveredText().compareToIgnoreCase("Act")     == 0) ||
//				(((Token)tokens[endTokenIndex]).getCoveredText().compareToIgnoreCase("Compact") == 0) ||
//				(((Token)tokens[endTokenIndex]).getCoveredText().compareToIgnoreCase("Code"   ) == 0) ||
//				(((Token)tokens[endTokenIndex]).getCoveredText().compareToIgnoreCase("Bill")    == 0));
	}
	
	private boolean endsWithRoad(AnnotationFS[] tokens, int endTokenIndex){
		return isRoad(((Token)tokens[endTokenIndex]).getCoveredText());
	}
		
	private boolean includesLocationKeyword(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex, AnnotationFS[] locationList){
		for(int i = 0; i < locationList.length; i++){
			LocationAnnotation LA = (LocationAnnotation) locationList[i];
			long begin = LA.getStartTokenIndex();
			long end   = LA.getEndTokenIndex();
			if((begin >= startTokenIndex && end <  endTokenIndex) || 
		       (begin >  startTokenIndex && end <= endTokenIndex)){
				return true;
			}
		}
		return false;
	}
	
	private boolean includesWaterKeyword(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex, AnnotationFS[] waterKeywordList){
		for(int i = 0; i < waterKeywordList.length; i++){
			WaterAnnotation WA = (WaterAnnotation) waterKeywordList[i];
			long begin = WA.getStartTokenIndex();
			long end   = WA.getEndTokenIndex();
			if((begin >= startTokenIndex && end <  endTokenIndex) || 
		       (begin >  startTokenIndex && end <= endTokenIndex)){
				return true;
			}
		}
		return false;
	}
	
	private boolean includesAdminTerm(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		for(int i = startTokenIndex; i <= endTokenIndex; i++) if(match(((Token)tokens[i]).getCoveredText(), adminTerms)) return true;
		return false;
	}
	
	private int startsWithGeoTerm(AnnotationFS[] tokens, int startTokenIndex){ 
		return (match(((Token)tokens[startTokenIndex]).getCoveredText(), initialGeoFeatures) ? 1 : 
			    (match(((Token)tokens[startTokenIndex]).getCoveredText(), geoTerms) &&  
			     match(((Token)tokens[startTokenIndex + 1]).getCoveredText(), initialGeoFeatures) ? 2 : 0)) ; 
	}
	
	private int geoFeaturesEndCount(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		int ret = 0;
		int i = endTokenIndex;
		while(i >= startTokenIndex && match(((Token)tokens[i]).getCoveredText(), geoFeatures)){
			ret++;
			i--;
		}
		return ret;
	}
	
	private boolean matchesNamePattern_1(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		String text = "";
		for(int i = startTokenIndex; i <= endTokenIndex; i++) text += (text.length() > 0 ? " " : "") + ((Token)tokens[i]).getCoveredText();
		return (text.matches("[A-Z][a-z]+ [A-Z][a-z]+"));
	}

	private boolean matchesNamePattern_2(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		String text = "";
		for(int i = startTokenIndex; i <= endTokenIndex; i++) text += (text.length() > 0 ? " " : "") + ((Token)tokens[i]).getCoveredText();
	    return (text.matches("[A-Z][a-z\\.]+ [A-Z]\\. [A-Z][a-z]+"));
	}
	
	// Contextual
	
	// Look for constructions like "Officials for Megalopolis Water..." or "Megalopolis Water officials said..."
	private boolean withRepresentatives(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		// Look for a preposition before the target phrase
		// If there is one, check the word before it; is this a 'representative
		if(startTokenIndex > 1){
			String possiblePrep = ((Token)tokens[startTokenIndex - 1]).getCoveredText();
			if((possiblePrep.compareTo("for") == 0)  ||
			   (possiblePrep.compareTo("from") == 0) ||
			   (possiblePrep.compareTo("of") == 0)){
				String term = ((Token)tokens[startTokenIndex - 2]).getCoveredText();
			  if(isRepresentative(term) || isPersonalTitle(term)) return true;
			}
		}
		if(startTokenIndex > 2){
			String possibleDefArt = ((Token)tokens[startTokenIndex - 1]).getCoveredText();
			if(possibleDefArt.compareToIgnoreCase("the") == 0){
				String possiblePrep = ((Token)tokens[startTokenIndex - 2]).getCoveredText();
				if((possiblePrep.compareTo("for") == 0)  ||
				   (possiblePrep.compareTo("from") == 0) ||
				   (possiblePrep.compareTo("of") == 0)){
					String term = ((Token)tokens[startTokenIndex - 3]).getCoveredText();
					if(isRepresentative(term) || isPersonalTitle(term)) return true;
				}
			}
		}
		
		
		// Look after the phrase, too
		if(endTokenIndex + 1 < tokens.length){
			String term = ((Token)tokens[endTokenIndex + 1]).getCoveredText();
			if(isRepresentative(term) || (isPersonalTitle(term) && term.compareTo("Dr.") != 0)) return true; // Note: 'Dr.' can't really be used this way, but it
			                                                                                                 // also means "Drive" (as in "Grand Mesa Dr.")
		}
		
		return false;
	}
	private boolean followedByAcronym(AnnotationFS[] tokens, int endTokenIndex){
		return ((endTokenIndex + 3) < tokens.length) &&
			   ( (((Token)tokens[endTokenIndex + 1]).getPos().compareTo("-LRB-") == 0) &&             // Left parenthesis
			     (((Token)tokens[endTokenIndex + 2]).getCoveredText().matches("[A-Z]{2,}")) &&        // Any series of capital letters longer than 1
			     (((Token)tokens[endTokenIndex + 3]).getPos().compareTo("-RRB-") == 0) );             // Right parenthesis
	}
	
	private boolean followedByPersTitle(AnnotationFS[] tokens, int endTokenIndex){
		return ((endTokenIndex + 1) < tokens.length) && 
			   (isPersonalTitle(((Token)tokens[endTokenIndex + 1]).getCoveredText()));		
	}
	
		
	@Override
	public void process(JCas cas) throws AnalysisEngineProcessException {
		count++;
		AnnotationFS[] tokenList        = getTokenList(cas);
		AnnotationFS[] sentenceList     = getSentenceList(cas);
		AnnotationFS[] locationList     = getLocationList(cas);
		AnnotationFS[] waterKeywordList = getWaterKeywordList(cas);

		// Loop through sentences
		for(int sentenceIndex = 0; sentenceIndex < sentenceList.length; sentenceIndex++){
			if(verbose){
				System.out.println("==============================================");
				System.out.println(((Sentence)sentenceList[sentenceIndex]).getCoveredText());
			}
			AnnotationFS[] sentenceTokenList = getSentenceTokenList(tokenList, (Sentence)sentenceList[sentenceIndex]);
			int sentenceStartToken = ((Sentence)sentenceList[sentenceIndex]).getStartTokenIndex();
			int indx = 0;
			while (indx < sentenceTokenList.length){
				PARSE_TYPE adjPOS_type = parse(getAdjPOS((Token)sentenceTokenList[indx]));
				if(verbose) System.out.println((((Token)tokenList[sentenceStartToken+indx]).getCoveredText() + "                                                  ").substring(0, 50) + (((Token)tokenList[sentenceStartToken+indx]).getPos()));
				if(adjPOS_type == PARSE_TYPE.NNP){
					int ix = indx + 1;
					while((ix < sentenceTokenList.length) && 
						  (adjPOS_type = parse(getAdjPOS((Token)sentenceTokenList[ix]))) != PARSE_TYPE.STOP){
						ix++;
					}
					ix--;
					while((adjPOS_type = parse(getAdjPOS((Token)sentenceTokenList[ix]))) != PARSE_TYPE.NNP) ix--;
					if(ix - indx > 0){
						String phrase = getPhrase(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						double iScore, pScore, wScore;
						int     countOfTerms              = ix - indx + 1;
						int     countOfNonNNPTerms        = countNonNNPTerms(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						boolean includesPlace             = includesLocationKeyword(tokenList, sentenceStartToken + indx, sentenceStartToken + ix, locationList);
						boolean includesWater             = includesWaterKeyword(tokenList, sentenceStartToken + indx, sentenceStartToken + ix, waterKeywordList);
						boolean includesAdmin             = includesAdminTerm(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						int     countOfInitialGeoTerms    = startsWithGeoTerm(tokenList, sentenceStartToken + indx);
						boolean endsWithRoad              = endsWithRoad(tokenList, sentenceStartToken + ix);
						boolean endsWithAct               = endsWithAct(tokenList, sentenceStartToken + ix);
						int     countOfEndGeoFeats        = geoFeaturesEndCount(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						boolean matchesName1              = matchesNamePattern_1(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						boolean matchesName2              = matchesNamePattern_2(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						boolean precededByDefiniteArticle = precededByDefArticle(tokenList, sentenceStartToken + indx);
						boolean precededByPersonalTitle   = precededByPersonalTitle(tokenList, sentenceStartToken + indx);
						boolean hasRepresentative         = withRepresentatives(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
						boolean followedByPersonalTitle   = followedByPersTitle(tokenList, sentenceStartToken + ix);
						boolean followedByAcronym         = followedByAcronym(tokenList, sentenceStartToken + ix);
						
						iScore = getInstitutionScore(tokenList, sentenceStartToken + indx, sentenceStartToken + ix, locationList, waterKeywordList);
					    pScore = getPersonScore(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);
					    wScore = getWaterScore(tokenList, sentenceStartToken + indx, sentenceStartToken + ix);

						addAnnotation(cas, tokenList, sentenceStartToken + indx, sentenceStartToken + ix,  phrase, 
								countOfTerms, countOfNonNNPTerms, includesPlace, includesWater, includesAdmin, countOfInitialGeoTerms, endsWithRoad, endsWithAct, countOfEndGeoFeats,
								matchesName1, matchesName2,
								precededByDefiniteArticle, precededByPersonalTitle, hasRepresentative, followedByPersonalTitle, followedByAcronym, iScore, pScore, wScore);
					}
					if(verbose){
						while(indx < ix){
							System.out.println((((Token)tokenList[sentenceStartToken+indx]).getCoveredText() + "                                                  ").substring(0, 50) + (((Token)tokenList[sentenceStartToken+indx]).getPos()));
							indx++;
						}
					}
					indx = ix;
				}
				indx++;
			}	
		}	
	}
	
	
	private void addAnnotation(JCas cas, AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex, String phrase, 
			int countOfTerms, int countOfNonNNPTerms, 
			boolean includesPlace, boolean includesWater, boolean includesAdmin, int countOfInitialGeoTerms, boolean endsWithRoad, boolean endsWithAct, int countOfEndGeoFeats, 
			boolean matchesName1, boolean matchesName2,
			boolean precededByDefiniteArticle, boolean precededByPersonalTitle, boolean hasRepresentative, boolean followedByPersonalTitle, boolean followedByAcronym, 
			double institutionScore, double personScore, double waterScore){
		Type annType = cas.getTypeSystem().getType("cnh.annotator.authorities.WaterAuthorityCandidate");
	    Annotation annotation = (Annotation) cas.getCas().createAnnotation(annType, startTokenIndex, endTokenIndex);
	    annotation.setIntValue(annType.getFeatureByBaseName("start"), tokens[startTokenIndex].getBegin());
	    annotation.setIntValue(annType.getFeatureByBaseName("end"), tokens[endTokenIndex].getEnd());
	    annotation.setIntValue(annType.getFeatureByBaseName("startTokenIndex"), startTokenIndex);
	    annotation.setIntValue(annType.getFeatureByBaseName("endTokenIndex"), endTokenIndex);
	    annotation.setStringValue(annType.getFeatureByBaseName("fullText"), phrase);
	    annotation.setIntValue(annType.getFeatureByBaseName("countOfTerms"), countOfTerms);
	    annotation.setIntValue(annType.getFeatureByBaseName("countOfNonNNPTerms"), countOfNonNNPTerms);
	    annotation.setIntValue(annType.getFeatureByBaseName("includesLocationKeyword"), (includesPlace ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("includesWaterKeyword"), (includesWater ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("includesAdminTerm"), (includesAdmin ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("countOfInitialGeoTerms"), countOfInitialGeoTerms );
	    annotation.setIntValue(annType.getFeatureByBaseName("endsWithRoad"), (endsWithRoad ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("endsWithAct"), (endsWithAct ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("countOfEndGeoFeatures"), countOfEndGeoFeats);
	    annotation.setIntValue(annType.getFeatureByBaseName("matchesName1"), (matchesName1 ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("matchesName2"), (matchesName2 ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("precededByDefiniteArticle"),(precededByDefiniteArticle ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("precededByPersonalTitle"),(precededByPersonalTitle ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("hasRepresentative"),(hasRepresentative ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("followedByPersonalTitle"), (followedByPersonalTitle ? 1 : 0) );
	    annotation.setIntValue(annType.getFeatureByBaseName("followedByAcronym"), (followedByAcronym ? 1 : 0) );		
	    annotation.setDoubleValue(annType.getFeatureByBaseName("institutionScore"), institutionScore);
	    annotation.setDoubleValue(annType.getFeatureByBaseName("personScore"), personScore);
	    annotation.setDoubleValue(annType.getFeatureByBaseName("waterScore"), waterScore);
	    annotation.addToIndexes();
	}
	
	// OLD- This is now calculated in SQL
	
	private double getInstitutionScore(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex, AnnotationFS[] locationList, AnnotationFS[] waterKeywordList){
		double score = 0;
		
		// Internal
		if((endTokenIndex - startTokenIndex) > 1) score +=1; // If 3 terms
		if((endTokenIndex - startTokenIndex) > 2) score +=1; // If 4 or more terms
		if(startTokenIndex > 0){
			if(((Token)tokens[startTokenIndex-1]).getPos().compareTo("DT") == 0) score +=2; // If preceded by a definite article
		}
		for(int i = startTokenIndex; i <= endTokenIndex; i++){
			if(!((Token)tokens[i]).getPos().startsWith("NNP")){
				score +=1;  // If includes non NNP words         +1
				break;
			}
		}
		if(((Token)tokens[endTokenIndex]).getCoveredText().compareTo("Act") == 0) score -= 2; // "Endangered Species Act"
		
		// If includes a Place Name (But is not _just_ a place name?)
		for(int i = 0; i < locationList.length; i++){
			LocationAnnotation LA = (LocationAnnotation) locationList[i];
			long begin = LA.getStartTokenIndex();
			long end   = LA.getEndTokenIndex();
			if((begin >= startTokenIndex && end <  endTokenIndex) || 
		       (begin >  startTokenIndex && end <= endTokenIndex)){
				score += 2;
//				System.out.println("    RAISING INSTITUTION SCORE BASED ON LOCATION: " + LA.getCoveredText());
				break;
			}
		}
		
		// If it includes a Water Keyword
		for(int i = 0; i < waterKeywordList.length; i++){
			WaterAnnotation WA = (WaterAnnotation) waterKeywordList[i];
			long begin = WA.getStartTokenIndex();
			long end   = WA.getEndTokenIndex();
			if((begin >= startTokenIndex && end <  endTokenIndex) || 
		       (begin >  startTokenIndex && end <= endTokenIndex)){
				score += 2;
//				System.out.println("    RAISING INSTITUTION SCORE BASED ON WATER KEYWORD: " + WA.getCoveredText());
				break;
			}
		}
		
		
		// Contextual
		if((endTokenIndex + 3) < tokens.length){
			if( (((Token)tokens[endTokenIndex + 1]).getPos().compareTo("-LRB-") == 0) &&             // Left parenthesis
			    (((Token)tokens[endTokenIndex + 2]).getCoveredText().matches("[A-Z]*")) &&           // Any series of capital letters
			    (((Token)tokens[endTokenIndex + 3]).getPos().compareTo("-RRB-") == 0)){              // Right parenthesis
			  score +=2;
//			  System.out.println("FOUND ACRONYM: " + getPhrase(tokens, startTokenIndex, endTokenIndex) + " = " + ((Token)tokens[endTokenIndex + 2]).getCoveredText());
			}
			
		}
		if((endTokenIndex + 1) < tokens.length)
			if(isPersonalTitle(((Token)tokens[endTokenIndex + 1]).getCoveredText())) score +=1;
		return score;
	}
	
	private double getPersonScore(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		double score = 0;
		String text = "";
		boolean includesOnlyNNPWords = true;
		// If it includes non NNP words -1
		for(int i = startTokenIndex; i <= endTokenIndex; i++){
			if(!((Token)tokens[i]).getPos().startsWith("NNP")) includesOnlyNNPWords = false;
			text += (text.length() > 0 ? " " : "") + ((Token)tokens[i]).getCoveredText();
		}
		if(!includesOnlyNNPWords) score -=1;
		
		// If the tokens match the pattern "Aaaa Aaaa"                         +1
		if(text.matches("[A-Z][a-z]+ [A-Z][a-z]+"))            score += 1;
		
		// If the tokens match the pattern "Aaaa A. Aaaa"                      +2
		if(text.matches("[A-Z][a-z\\.]+ [A-Z]\\. [A-Z][a-z]+"))   score += 2;
		
		
		
		// If the tokens are preceded by a title                               +1
		if(startTokenIndex > 0){
			if(isPersonalTitle(((Token)tokens[startTokenIndex - 1]).getCoveredText())) score +=1;
		}
		
		// If the last token is used throughout the rest of the document       +1
		// If the last token is used throughout the rest, followed by 'said'   +2
		return score;
	}
	
	private double getWaterScore(AnnotationFS[] tokens, int startTokenIndex, int endTokenIndex){
		double score = 0;
		for(int i = startTokenIndex; i <= endTokenIndex; i++){
			if(((Token)tokens[i]).getCoveredText().compareToIgnoreCase("water")      == 0) score += 3;
			if(((Token)tokens[i]).getCoveredText().compareToIgnoreCase("irrigation") == 0) score += 3;
		}
		return score;
	}
	

	
}
