/**
 * 
 */
package cnh.annotator.stemmer;

import java.lang.reflect.Method;
import java.util.HashMap;

import opennlp.uima.Token;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.Language;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;
import org.tartarus.snowball.SnowballProgram;
import org.tartarus.snowball.ext.danishStemmer;
import org.tartarus.snowball.ext.dutchStemmer;
import org.tartarus.snowball.ext.englishStemmer;
import org.tartarus.snowball.ext.finnishStemmer;
import org.tartarus.snowball.ext.germanStemmer;
import org.tartarus.snowball.ext.hungarianStemmer;
import org.tartarus.snowball.ext.italianStemmer;
import org.tartarus.snowball.ext.norwegianStemmer;
import org.tartarus.snowball.ext.portugueseStemmer;
import org.tartarus.snowball.ext.russianStemmer;
import org.tartarus.snowball.ext.spanishStemmer;
import org.tartarus.snowball.ext.swedishStemmer;

/**
 * 
 * Stems tokens using the SNOWBALL stemmer. Most of this code is from the UIMA
 * add-ons snowball annotator. The only real change is to use the OpenNLP Token
 * type as the token to stem.
 * 
 * @author Nick Collier
 */
public class StemmerAnnotator extends JCasAnnotator_ImplBase {

	private Logger logger;

	private HashMap<String, SnowballProgram> stemmers;

	private static final Object[] NO_ARGS = new Object[0];

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.uima.analysis_component.JCasAnnotator_ImplBase#process(org
	 * .apache.uima.jcas.JCas)
	 */
	@Override
	public void process(JCas cas) throws AnalysisEngineProcessException {
		this.logger.log(Level.FINER, "stemmer annotator starts processing");

		// get get stemmer for the document language
		String language = new Language(cas.getDocumentLanguage()).getLanguagePart();

		SnowballProgram stemmer = this.stemmers.get(language);

		// create stemms if stemmer for the current document language is
		// available
		if (stemmer != null) {

			// get stem() method from stemmer
			Method stemmerStemMethod;
			try {
				stemmerStemMethod = stemmer.getClass().getMethod("stem", new Class[0]);
			} catch (Exception ex) {
				throw new AnalysisEngineProcessException(ex);
			}

			// iterate over all token annotations and add stem if available
			FSIterator<Annotation> tokenIterator = cas.getAnnotationIndex(Token.type).iterator();
			while (tokenIterator.hasNext()) {
				// get token content
				Token annot = (Token) tokenIterator.next();
				String span = annot.getCoveredText();

				// set annotation content and call stemmer
				try {
					stemmer.setCurrent(span);
					stemmerStemMethod.invoke(stemmer, NO_ARGS);
				} catch (Exception ex) {
					throw new AnalysisEngineProcessException(ex);
				}

				// get stemmer result and set annotation feature
				annot.setStem(stemmer.getCurrent());
			}
		} else {
			if (language.equals("x")) {
				this.logger.log(Level.WARNING,
						"Language of the CAS is set to 'x', SnowballAnnotator skipped processing.");
			}
		}
		this.logger.log(Level.FINER, "stemmer annotator processing finished");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.uima.analysis_component.AnalysisComponent_ImplBase#initialize
	 * (org.apache.uima.UimaContext)
	 */
	@Override
	public void initialize(UimaContext aContext) throws ResourceInitializationException {
		super.initialize(aContext);
		super.initialize(aContext);
		// initialize logger
		try {
			this.logger = aContext.getLogger();

			// initialize stemmers
			this.stemmers = new HashMap<String, SnowballProgram>();
			this.stemmers.put("da", new danishStemmer());
			this.stemmers.put("nl", new dutchStemmer());
			this.stemmers.put("en", new englishStemmer());
			this.stemmers.put("fi", new finnishStemmer());
			// NC: french stemmer wouldn't compile from snowball source
			// this.stemmers.put("fr", new frenchStemmer());
			this.stemmers.put("de", new germanStemmer());
			this.stemmers.put("hu", new hungarianStemmer());
			this.stemmers.put("it", new italianStemmer());
			this.stemmers.put("no", new norwegianStemmer());
			this.stemmers.put("pt", new portugueseStemmer());
			this.stemmers.put("ru", new russianStemmer());
			this.stemmers.put("es", new spanishStemmer());
			this.stemmers.put("sw", new swedishStemmer());
		} catch (Exception ex) {
			throw new ResourceInitializationException(ex);
		}

		this.logger.log(Level.INFO, "Snowball annotator successfully initialized");
	}

}
