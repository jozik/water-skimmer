package cnh.annotator.sentiment;


import java.util.List;
import java.util.Map;

import opennlp.uima.Token;

import org.apache.commons.lang3.StringUtils;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceAccessException;
import org.apache.uima.resource.ResourceInitializationException;

public class SentimentClueAnnotator extends JCasAnnotator_ImplBase {

	private ClueListFromPrefixMapResource clueListMap;
	Map<String,String> posMap;
	
	@Override
	public void initialize(UimaContext aContext)
			throws ResourceInitializationException {	
		super.initialize(aContext);
		try {
			clueListMap = (ClueListFromPrefixMapResource) getContext().getResourceObject("ClueList");
		}
		catch(ResourceAccessException e) {
		      throw new ResourceInitializationException(e);
	    }
		posMap = SentimentUtil.getPosMap();
	}
	
	protected SentimentClue matchClue(Token t, String simplePos){
		//System.out.println(t.getCoveredText());
		List<SentimentClue> list = clueListMap.get(StringUtils.substring(t.getCoveredText(),0,3).toLowerCase());
		if (list == null) {
			return null;
		}
		else {
			for (SentimentClue clue : list){
				if (clue.match(t, simplePos)){
					return clue;
				}
			}
			return null;
		}
	}

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		FSIterator<Annotation> tokenIterator = aJCas.getAnnotationIndex(Token.type).iterator();
		long counter = 0;
		while (tokenIterator.hasNext()){
			Token t = (Token) tokenIterator.next();
			String pos = t.getPos();
			String simplePos = posMap.get(pos);
			if (simplePos != null){
				SentimentClue clue = matchClue(t,simplePos);
				if (clue != null){
					// create Sentiment annotation
			          Sentiment sAnnot = new Sentiment(aJCas);
			          sAnnot.setBegin(t.getBegin());
			          sAnnot.setEnd(t.getEnd());
			          sAnnot.setSubjType(clue.getSubjType());
			          sAnnot.setPriorP(clue.getPriorP());
			          sAnnot.setTokenIndex(counter);
			          sAnnot.addToIndexes();
				}
			}
			counter++;
		}

	}

}
