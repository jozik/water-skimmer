package cnh.annotator.sentiment;

import opennlp.uima.Token;

public class SentimentClue {
	
	static private final String ANYPOS = "anypos";
	
	//subj_type, word, pos, stemmed, prior_p
	String subjType;
	public String getSubjType() {
		return subjType;
	}

	public String getPriorP() {
		return priorP;
	}

	String pos;
	String priorP;
	String word;
	
	private IMatcher matcher;
	
	private static interface IMatcher{
		public boolean match(Token t);
	}
	
	private class StemmerMatcher implements IMatcher {

		@Override
		public boolean match(Token t) {
			return word.equals(t.getStem());
		}
		
	}
	
	private class Matcher implements IMatcher {

		@Override
		public boolean match(Token t) {
			return word.equals(t.getCoveredText());
		}
		
	}

	public boolean match(Token t, String pos){
		return (this.pos.equals(ANYPOS) || this.pos.equals(pos)) && matcher.match(t);
	}
	
	public SentimentClue(String subjType, String word, String pos,
			String stemmed, String priorP) {
		this.subjType = subjType;
		this.pos = pos;
		this.priorP = priorP;
		this.word = word;
		
		if (stemmed.equals("y")){
			matcher = new StemmerMatcher();
		}
		else{
			matcher = new Matcher();
		}
	}	
	
}
