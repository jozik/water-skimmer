package cnh.annotator.sentiment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

public class ClueListFromPrefixMapResource_impl implements
		ClueListFromPrefixMapResource, SharedResourceObject {
	
	Map<String,List<SentimentClue>> clueListFromPrefixMap = new HashMap<String,List<SentimentClue>>();

	@Override
	public List<SentimentClue> get(String prefix) {
		return clueListFromPrefixMap.get(prefix);
	}
	
	protected void addClue(String prefix, SentimentClue clue){
		List<SentimentClue> list = clueListFromPrefixMap.get(prefix);
		if (list == null){
			list = new ArrayList<SentimentClue>();
			list.add(clue);
			clueListFromPrefixMap.put(prefix, list);
		}
		else {
			list.add(clue);
		}
	}
	
	protected List<String> splitLine(String line){
		List<String> l = new ArrayList<String>();
		for (String str : line.trim().split("\\s+")){
			String [] pair = str.split("=");
			if (!pair[0].equals("len")){
				l.add(pair[1]);
			}
  	  	}
		
		return l;
	}

	@Override
	public void load(DataResource aData) throws ResourceInitializationException {
		InputStream inStr = null;
	    try {
	      // open input stream to data
	      inStr = aData.getInputStream();
	      // read each line
	      BufferedReader reader = new BufferedReader(new InputStreamReader(inStr));
	      String line;
	      while ((line = reader.readLine()) != null) {
	    	  List<String> l = splitLine(line);
	    	  
	    	  //type=weaksubj [len=1 ...] word1=abandoned pos1=adj stemmed1=n priorpolarity=negative
	    	  SentimentClue clue = new SentimentClue(l.get(0),l.get(1),l.get(2),l.get(3),l.get(4));
	    	  addClue(StringUtils.substring(l.get(1), 0,3),clue);
	    	  
	      }
	    } catch (IOException e) {
	      throw new ResourceInitializationException(e);
	    } finally {
	      if (inStr != null) {
	        try {
	          inStr.close();
	        } catch (IOException e) {
	        }
	      }
	    }
		
		
	}

}
