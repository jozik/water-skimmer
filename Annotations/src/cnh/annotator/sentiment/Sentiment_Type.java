
/* First created by JCasGen Mon Jul 16 14:55:05 EDT 2012 */
package cnh.annotator.sentiment;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Jul 16 14:55:05 EDT 2012
 * @generated */
public class Sentiment_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Sentiment_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Sentiment_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Sentiment(addr, Sentiment_Type.this);
  			   Sentiment_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Sentiment(addr, Sentiment_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Sentiment.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.annotator.sentiment.Sentiment");
 
  /** @generated */
  final Feature casFeat_subjType;
  /** @generated */
  final int     casFeatCode_subjType;
  /** @generated */ 
  public String getSubjType(int addr) {
        if (featOkTst && casFeat_subjType == null)
      jcas.throwFeatMissing("subjType", "cnh.annotator.sentiment.Sentiment");
    return ll_cas.ll_getStringValue(addr, casFeatCode_subjType);
  }
  /** @generated */    
  public void setSubjType(int addr, String v) {
        if (featOkTst && casFeat_subjType == null)
      jcas.throwFeatMissing("subjType", "cnh.annotator.sentiment.Sentiment");
    ll_cas.ll_setStringValue(addr, casFeatCode_subjType, v);}
    
  
 
  /** @generated */
  final Feature casFeat_priorP;
  /** @generated */
  final int     casFeatCode_priorP;
  /** @generated */ 
  public String getPriorP(int addr) {
        if (featOkTst && casFeat_priorP == null)
      jcas.throwFeatMissing("priorP", "cnh.annotator.sentiment.Sentiment");
    return ll_cas.ll_getStringValue(addr, casFeatCode_priorP);
  }
  /** @generated */    
  public void setPriorP(int addr, String v) {
        if (featOkTst && casFeat_priorP == null)
      jcas.throwFeatMissing("priorP", "cnh.annotator.sentiment.Sentiment");
    ll_cas.ll_setStringValue(addr, casFeatCode_priorP, v);}
    
  
 
  /** @generated */
  final Feature casFeat_tokenIndex;
  /** @generated */
  final int     casFeatCode_tokenIndex;
  /** @generated */ 
  public long getTokenIndex(int addr) {
        if (featOkTst && casFeat_tokenIndex == null)
      jcas.throwFeatMissing("tokenIndex", "cnh.annotator.sentiment.Sentiment");
    return ll_cas.ll_getLongValue(addr, casFeatCode_tokenIndex);
  }
  /** @generated */    
  public void setTokenIndex(int addr, long v) {
        if (featOkTst && casFeat_tokenIndex == null)
      jcas.throwFeatMissing("tokenIndex", "cnh.annotator.sentiment.Sentiment");
    ll_cas.ll_setLongValue(addr, casFeatCode_tokenIndex, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Sentiment_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_subjType = jcas.getRequiredFeatureDE(casType, "subjType", "uima.cas.String", featOkTst);
    casFeatCode_subjType  = (null == casFeat_subjType) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_subjType).getCode();

 
    casFeat_priorP = jcas.getRequiredFeatureDE(casType, "priorP", "uima.cas.String", featOkTst);
    casFeatCode_priorP  = (null == casFeat_priorP) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_priorP).getCode();

 
    casFeat_tokenIndex = jcas.getRequiredFeatureDE(casType, "tokenIndex", "uima.cas.Long", featOkTst);
    casFeatCode_tokenIndex  = (null == casFeat_tokenIndex) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tokenIndex).getCode();

  }
}



    