package cnh.annotator.sentiment;

import java.util.List;

public interface ClueListFromPrefixMapResource {

	public List<SentimentClue> get(String prefix); 
}
