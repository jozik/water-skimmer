package cnh.annotator.sentiment

class SentimentUtil {
	
	private static final String NOUN = 'noun'
	private static final String VERB = 'verb'
	private static final String ADJ = 'adj'
	private static final String ADVERB = 'adverb'
	
	public static Map<String,String> getPosMap(){
		return ['NN' : NOUN, 'NNP' : NOUN, 'NNPS' : NOUN, 'NNS' : NOUN, 
               'JJ' : ADJ, 'JJR' : ADJ, 'JJS' : ADJ, 'RB' : ADVERB, 'RBR' : ADVERB,
               'RBS' : ADVERB, 'VB' : VERB, 'VBD' : VERB, 'VBG' : VERB, 'VBN' : VERB,
               'VBP' : VERB, 'VBZ' : VERB]
	}

}
