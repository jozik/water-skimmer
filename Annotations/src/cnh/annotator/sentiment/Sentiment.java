

/* First created by JCasGen Mon Jul 16 14:55:05 EDT 2012 */
package cnh.annotator.sentiment;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Jul 16 14:55:05 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/SentimentTypeSystem.xml
 * @generated */
public class Sentiment extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Sentiment.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Sentiment() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Sentiment(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Sentiment(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Sentiment(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: subjType

  /** getter for subjType - gets 
   * @generated */
  public String getSubjType() {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_subjType == null)
      jcasType.jcas.throwFeatMissing("subjType", "cnh.annotator.sentiment.Sentiment");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Sentiment_Type)jcasType).casFeatCode_subjType);}
    
  /** setter for subjType - sets  
   * @generated */
  public void setSubjType(String v) {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_subjType == null)
      jcasType.jcas.throwFeatMissing("subjType", "cnh.annotator.sentiment.Sentiment");
    jcasType.ll_cas.ll_setStringValue(addr, ((Sentiment_Type)jcasType).casFeatCode_subjType, v);}    
   
    
  //*--------------*
  //* Feature: priorP

  /** getter for priorP - gets 
   * @generated */
  public String getPriorP() {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_priorP == null)
      jcasType.jcas.throwFeatMissing("priorP", "cnh.annotator.sentiment.Sentiment");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Sentiment_Type)jcasType).casFeatCode_priorP);}
    
  /** setter for priorP - sets  
   * @generated */
  public void setPriorP(String v) {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_priorP == null)
      jcasType.jcas.throwFeatMissing("priorP", "cnh.annotator.sentiment.Sentiment");
    jcasType.ll_cas.ll_setStringValue(addr, ((Sentiment_Type)jcasType).casFeatCode_priorP, v);}    
   
    
  //*--------------*
  //* Feature: tokenIndex

  /** getter for tokenIndex - gets Token index of the underlying token. (Note that the sentiment is token based in this AE.)
   * @generated */
  public long getTokenIndex() {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_tokenIndex == null)
      jcasType.jcas.throwFeatMissing("tokenIndex", "cnh.annotator.sentiment.Sentiment");
    return jcasType.ll_cas.ll_getLongValue(addr, ((Sentiment_Type)jcasType).casFeatCode_tokenIndex);}
    
  /** setter for tokenIndex - sets Token index of the underlying token. (Note that the sentiment is token based in this AE.) 
   * @generated */
  public void setTokenIndex(long v) {
    if (Sentiment_Type.featOkTst && ((Sentiment_Type)jcasType).casFeat_tokenIndex == null)
      jcasType.jcas.throwFeatMissing("tokenIndex", "cnh.annotator.sentiment.Sentiment");
    jcasType.ll_cas.ll_setLongValue(addr, ((Sentiment_Type)jcasType).casFeatCode_tokenIndex, v);}    
  }

    