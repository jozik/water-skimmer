/**
 * 
 */
package cnh.annotator.title;

import org.apache.uima.jcas.JCas;

import anl.mifs.types.DocumentMetaData;
import cnh.annotator.types.Title;

/**
 * Inserts the title into jcas contents and adds a title
 * annotation. 
 * 
 * @author Nick Collier
 */
public class TitleMerger {

	public void mergeTitle(JCas jcas, DocumentMetaData docData, String contents) {
		String title = docData.getTitle();
		if (title.length() > 0) {
			if (!title.endsWith(".")) title = title + ".";
			title += " ";
			contents = title + contents;
		}
		
		jcas.setDocumentText(contents);
		docData.setEnd(contents.length());
		if (!jcas.getAnnotationIndex(DocumentMetaData.type).iterator().hasNext()) {
			docData.addToIndexes();
		}
		
		Title titleAnn = new Title(jcas, 0, title.length());
		titleAnn.addToIndexes();
	}
}
