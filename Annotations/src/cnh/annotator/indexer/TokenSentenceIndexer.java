/**
 * 
 */
package cnh.annotator.indexer;

import opennlp.uima.Sentence;
import opennlp.uima.Token;

import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

import cnh.annotator.types.CNHDocument;

/**
 * Assigns an index to Tokens indicating their position in the Token list. Also
 * assigns start and end tokens to any sentence annotations it finds.
 * 
 * @author Nick Collier
 */
public class TokenSentenceIndexer extends JCasAnnotator_ImplBase {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.uima.analysis_component.JCasAnnotator_ImplBase#process(org
	 * .apache.uima.jcas.JCas)
	 */
	@Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		int index = 0;
		for (FSIterator<Annotation> iter = jcas.getAnnotationIndex(Token.type).iterator(); iter.hasNext();) {
			Token token = (Token)iter.next();
			token.setTokenIndex(index);
			index++;
		}
		
		CNHDocument doc = new CNHDocument(jcas);
		doc.setTokenCount(index - 1);
		doc.addToIndexes();
		
		AnnotationIndex<Annotation> tokenIdx = jcas.getAnnotationIndex(Token.type);
		for (FSIterator<Annotation> sIter = jcas.getAnnotationIndex(Sentence.type).iterator(); sIter.hasNext(); ) {
			Sentence sentence = (Sentence)sIter.next();
			FSIterator<Annotation> tIter = tokenIdx.subiterator(sentence);
			Token token = ((Token)tIter.next());
			int startTokenIdx = token.getTokenIndex();
			while(tIter.hasNext()) {
				token = ((Token)tIter.next());
			}
			int endTokenIdx = token.getTokenIndex();
			sentence.setStartTokenIndex(startTokenIdx);
			sentence.setEndTokenIndex(endTokenIdx);
		}
	}
}
