
/* First created by JCasGen Wed Aug 01 15:54:42 EDT 2012 */
package cnh.annotator.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Fri Aug 17 10:09:51 EDT 2012
 * @generated */
public class CNHDocument_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (CNHDocument_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = CNHDocument_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new CNHDocument(addr, CNHDocument_Type.this);
  			   CNHDocument_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new CNHDocument(addr, CNHDocument_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = CNHDocument.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.annotator.types.CNHDocument");
 
  /** @generated */
  final Feature casFeat_tokenCount;
  /** @generated */
  final int     casFeatCode_tokenCount;
  /** @generated */ 
  public int getTokenCount(int addr) {
        if (featOkTst && casFeat_tokenCount == null)
      jcas.throwFeatMissing("tokenCount", "cnh.annotator.types.CNHDocument");
    return ll_cas.ll_getIntValue(addr, casFeatCode_tokenCount);
  }
  /** @generated */    
  public void setTokenCount(int addr, int v) {
        if (featOkTst && casFeat_tokenCount == null)
      jcas.throwFeatMissing("tokenCount", "cnh.annotator.types.CNHDocument");
    ll_cas.ll_setIntValue(addr, casFeatCode_tokenCount, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public CNHDocument_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_tokenCount = jcas.getRequiredFeatureDE(casType, "tokenCount", "uima.cas.Integer", featOkTst);
    casFeatCode_tokenCount  = (null == casFeat_tokenCount) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_tokenCount).getCode();

  }
}



    