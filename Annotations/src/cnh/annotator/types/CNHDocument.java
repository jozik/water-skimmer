

/* First created by JCasGen Wed Aug 01 15:54:42 EDT 2012 */
package cnh.annotator.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Fri Aug 17 10:09:50 EDT 2012
 * XML source: /Users/nick/Documents/repos/CNH/Annotations/desc/CNHTypeSystem.xml
 * @generated */
public class CNHDocument extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(CNHDocument.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected CNHDocument() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public CNHDocument(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public CNHDocument(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public CNHDocument(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: tokenCount

  /** getter for tokenCount - gets 
   * @generated */
  public int getTokenCount() {
    if (CNHDocument_Type.featOkTst && ((CNHDocument_Type)jcasType).casFeat_tokenCount == null)
      jcasType.jcas.throwFeatMissing("tokenCount", "cnh.annotator.types.CNHDocument");
    return jcasType.ll_cas.ll_getIntValue(addr, ((CNHDocument_Type)jcasType).casFeatCode_tokenCount);}
    
  /** setter for tokenCount - sets  
   * @generated */
  public void setTokenCount(int v) {
    if (CNHDocument_Type.featOkTst && ((CNHDocument_Type)jcasType).casFeat_tokenCount == null)
      jcasType.jcas.throwFeatMissing("tokenCount", "cnh.annotator.types.CNHDocument");
    jcasType.ll_cas.ll_setIntValue(addr, ((CNHDocument_Type)jcasType).casFeatCode_tokenCount, v);}    
  }

    