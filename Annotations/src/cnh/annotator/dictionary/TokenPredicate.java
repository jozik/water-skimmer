/**
 * 
 */
package cnh.annotator.dictionary;

import opennlp.uima.Token;

/**
 * Interface for classes that apply some predicate condition to a Token.
 * 
 * @author Nick Collier
 */
public interface TokenPredicate {

	/**
	 * Applies this predicate to the specified token.
	 * 
	 * @param token the token to apply the predicate to
	 * 
	 * @return true if the token "passes" the predicate, otherwise false.
	 */
	boolean apply(Token token);
}
