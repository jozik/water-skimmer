/**
 * 
 */
package cnh.annotator.dictionary;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

/**
 * Abstract FeatureSetter implementation that sets the start and end token
 * features.
 * 
 * @author Nick Collier
 */
public abstract class AbstractFeatureSetter implements FeatureSetter {

	private Feature tokenIndexStartFeature, tokenIndexEndFeature;

	public AbstractFeatureSetter(String typeName, JCas cas) throws AnalysisEngineProcessException {
		Type annType = cas.getTypeSystem().getType(typeName);
		tokenIndexStartFeature = annType.getFeatureByBaseName("startTokenIndex");
		if (tokenIndexStartFeature == null) {
			throw new AnalysisEngineProcessException(DictionaryAnnotator.MESSAGE_DIGEST, "dictionary_feature_error",
					new Object[] { "startTokenIndex" });
		}
		
		tokenIndexEndFeature = annType.getFeatureByBaseName("endTokenIndex");
		if (tokenIndexEndFeature == null) {
			throw new AnalysisEngineProcessException(DictionaryAnnotator.MESSAGE_DIGEST, "dictionary_feature_error",
					new Object[] { "endTokenIndex" });
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * cnh.annotator.dictionary.FeatureSetter#run(org.apache.uima.jcas.tcas.
	 * Annotation,
	 * org.apache.uima.annotator.dict_annot.dictionary.DictionaryMatch, int,
	 * int)
	 */
	@Override
	public void run(Annotation annotation, int startToken, int endToken) {
		annotation.setLongValue(tokenIndexStartFeature, startToken);
		annotation.setLongValue(tokenIndexEndFeature, endToken);
	}
}
