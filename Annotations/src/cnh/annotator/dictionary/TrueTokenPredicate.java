/**
 * 
 */
package cnh.annotator.dictionary;

import opennlp.uima.Token;

/**
 * Token predicate that always returns true.
 * 
 * @author Nick Collier
 */
public class TrueTokenPredicate implements TokenPredicate {

	/* (non-Javadoc)
	 * @see cnh.annotator.dictionary.TokenPredicate#apply(opennlp.uima.Token)
	 */
	@Override
	public final boolean apply(Token token) {
		return true;
	}
}
