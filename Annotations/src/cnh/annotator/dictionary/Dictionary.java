/**
 * 
 */
package cnh.annotator.dictionary;

import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.annotator.dict_annot.dictionary.DictionaryMatch;
import org.apache.uima.annotator.dict_annot.dictionary.impl.FeaturePathInfo;
import org.apache.uima.annotator.dict_annot.impl.FeaturePathInfo_impl;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;

/**
 * @author Nick Collier
 */
public class Dictionary {

	private org.apache.uima.annotator.dict_annot.dictionary.Dictionary dictionary;
	private DictionaryTypeData typeData;
	private FeaturePathInfo featurePathInfo = new FeaturePathInfo_impl();

	public Dictionary(
			org.apache.uima.annotator.dict_annot.dictionary.Dictionary dictionary,
			DictionaryTypeData data) {
		this.dictionary = dictionary;
		this.typeData = data;
	}

	/**
	 * Checks if at the current position in the token array a match in the
	 * dictionary is found.
	 * 
	 * @param pos
	 *            current array position
	 * 
	 * @param annotFSs
	 *            input annotation FS array
	 * 
	 * @return returns a DictionaryMatch object in case a match was found. If no
	 *         match was found, null is returned
	 */
	public DictionaryMatch matchEntry(int pos, AnnotationFS[] annotFSs) {
		return dictionary.matchEntry(pos, annotFSs, featurePathInfo);
	}
	
	
	/**
	 * Gets the TokenPredicate for the specified match. 
	 * 
	 * @param match
	 * @return the TokenPredicate for the specified match. 
	 */
	public TokenPredicate getPredicate(DictionaryMatch match) {
		return typeData.getPredicate(match);
	}

	/**
	 * Creates the FeatureSetter that will set the features on the 
	 * annotation associated with this dictionary.
	 * 
	 * @param cas
	 * @return the created FeatureSetter.
	 * 
	 * @throws AnalysisEngineProcessException
	 */
	public FeatureSetter createFeatureSetter(JCas cas) throws AnalysisEngineProcessException {
		return typeData.createFeatureSetter(cas);
	}

	public String getTypeName() {
		return typeData.getTypeName();
	}

	public int getEntryCount() {
		return dictionary.getEntryCount();
	}

	public boolean contains(String[] words) {
		return dictionary.contains(words);
	}
}
