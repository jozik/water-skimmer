/**
 * 
 */
package cnh.annotator.dictionary;

import java.util.ArrayList;
import java.util.List;

import opennlp.uima.Token;

/**
 * TokenPredicate that evaluates to true if all its children
 * return true; 
 * 
 * @author Nick Collier
 */
public class AndTokenPredicate implements TokenPredicate {
	
	List<TokenPredicate> predicates = new ArrayList<TokenPredicate>();
	
	/**
	 * Adds the specified predicate to this AndTokenPredicate.
	 * 
	 * @param predicate
	 */
	public void addTokenPredicate(TokenPredicate predicate) {
		predicates.add(predicate);
	}

	/* (non-Javadoc)
	 * @see cnh.annotator.dictionary.TokenPredicate#apply(opennlp.uima.Token)
	 */
	@Override
	public boolean apply(Token token) {
		for (TokenPredicate predicate : predicates) {
			if (!predicate.apply(token)) return false;
		}
		return true;
	}
}
