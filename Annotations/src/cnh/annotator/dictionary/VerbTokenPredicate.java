/**
 * 
 */
package cnh.annotator.dictionary;

/**
 * Test whether or not a Token is a verb.
 * 
 * @author Nick Collier
 */
public class VerbTokenPredicate extends POSTokenPredicate {
	
	public VerbTokenPredicate() {
		posSet.add("VB");
		posSet.add("VBD");
		posSet.add("VBG");
		posSet.add("VBN");
		posSet.add("VBP");
		posSet.add("VBZ");
	}
}
