/**
 * 
 */
package cnh.annotator.dictionary;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;

import opennlp.uima.Token;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.annotator.dict_annot.dictionary.DictionaryMatch;
import org.apache.uima.cas.FSIterator;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.Level;
import org.apache.uima.util.Logger;

import anl.mifs.types.DocumentMetaData;

import cnh.water.annotation.InstitutionAnnotation;

/**
 * Does a dictionary like lookup on tokens and annotates them with a specified
 * annotation, and optionally setting a feature value.
 * 
 * Much of this code is copied from UIMA's DictionaryAnnotator class.
 * 
 * @author NC
 */
public class DictionaryAnnotator extends JCasAnnotator_ImplBase {

	/**
	 * Message catalog
	 */
	public static final String MESSAGE_DIGEST = "cnh.annotator.dictionary.DictionaryAnnotatorMessages";

	// DictionaryFile configuration parameter name
	private static final String DICTIONARY_FILE = "DictionaryFile";
	// type of input annotation to match on
	private static final String INPUT_MATCH_TYPE = "InputMatchType";

	// dictionaries used with this annotator
	private List<Dictionary> dictionaries;
	private Logger logger;
	private String inputMatchTypeStr;
	private Type inputMatchType = null;

	@Override
	public void process(JCas cas) throws AnalysisEngineProcessException {
		if (inputMatchType == null) {
			inputMatchType = cas.getTypeSystem().getType(inputMatchTypeStr);
		}

		// create array from FSIterator
		ArrayList<AnnotationFS> inputTypeAnnots = new ArrayList<AnnotationFS>();
		for (FSIterator<Annotation> it = cas.getAnnotationIndex(this.inputMatchType).iterator(); it.hasNext();) {
			inputTypeAnnots.add(it.next());
		}
		AnnotationFS[] annotFSs = inputTypeAnnots.toArray(new AnnotationFS[] {});

		for (Dictionary dictionary : dictionaries) {
			try {
				FeatureSetter featureSetter = dictionary.createFeatureSetter(cas);

				Type annType = cas.getTypeSystem().getType(dictionary.getTypeName());
				if (annType == null) {
					throw new AnalysisEngineProcessException(MESSAGE_DIGEST, "dictionary_type_error",
							new Object[] { dictionary.getTypeName() });
				}

				int currentPos = 0;
				while (currentPos < annotFSs.length) {
					DictionaryMatch match = dictionary.matchEntry(currentPos, annotFSs);

					if (match != null) {
						// -- we have found a match starting at the current
						// position --

						// get match length of the match
						int matchLength = match.getMatchLength();

						// create annotation for the match we found
						int start = annotFSs[currentPos].getBegin();
						int end = annotFSs[currentPos + matchLength - 1].getEnd();

						TokenPredicate predicate = dictionary.getPredicate(match);
						// do multi-token match: if any of the tokens pass the
						// predicate
						// then that's a match
						for (int tokenIndex = currentPos, n = currentPos + matchLength; tokenIndex < n; tokenIndex++) {
							Token token = (Token) annotFSs[tokenIndex];
							if (predicate.apply(token)) {
								Annotation annotation = (Annotation) cas.getCas().createAnnotation(annType, start, end);
								featureSetter.run(annotation, currentPos, currentPos + matchLength - 1);
								annotation.addToIndexes();
								break;
							}
						}

						// adjust current array position, add match length
						currentPos = currentPos + matchLength;
					} else {
						// -- no match was found, go on with the next token --
						currentPos++;
					}
				}

			} catch (Exception ex) {
				throw new AnalysisEngineProcessException(MESSAGE_DIGEST, "dictionary_type_error", new Object[] {
						dictionary.getTypeName(), ex.getMessage() });
			}
		}
	}

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);

		this.logger = this.getContext().getLogger();
		// get input match type
		this.inputMatchTypeStr = (String) this.getContext().getConfigParameterValue(INPUT_MATCH_TYPE);

		// get configuration parameter settings
		// get parameter ConceptFiles, default is an empty array
		String fname = (String) getContext().getConfigParameterValue(DICTIONARY_FILE);
		dictionaries = new ArrayList<Dictionary>();

		try {
			InputStream in = this.getClass().getClassLoader().getResourceAsStream(fname);
			DictionaryFileParser parser = new DictionaryFileParser();
			dictionaries.addAll(parser.parse(in));
			logger.log(Level.INFO, "Created dictionary from: " + fname);
			in.close();
		} catch (IOException ex) {
			throw new ResourceInitializationException(MESSAGE_DIGEST, "dictionary_file_error", new Object[] { fname },
					ex);
		} catch (XMLStreamException ex) {
			throw new ResourceInitializationException(MESSAGE_DIGEST, "dictionary_file_error", new Object[] { fname },
					ex);
		}
	}
}
