/**
 * 
 */
package cnh.annotator.dictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.annotator.dict_annot.dictionary.DictionaryMatch;
import org.apache.uima.annotator.dict_annot.dictionary.EntryMetaData;
import org.apache.uima.cas.Feature;
import org.apache.uima.cas.Type;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;

/**
 * Stores type names and mappings for the dictionary annotator.
 * 
 * @author NC
 */
public class DictionaryTypeData {

	private static class FS extends AbstractFeatureSetter {

		private List<Pair<Feature, String>> map = new ArrayList<Pair<Feature, String>>();

		public FS(String typeName, JCas cas, List<Pair<String, String>> fMap) throws AnalysisEngineProcessException {
			super(typeName, cas);
			Type annType = cas.getTypeSystem().getType(typeName);
			for (Pair<String, String> entry : fMap) {
				Feature feature = annType.getFeatureByBaseName(entry.getLeft());
				if (feature == null) {
					throw new AnalysisEngineProcessException(DictionaryAnnotator.MESSAGE_DIGEST,
							"dictionary_feature_error", new Object[] { entry.getKey() });
				}
				map.add(new ImmutablePair<Feature, String>(feature, entry.getValue()));
			}
		}

		@Override
		public void run(Annotation annotation, int startToken, int endToken) {
			super.run(annotation, startToken, endToken);
			for (Pair<Feature, String> pair : map) {
				annotation.setStringValue(pair.getLeft(), pair.getRight());
			}
		}
	}

	private String typeName;
	private List<Pair<String, String>> featureMap = new ArrayList<Pair<String, String>>();
	private Map<Integer, TokenPredicate> tokenMap = new HashMap<Integer, TokenPredicate>();

	public DictionaryTypeData(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * Creates a FeatureSetter that will set the appropriate features on the annotation type 
	 * associated with this DictionaryTypeData.
	 * 
	 * @param cas
	 * 
	 * @return the created FeatureSetter
	 * 
	 * @throws AnalysisEngineProcessException
	 */
	public FeatureSetter createFeatureSetter(JCas cas) throws AnalysisEngineProcessException {
		return new FS(typeName, cas, featureMap);
	}

	/**
	 * Maps the specified dictionary entry ids to the specified featureValue.
	 * Matches on these entry ids will have the feature set to the specified
	 * value.
	 * 
	 * @param featureName
	 * @param featureValue
	 */
	public void addMapping(String featureName, String featureValue) {
		featureMap.add(new ImmutablePair<String, String>(featureName, featureValue));
	}

	/**
	 * Maps the predicate to the dictionary entry id.
	 * 
	 * @param id
	 * @param predicate
	 */
	public void addPredicateMapping(Integer id, TokenPredicate predicate) {
		tokenMap.put(id, predicate);
	}

	/**
	 * Gets the TokenPredicate for the specified match.
	 * 
	 * @param match
	 * @return the TokenPredicate for the specified match.
	 */
	public TokenPredicate getPredicate(DictionaryMatch match) {
		EntryMetaData data = match.getMatchMetaData();
		return tokenMap.get(data.getId());
	}

	/**
	 * Gets the annotation type for this DictionaryTypeData.
	 * 
	 * @return the annotation type for this DictionaryTypeData.
	 */
	public String getTypeName() {
		return typeName;
	}

}
