/**
 * 
 */
package cnh.annotator.dictionary.water;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Contains the current categories as read from the dictionary file.
 * 
 * @author Nick Collier
 */
public class Categories {
	
	public static final String WATER_CATEGORIES = "WATER_CATEGORIES"; 
	public static final String LOCATION_CATEGORIES = "LOCATION_CATEGORIES";
	public static final String INSTITUTION_CATEGORIES = "INSTITUTION_CATEGORIES";

	private static final String PRIMARY = "WATER";

	private static final QName FEATURE_MAP = new QName("feature_map");
	private static final QName NAME = new QName("name");
	private static final QName VALUE = new QName("value");

	private static Categories instance = null;

	public static Categories getInstance() {
		if (instance == null) {
			instance = new Categories();
		}
		return instance;
	}

	private Map<String, Set<String>> categories = new HashMap<String, Set<String>>();

	public Categories() {
		try {
			Properties props = new Properties();
			props.load(getClass().getResourceAsStream("/category.props"));
			for (Object category : props.values()) {
				categories.put(category.toString().trim(), new HashSet<String>());
			}
			loadCategories(props);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void loadCategories(Properties props) throws XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = factory.createXMLEventReader(getClass().getResourceAsStream("/dictionary.xml"));

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(FEATURE_MAP)) {
					parseFeatureMap(reader, props);
				}
			}
		}
	}

	private void parseFeatureMap(XMLEventReader reader, Properties props) throws XMLStreamException {
		Set<String> set = null;
		String value = null;
		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(NAME)) {
					String name = getString(reader);
					String category = props.getProperty(name);
					if (category != null) {
						set = categories.get(category);
					}
			
				} else if (elmName.equals(VALUE)) {
					value = getString(reader);
				}

			} else if (evt.isEndElement() && evt.asEndElement().getName().equals(FEATURE_MAP)) {
				if (set != null) set.add(value);
				break;
			}
		}
	}

	public List<String> getCategories(String categoryName) {
		Set<String> set = categories.get(categoryName);
		if (set == null) throw new IllegalArgumentException("Unknown category name: '" + categoryName + "'");
		return new ArrayList<String>(set);
	}

	/**
	 * Gets the primary category.
	 * 
	 * @return the primary category.
	 */
	public String getPrimaryCategory() {
		return PRIMARY;
	}

	private String getString(XMLEventReader reader) throws XMLStreamException {
		XMLEvent evt = reader.peek();
		if (!evt.isCharacters())
			return "";

		Characters characters = reader.nextEvent().asCharacters();
		return characters.getData().trim();
	}

}
