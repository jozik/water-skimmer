/**
 * 
 */
package cnh.annotator.dictionary.water;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

/**
 * Creates a Dictionary from the the water keywords.csv file. The
 * keyword csv file should have 3 columns: keyword, category and predicate.
 * The predicate is optional. 
 * 
 * @author Nick Collier
 */
public class WaterDictionaryCreator {
	
	private static class Entry {
		String keyword, predicate;
	}

	public void run(XMLStreamWriter writer, String input) throws IOException, XMLStreamException, FactoryConfigurationError {
		Map<String, List<Entry>> inputMap = new HashMap<String, List<Entry>>();
		fillInputMap(input, inputMap);
		writeOutput(writer, inputMap);
	}

	private void writeOutput(XMLStreamWriter writer, Map<String, List<Entry>> inputMap) throws IOException, XMLStreamException,
			FactoryConfigurationError {
		for (String category : inputMap.keySet()) {
			
			writer.writeStartElement("dictionary");
			writer.writeAttribute("language", "en");
			writer.writeAttribute("caseNormalization", "true");
			writer.writeAttribute("multiWordEntries", "true");
			writer.writeAttribute("multiWordSeparator", " ");

			writer.writeStartElement("type");

			writer.writeStartElement("type_name");
			writer.writeCharacters("cnh.water.annotation.WaterAnnotation");
			writer.writeEndElement();
			// end of type element
			writer.writeEndElement();

			writer.writeStartElement("entries");
			
			writer.writeStartElement("feature_map");
			writer.writeStartElement("name");
			writer.writeCharacters("waterCategory");
			writer.writeEndElement();
			writer.writeStartElement("value");
			writer.writeCharacters(category);
			writer.writeEndElement();
			writer.writeEndElement();
			for (Entry entry : inputMap.get(category)) {
				writer.writeStartElement("entry");
				writer.writeStartElement("key");
				writer.writeCharacters(entry.keyword);
				writer.writeEndElement();
				writer.writeStartElement("predicate");
				writer.writeCharacters(entry.predicate);
				writer.writeEndElement();
				writer.writeEndElement();
			}
			

			// end the entry
			writer.writeEndElement();

			writer.writeEndElement();
		}
	}

	private void fillInputMap(String input, Map<String, List<Entry>> inputMap) throws IOException {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(input));
			// skip first line
			reader.readLine();

			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] vals = line.trim().split(",");
				int length = vals.length;
				if (length < 2 || length > 3)
					throw new IOException("Invalid input file: " + input);
				String feature = vals[1].trim();
				List<Entry> list = inputMap.get(feature);
				if (list == null) {
					list = new ArrayList<Entry>();
					inputMap.put(feature, list);
				}
				
				String keyword = vals[0].trim();
				String predicate = length == 3 ? vals[2].trim() : "";
				Entry entry = new Entry();
				entry.keyword = keyword;
				entry.predicate = predicate;
				list.add(entry);

			}
		} finally {
			if (reader != null)
				reader.close();
		}
	}
}
