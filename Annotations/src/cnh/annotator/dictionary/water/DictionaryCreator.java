/**
 * 
 */
package cnh.annotator.dictionary.water;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import cnh.annotator.dictionary.water.TwoCategoryDictionaryCreator.TypeData;
import cnh.water.annotation.InstitutionAnnotation;
import cnh.water.annotation.LocationAnnotation;
/**
 * Creates the dictionary xml file.
 * @author Nick Collier
 */
public class DictionaryCreator {
	
	private static final String WATER_INPUT = "./data/WaterKeywords_v5.csv";
	
	private static final String[] LOCATION_INPUT = {"./data/locations/GrandJunction.csv", "./data/locations/LasVegas.csv",
		"./data/locations/Phoenix.csv", "./data/locations/Tucson.csv"};
	
	private static final String[] INSTITUTION_INPUT = {"./data/institutions/GrandJunction.csv"};
	
	private static final String OUTPUT = "./resources/dictionary.xml";

	public void run() throws IOException, XMLStreamException, FactoryConfigurationError {
		XMLStreamWriter writer = XMLOutputFactory.newInstance().createXMLStreamWriter(new FileOutputStream(OUTPUT),
				"UTF-8");
		writer.writeStartDocument();
		writer.writeStartElement("dictionaries");
		
		new WaterDictionaryCreator().run(writer, WATER_INPUT);
		
		TypeData typeData = new TypeData();
		typeData.annotationType = LocationAnnotation.class.getName();
		typeData.categoryName = "location";
		typeData.subCategoryName = "sublocation";
		
		for (String in : LOCATION_INPUT) {
			File file = new File(in);
			String categoryValue = file.getName().substring(0, file.getName().indexOf(".")).toUpperCase();
			categoryValue += "_LOCATIONS";
			typeData.categoryValue = categoryValue;
			new TwoCategoryDictionaryCreator().run(writer, in, typeData);
		}
		
		typeData.annotationType = InstitutionAnnotation.class.getName();
		typeData.categoryName = "location";
		typeData.subCategoryName = "name";
		
		for (String in : INSTITUTION_INPUT) {
			File file = new File(in);
			String categoryValue = file.getName().substring(0, file.getName().indexOf(".")).toUpperCase();
			categoryValue += "_INSTITUTIONS";
			typeData.categoryValue = categoryValue;
			new TwoCategoryDictionaryCreator().run(writer, in, typeData);
		}
		
		writer.writeEndElement();
		writer.writeEndDocument();
		System.out.println("DONE.");
	}
	
	public static void main(String[] args) {
		try {
			new DictionaryCreator().run();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
