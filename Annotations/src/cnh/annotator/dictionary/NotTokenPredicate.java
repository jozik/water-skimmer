/**
 * 
 */
package cnh.annotator.dictionary;

import opennlp.uima.Token;

/**
 * Token predicate that reverses the result of a delegate predicate. 
 * 
 * @author Nick Collier
 */
public class NotTokenPredicate implements TokenPredicate {
	
	private TokenPredicate predicate;
	
	public NotTokenPredicate(TokenPredicate predicate) {
		this.predicate = predicate;
	}

	/* (non-Javadoc)
	 * @see cnh.annotator.dictionary.TokenPredicate#apply(opennlp.uima.Token)
	 */
	@Override
	public boolean apply(Token token) {
		return !predicate.apply(token);
	}
}
