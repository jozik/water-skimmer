/**
 * 
 */
package cnh.annotator.dictionary;

import java.util.HashSet;
import java.util.Set;

import opennlp.uima.Token;

/**
 * Tests whether or not a token's POS is in a set of
 * POS strings. 
 * 
 * @author Nick Collier
 */
public abstract class POSTokenPredicate implements TokenPredicate {
	
	protected Set<String> posSet = new HashSet<String>();

	/* (non-Javadoc)
	 * @see cnh.annotator.dictionary.TokenPredicate#apply(opennlp.uima.Token)
	 */
	@Override
	public boolean apply(Token token) {
		return posSet.contains(token.getPos());
	}
}
