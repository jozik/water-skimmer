/**
 * 
 */
package cnh.annotator.dictionary;

import java.util.HashMap;
import java.util.Map;

/**
 * Parses a token predicate string into a TokenPredicate. The valid
 * token string units are "verb", "noun", "proper_noun" and a not symobl "!".
 * "!" should immediately preceed the other units, for example, "!verb". Multiple
 * units can be separated and will be and'ed together.
 * 
 * @author Nick Collier
 */
public class TokenPredicateFactory {
	
	private static final String NOT = "!";
	private static final String VERB = "verb";
	private static final String NOUN = "noun";
	private static final String PROPER_NOUN = "proper_noun";
	
	private static TokenPredicateFactory instance = new TokenPredicateFactory();
	private static final TokenPredicate TRUE_PREDICATE = new TrueTokenPredicate();
	
	public static TokenPredicateFactory getInstance() {
		return instance;
	}
	
	private Map<String, TokenPredicate> predicateMap = new HashMap<String, TokenPredicate>();
	
	private TokenPredicateFactory() {
		predicateMap.put(VERB, new VerbTokenPredicate());
		predicateMap.put(NOUN, new NounTokenPredicate());
		predicateMap.put(PROPER_NOUN, new ProperNounTokenPredicate());
	}
	
	private TokenPredicate parseSingleToken(String tokenP) {
		boolean not = tokenP.startsWith(NOT);
		if (not) tokenP = tokenP.substring(1, tokenP.length());
		TokenPredicate predicate = predicateMap.get(tokenP);
		if (predicate == null) throw new IllegalArgumentException("Unknown token predicate: '" + tokenP + "'");
		if (not) return new NotTokenPredicate(predicate);
		return predicate;
	}
	
	/**
	 * Parse the token predicate string and return a TokePredicate.
	 * 
	 * @param tokens
	 * @return the TokenPredicate from the parsed String.
	 */
	public TokenPredicate parseTokenString(String tokens) {
		if (tokens.trim().length() == 0) return TRUE_PREDICATE;
		
		String[] preds = tokens.split(" ");
		AndTokenPredicate and = new AndTokenPredicate();
		for (String predicate : preds) {
			and.addTokenPredicate(parseSingleToken(predicate));
		}
		return and;
	}

	
}
