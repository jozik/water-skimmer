/**
 * 
 */
package cnh.annotator.dictionary;

/**
 * Test whether or not a Token is proper noun.
 * 
 * @author Nick Collier
 */
public class ProperNounTokenPredicate extends POSTokenPredicate {
	
	public ProperNounTokenPredicate() {
		posSet.add("NNP");
		posSet.add("NNPS");
	}

}
