/**
 * 
 */
package cnh.annotator.dictionary;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Parses a dictionary file, creating a dictionary.
 * 
 * @author nick
 */
public class DictionaryFileParser {

	private static final QName TYPE_NAME = new QName("type_name");
	private static final QName TYPE = new QName("type");
	private static final QName FEATURE_MAP = new QName("feature_map");
	private static final QName NAME = new QName("name");
	private static final QName VALUE = new QName("value");
	private static final QName ENTRIES = new QName("entries");
	private static final QName ENTRY = new QName("entry");
	private static final QName KEY = new QName("key");
	private static final QName PREDICATE = new QName("predicate");
	private static final QName CASE_NORM = new QName("caseNormalization");
	private static final QName MULTI_WORD = new QName("multiWordEntries");
	private static final QName MULTI_WORD_SEP = new QName("multiWordSeparator");
	private static final QName DICTIONARY = new QName("dictionary");
	private static final QName LANGUAGE = new QName("language");

	public List<Dictionary> parse(InputStream in) throws XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_COALESCING, Boolean.TRUE);
		XMLEventReader reader = factory.createXMLEventReader(in);
		List<Dictionary> dictionaries = new ArrayList<Dictionary>();

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(DICTIONARY)) {
					dictionaries.add(parseDictionary(reader, elm));
				}
			}
		}
		return dictionaries;
	}

	private Dictionary parseDictionary(XMLEventReader reader, StartElement dictionary) throws XMLStreamException {

		DictionaryBuilder builder = new DictionaryBuilder();
		DictionaryTypeData typeData = null;

		boolean caseNorm = Boolean.parseBoolean(dictionary.getAttributeByName(CASE_NORM).getValue());
		boolean multWord = Boolean.parseBoolean(dictionary.getAttributeByName(MULTI_WORD).getValue());
		String sep = null;
		if (dictionary.getAttributeByName(MULTI_WORD_SEP) != null) {
			sep = dictionary.getAttributeByName(MULTI_WORD_SEP).getValue();
		}
		String language = dictionary.getAttributeByName(LANGUAGE).getValue();
		builder.setDictionaryProperties(language, "", caseNorm, multWord, sep);

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(TYPE)) {
					typeData = parseType(reader);
				} else if (elmName.equals(ENTRIES)) {
					parseEntries(reader, typeData, builder);
				}

			} else if (evt.isEndElement()) {
				break;
			}
		}

		return new Dictionary(builder.getDictionary(), typeData);
	}

	private void parseFeatureMap(XMLEventReader reader, DictionaryTypeData typeData) throws XMLStreamException {
		String name = null, value = null;

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(VALUE)) {
					value = getString(reader);
				} else if (elmName.equals(NAME)) {
					name = getString(reader);
				}
			} else if (evt.isEndElement() && evt.asEndElement().getName().equals(FEATURE_MAP)) {
				typeData.addMapping(name, value);
				break;
			}
		}

	}

	private void parseEntries(XMLEventReader reader, DictionaryTypeData typeData, DictionaryBuilder builder)
			throws XMLStreamException {

		String predicate = "", word = "";

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(KEY)) {
					word = getString(reader);
					
				} else if (elmName.equals(PREDICATE)) {
					predicate = getString(reader);
				} else if (elmName.equals(FEATURE_MAP)) {
					parseFeatureMap(reader, typeData);
				}
			} else if (evt.isEndElement()) {
				if (evt.asEndElement().getName().equals(ENTRY)) {
					int id = builder.addWord(word);
					typeData.addPredicateMapping(id,
							TokenPredicateFactory.getInstance().parseTokenString(predicate.trim()));
					word = "";
					predicate = "";
					
				} else if (evt.asEndElement().getName().equals(ENTRIES)) {
					break;
				}
			}
		}
	}

	private DictionaryTypeData parseType(XMLEventReader reader) throws XMLStreamException {

		String typeName = null;
		DictionaryTypeData typeData = null;

		while (reader.hasNext()) {
			XMLEvent evt = reader.nextEvent();
			if (evt.isStartElement()) {
				StartElement elm = evt.asStartElement();
				QName elmName = elm.getName();
				if (elmName.equals(TYPE_NAME)) {
					typeName = getString(reader);
				}
			} else if (evt.isEndElement() && evt.asEndElement().getName().equals(TYPE)) {
				typeData = new DictionaryTypeData(typeName);
				break;
			}
		}

		return typeData;
	}

	private String getString(XMLEventReader reader) throws XMLStreamException {
		XMLEvent evt = reader.peek();
		if (!evt.isCharacters())
			return "";

		Characters characters = reader.nextEvent().asCharacters();
		return characters.getData().trim();
	}
}
