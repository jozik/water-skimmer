/**
 * 
 */
package cnh.annotator.dictionary;

/**
 * Test whether or not a Token is a noun, including
 * proper nouns.
 * 
 * @author Nick Collier
 */
public class NounTokenPredicate extends POSTokenPredicate {
	
	public NounTokenPredicate() {
		posSet.add("NN");
		posSet.add("NNS");
		posSet.add("NNP");
		posSet.add("NNPS");
	}

}
