/**
 * 
 */
package cnh.annotator.dictionary;

import org.apache.uima.jcas.tcas.Annotation;

/** 
 * Sets feature values on a dictionary annotation.
 * 
 * @author Nick Collier
 */
public interface FeatureSetter {
	
	/**
	 * Sets features on the specified annotation.
	 * 
	 * @param annotation
	 * @param startToken
	 * @param endToken
	 */
	void run(Annotation annotation, int startToken, int endToken);

}
