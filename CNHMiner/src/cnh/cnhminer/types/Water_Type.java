
/* First created by JCasGen Wed Feb 22 13:21:39 CST 2012 */
package cnh.cnhminer.types;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Wed Feb 22 13:21:39 CST 2012
 * @generated */
public class Water_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Water_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Water_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Water(addr, Water_Type.this);
  			   Water_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Water(addr, Water_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Water.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("cnh.cnhminer.types.Water");
 
  /** @generated */
  final Feature casFeat_kind;
  /** @generated */
  final int     casFeatCode_kind;
  /** @generated */ 
  public String getKind(int addr) {
        if (featOkTst && casFeat_kind == null)
      jcas.throwFeatMissing("kind", "cnh.cnhminer.types.Water");
    return ll_cas.ll_getStringValue(addr, casFeatCode_kind);
  }
  /** @generated */    
  public void setKind(int addr, String v) {
        if (featOkTst && casFeat_kind == null)
      jcas.throwFeatMissing("kind", "cnh.cnhminer.types.Water");
    ll_cas.ll_setStringValue(addr, casFeatCode_kind, v);}
    
  
 
  /** @generated */
  final Feature casFeat_annotationString;
  /** @generated */
  final int     casFeatCode_annotationString;
  /** @generated */ 
  public String getAnnotationString(int addr) {
        if (featOkTst && casFeat_annotationString == null)
      jcas.throwFeatMissing("annotationString", "cnh.cnhminer.types.Water");
    return ll_cas.ll_getStringValue(addr, casFeatCode_annotationString);
  }
  /** @generated */    
  public void setAnnotationString(int addr, String v) {
        if (featOkTst && casFeat_annotationString == null)
      jcas.throwFeatMissing("annotationString", "cnh.cnhminer.types.Water");
    ll_cas.ll_setStringValue(addr, casFeatCode_annotationString, v);}
    
  
 
  /** @generated */
  final Feature casFeat_canonicalName;
  /** @generated */
  final int     casFeatCode_canonicalName;
  /** @generated */ 
  public String getCanonicalName(int addr) {
        if (featOkTst && casFeat_canonicalName == null)
      jcas.throwFeatMissing("canonicalName", "cnh.cnhminer.types.Water");
    return ll_cas.ll_getStringValue(addr, casFeatCode_canonicalName);
  }
  /** @generated */    
  public void setCanonicalName(int addr, String v) {
        if (featOkTst && casFeat_canonicalName == null)
      jcas.throwFeatMissing("canonicalName", "cnh.cnhminer.types.Water");
    ll_cas.ll_setStringValue(addr, casFeatCode_canonicalName, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Water_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_kind = jcas.getRequiredFeatureDE(casType, "kind", "uima.cas.String", featOkTst);
    casFeatCode_kind  = (null == casFeat_kind) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_kind).getCode();

 
    casFeat_annotationString = jcas.getRequiredFeatureDE(casType, "annotationString", "uima.cas.String", featOkTst);
    casFeatCode_annotationString  = (null == casFeat_annotationString) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_annotationString).getCode();

 
    casFeat_canonicalName = jcas.getRequiredFeatureDE(casType, "canonicalName", "uima.cas.String", featOkTst);
    casFeatCode_canonicalName  = (null == casFeat_canonicalName) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_canonicalName).getCode();

  }
}



    