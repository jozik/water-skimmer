

/* First created by JCasGen Wed Feb 22 13:21:39 CST 2012 */
package cnh.cnhminer.types;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Wed Feb 22 13:21:39 CST 2012
 * XML source: /Volumes/PassportT/GitRepositories/TextMiner/Core/desc/Water.xml
 * @generated */
public class Water extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Water.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Water() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Water(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Water(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Water(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: kind

  /** getter for kind - gets 
   * @generated */
  public String getKind() {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_kind == null)
      jcasType.jcas.throwFeatMissing("kind", "cnh.cnhminer.types.Water");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Water_Type)jcasType).casFeatCode_kind);}
    
  /** setter for kind - sets  
   * @generated */
  public void setKind(String v) {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_kind == null)
      jcasType.jcas.throwFeatMissing("kind", "cnh.cnhminer.types.Water");
    jcasType.ll_cas.ll_setStringValue(addr, ((Water_Type)jcasType).casFeatCode_kind, v);}    
   
    
  //*--------------*
  //* Feature: annotationString

  /** getter for annotationString - gets 
   * @generated */
  public String getAnnotationString() {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_annotationString == null)
      jcasType.jcas.throwFeatMissing("annotationString", "cnh.cnhminer.types.Water");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Water_Type)jcasType).casFeatCode_annotationString);}
    
  /** setter for annotationString - sets  
   * @generated */
  public void setAnnotationString(String v) {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_annotationString == null)
      jcasType.jcas.throwFeatMissing("annotationString", "cnh.cnhminer.types.Water");
    jcasType.ll_cas.ll_setStringValue(addr, ((Water_Type)jcasType).casFeatCode_annotationString, v);}    
   
    
  //*--------------*
  //* Feature: canonicalName

  /** getter for canonicalName - gets 
   * @generated */
  public String getCanonicalName() {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_canonicalName == null)
      jcasType.jcas.throwFeatMissing("canonicalName", "cnh.cnhminer.types.Water");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Water_Type)jcasType).casFeatCode_canonicalName);}
    
  /** setter for canonicalName - sets  
   * @generated */
  public void setCanonicalName(String v) {
    if (Water_Type.featOkTst && ((Water_Type)jcasType).casFeat_canonicalName == null)
      jcasType.jcas.throwFeatMissing("canonicalName", "cnh.cnhminer.types.Water");
    jcasType.ll_cas.ll_setStringValue(addr, ((Water_Type)jcasType).casFeatCode_canonicalName, v);}    
  }

    