package cnh.cnhminer.annotator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.uima.resource.DataResource;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.SharedResourceObject;

public class ClueListFromPrefixMapResource_impl implements
		ClueListFromPrefixMapResource, SharedResourceObject {
	
	Map<String,List<SentimentClue>> clueListFromPrefixMap;

	@Override
	public List<SentimentClue> get(String prefix) {
		return clueListFromPrefixMap.get(prefix);
	}
	
	protected void addClue(String prefix, SentimentClue clue){
		List<SentimentClue> list = clueListFromPrefixMap.get(prefix);
		if (list == null){
			list = new ArrayList<SentimentClue>();
			list.add(clue);
			clueListFromPrefixMap.put(prefix, list);
		}
		else {
			list.add(clue);
		}
	}

	@Override
	public void load(DataResource aData) throws ResourceInitializationException {
		InputStream inStr = null;
	    try {
	      // open input stream to data
	      inStr = aData.getInputStream();
	      // read each line
	      BufferedReader reader = new BufferedReader(new InputStreamReader(inStr));
	      String line;
	      while ((line = reader.readLine()) != null) {
	    	  List<String> l = new ArrayList<String>();
	    	  for (String str : line.trim().split("\\s+")){
	    		  l.add(str.split("=")[1]);
	    	  }
	    	  //type=weaksubj len=1 word1=abandoned pos1=adj stemmed1=n priorpolarity=negative
	    	  SentimentClue clue = new SentimentClue(l.get(0),l.get(2),l.get(3),l.get(4),l.get(5));
	    	  addClue(l.get(2).substring(0, 2),clue);
	      }
	    } catch (IOException e) {
	      throw new ResourceInitializationException(e);
	    } finally {
	      if (inStr != null) {
	        try {
	          inStr.close();
	        } catch (IOException e) {
	        }
	      }
	    }
		
		
	}

}
