package cnh.cnhminer.annotator;

import java.util.List;

public interface ClueListFromPrefixMapResource {

	public List<SentimentClue> get(String prefix); 
}
