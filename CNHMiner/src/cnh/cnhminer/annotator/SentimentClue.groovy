package cnh.cnhminer.annotator;

import groovy.transform.Canonical;
import groovy.transform.EqualsAndHashCode;
import groovy.transform.Immutable

@EqualsAndHashCode
public class SentimentClue {
	//subj_type, word, pos, stemmed, prior_p
	String subjType
	String pos
	String priorP
	
	//TODO: create the matchers indicated below
	//private Matcher matcher

	public SentimentClue(String subjType, String word, String pos,
			String stemmed, String priorP) {
		this.subjType = subjType;
		this.pos = pos;
		this.priorP = priorP;
		
		if (stemmed.equals("y")){
			//this.matcher = stemmermatcher(word)
		}
		else{
			//this.matcher = regularmatcher(word)
		}
	}	
	
}
