package cnh.cnhminer.runner;

import java.io.IOException;

import org.apache.uima.UIMAFramework;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionProcessingEngine;
import org.apache.uima.collection.EntityProcessStatus;
import org.apache.uima.collection.StatusCallbackListener;
import org.apache.uima.collection.metadata.CasProcessorConfigurationParameterSettings;
import org.apache.uima.collection.metadata.CpeCasProcessor;
import org.apache.uima.collection.metadata.CpeCollectionReader;
import org.apache.uima.collection.metadata.CpeDescription;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.collection.metadata.NameValuePair;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.Level;
import org.apache.uima.util.XMLInputSource;

/**
 * Runs the CNHMiner.
 * 
 * @author Nick Collier
 */
public class CNHMinerRunner {

	private class StatusCallbackListenerImpl implements StatusCallbackListener {

		String collection;

		public StatusCallbackListenerImpl(String collection) {
			this.collection = collection;
		}

		public void entityProcessComplete(CAS cas, EntityProcessStatus entityProcessStatus) {
		}

		public void initializationComplete() {
		}

		public void batchProcessComplete() {
		}

		public void collectionProcessComplete() {
			synchronized (this) {
				UIMAFramework.getLogger().log(Level.INFO, collection + ": CPE Runner Finished");
				notifyAll();
			}
		}

		public void paused() {
		}

		public void resumed() {
		}

		public void aborted() {
			synchronized (this) {
				notifyAll();
			}
		}
	}

	private static String CPE_PATH = "./desc/CPE_Data_To_Analysis.xml";
	private static String SQL_PATH = "../SQLComponents/sql/";
	private static Boolean DELETE_COLLECTION = Boolean.TRUE;

	private static String[] COLLECTIONS = {	"testData"	};
	
	private StatusCallbackListenerImpl listener;

	private String gatherSettings(CasProcessorConfigurationParameterSettings settings) {
		StringBuilder buf = new StringBuilder();
		boolean append = false;
		for (NameValuePair pair : settings.getParameterSettings()) {
			if (append) buf.append("\n");
			buf.append("\t" + pair.getName() + ": " + pair.getValue());
			append = true;
		}
		
		return buf.toString();
	}

	public void run() throws InvalidXMLException, IOException, CpeDescriptorException, ResourceInitializationException {
		for (String collection : COLLECTIONS) {
			run(collection);
		}
	}

	public void run(String collection) throws InvalidXMLException, IOException, CpeDescriptorException,
			ResourceInitializationException {
		
		CpeDescription desc = UIMAFramework.getXMLParser().parseCpeDescription(new XMLInputSource(CPE_PATH));
		configure(desc, collection);
		CollectionProcessingEngine engine = UIMAFramework.produceCollectionProcessingEngine(desc);
		listener = new StatusCallbackListenerImpl(collection);
		engine.addStatusCallbackListener(listener);

		engine.process();
		try {
			// engine.process occurs in different thread
			// so we wait here for it to finish. The StatusCallbackListener
			// does a notifyAll() when the collection processing is complete
			// and that stops the wait.
			synchronized (listener) {
				listener.wait();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void configure(CpeDescription desc, String collection) throws CpeDescriptorException {
		CpeCollectionReader[] readers = desc.getAllCollectionCollectionReaders();
		for (CpeCollectionReader reader : readers) {
			CasProcessorConfigurationParameterSettings settings = reader.getConfigurationParameterSettings();
			settings.setParameterValue("collectionName", collection);
			UIMAFramework.getLogger().log(Level.INFO,
					"Reader Settings:\n" + gatherSettings(reader.getConfigurationParameterSettings()));
		}

		CpeCasProcessor[] procs = desc.getCpeCasProcessors().getAllCpeCasProcessors();
		for (CpeCasProcessor proc : procs) {
			if (proc.getName().equals("MongoDBOutAndScorer")) {
				CasProcessorConfigurationParameterSettings settings = proc.getConfigurationParameterSettings();
				settings.setParameterValue("collectionName", collection);
				settings.setParameterValue("dropCollection", DELETE_COLLECTION);
				UIMAFramework.getLogger().log(Level.INFO,
						"MongoDBOutAndScorer Settings:\n" + gatherSettings(proc.getConfigurationParameterSettings()));
			}
			if (proc.getName().equals("SQLOutputter")){
				CasProcessorConfigurationParameterSettings settings = proc.getConfigurationParameterSettings();
				settings.setParameterValue("collectionName", collection);
				settings.setParameterValue("sqlpath", SQL_PATH);
			}
		}
	}

	public static void main(String[] args) {
		try {
			new CNHMinerRunner().run();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
