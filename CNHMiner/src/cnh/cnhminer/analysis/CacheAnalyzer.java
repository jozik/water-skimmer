package cnh.cnhminer.analysis;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import anl.mifs.util.DateRange;

/**
 * Cache analysis class.
 * 
 * @author jozik
 * 
 */
public class CacheAnalyzer {

	private static final Version VERSION = Version.LUCENE_35;
	private static final String TIMESTAMP_FIELDNAME = "timestamp";
	protected static final String TARGET_FIELDNAME = "water_cache";
	private static final String datePattern = "MMMyyyy";
	private static final DateFormat dateFormat = new SimpleDateFormat(datePattern);

	IndexReader reader;
	IndexSearcher searcher;
	Directory dir;

	public CacheAnalyzer(Directory dir) {
		this.dir = dir;
		try {
			reader = IndexReader.open(dir);
			searcher = new IndexSearcher(reader);
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected TopDocs getTopDocsForQuery(String queryString) {
		Analyzer analyzer = new StandardAnalyzer(VERSION);
		String defaultField = "title";
		QueryParser parser = new QueryParser(VERSION, defaultField, analyzer);
		int nDocs = reader.numDocs();
		TopDocs result = null;
		try {
			Query q = parser.parse(queryString);
			result = searcher.search(q, nDocs);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Return the topdocs hits for the monthYear.
	 * 
	 * @param monthYear
	 * @return
	 */
	protected TopDocs getTopDocsForDateMonthYear(Date monthYear) {
		String queryString = createQueryStringFromMonthYear(monthYear);
		return getTopDocsForQuery(queryString);
	}

	/**
	 * Returns a list of Date objects set at Year, Month and the rest as
	 * default, spanning the months of the DateRange dr.
	 * 
	 * @param dr
	 *            the date range
	 * @return list of Date objects
	 */
	protected List<Date> getMonthYears(DateRange dr) {
		List<Date> result = new ArrayList<Date>();
		Date start = dr.getStart();
		Date end = dr.getEnd();
		Calendar calStart = new GregorianCalendar();
		calStart.setTime(start);

		Calendar cal = new GregorianCalendar(calStart.get(Calendar.YEAR),
				calStart.get(Calendar.MONTH), 1);
		while (cal.getTime().before(end)) {
			result.add(cal.getTime());
			cal.add(Calendar.MONTH, 1);
		}
		return result;
	}

	/**
	 * Creates the query string for date range corresponding to the monthYear.
	 * 
	 * @param monthYear
	 * @return
	 */
	protected String createQueryStringFromMonthYear(Date monthYear) {
		StringBuilder sb = new StringBuilder();
		sb.append(TIMESTAMP_FIELDNAME);
		sb.append(":[");

		// start date
		Calendar cal = new GregorianCalendar();
		cal.setTime(monthYear);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1; // since Calendar's months are
													// 0-based
		int day = 1;
		sb.append(createYearMonthDayString(year, month, day));

		// TO
		sb.append(" TO ");

		// end date
		int endDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		sb.append(createYearMonthDayString(year, month, endDay));

		sb.append("]");

		return sb.toString();
	}

	protected String createYearMonthDayString(int year, int month, int day) {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%04d", year));
		sb.append(String.format("%02d", month));
		sb.append(String.format("%02d", day));
		return sb.toString();
	}

	protected String[] getFieldValuesFromScoreDoc(ScoreDoc scoreDoc,
			String fieldName) {
		int doc = scoreDoc.doc;
		Document document;
		try {
			document = searcher.doc(doc);
			Fieldable f = document.getFieldable(TARGET_FIELDNAME);
			if (f == null) {
				return new String[0];
			} else {
				// note, assumes the fieldable contains a string value
				String fs = f.stringValue();
				if (fs != null) {
					fs = StringUtils.replace(fs, ",", " "); // in case it's a
															// comma separated
															// list of strings
					return StringUtils.split(fs);
				}
				Reader fr = f.readerValue();
				if (fr != null) {
					String tempString = StringUtils.replace(
							IOUtils.toString(fr), ",", " "); // in case it's a
																// comma
																// separated
																// list of
																// strings
					return StringUtils.split(tempString);
				}
				TokenStream tokenStream = f.tokenStreamValue();
				CharTermAttribute charTermAttribute = tokenStream
						.getAttribute(CharTermAttribute.class);
				List<String> list = new ArrayList<String>();
				while (tokenStream.incrementToken()) {
					list.add(charTermAttribute.toString());
				}
				return (String[]) list.toArray();
			}
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	protected Map<String, Long> getNumDocsMentioningTermsInMonthYear(
			Date monthYear, String fieldName) {
		Set<String> allTerms = new HashSet<String>();
		List<Set<String>> docTermSetList = new ArrayList<Set<String>>();
		TopDocs td = getTopDocsForDateMonthYear(monthYear);
		ScoreDoc[] sds = td.scoreDocs;
		for (ScoreDoc sd : sds) {
			String[] vals = getFieldValuesFromScoreDoc(sd, fieldName);
			Set<String> s = new HashSet<String>();
			s.addAll(Arrays.asList(vals));
			docTermSetList.add(s);
			allTerms.addAll(s); // add this set to allTerms set
		}
		Map<String, Long> result = new HashMap<String, Long>();
		for (String term : allTerms) {
			long count = 0;
			for (Set<String> s : docTermSetList) {
				if (s.contains(term))
					count++;
			}
			result.put(term, count);
		}
		return result;
	}

	protected Map<Date, Map<String, Long>> getNumDocsDataForDateRange(
			DateRange dateRange, String fieldName) {
		Map<Date, Map<String, Long>> result = new LinkedHashMap<Date, Map<String, Long>>();
		for (Date d : getMonthYears(dateRange)) {
			result.put(d, getNumDocsMentioningTermsInMonthYear(d, fieldName));
		}
		return result;
	}

	/**
	 * Transforms the num docs data into a csv string. Optionally, tags can be
	 * supplied to restrict the output to desired tags. Maintains the order of 
	 * an ordered map.
	 * 
	 * @param data
	 * @param tags
	 * @return
	 */
	protected String numDocsDataToString(Map<Date, Map<String, Long>> data,
			List<String> tags) {
		
		// iterate to get all tags
		Set<String> allTagSet = new LinkedHashSet<String>();
		for (Map<String, Long> m : data.values()) {
			allTagSet.addAll(m.keySet());
		}

		if (tags == null) {
			tags = new ArrayList<String>(allTagSet);
		}

		StringBuilder sb = new StringBuilder();

		// create header row
		boolean postFirst = false;
		for (String tag : tags) {
			if (postFirst)
				sb.append(",");
			else
				postFirst = true;
			sb.append(tag);
		}

		// iterate through data to get each row
		for (Entry<Date, Map<String, Long>> entry : data.entrySet()){
			sb.append("\n");
			sb.append(formatDate(entry.getKey()));
			for(String tag : tags){
				sb.append(",");
				Long value = entry.getValue().get(tag);
				if (value == null){
					sb.append("0");
				}
				else {
					sb.append(value.longValue());
				}
			}
		}
		
		return sb.toString();
		
	}

	protected String formatDate(Date date) {
		return dateFormat.format(date);
	}

	public static void main(String[] args) throws IOException {
		String indexLocation = "./scenarios/experimental/cacheFremont/index";
		Directory indexDirectory = FSDirectory.open(new File(indexLocation));
		CacheAnalyzer ca = new CacheAnalyzer(indexDirectory);
		Calendar cal = new GregorianCalendar(2009,Calendar.JANUARY,1);
		Date start = cal.getTime(); // start date
		cal.set(Calendar.YEAR,2009);
		cal.set(Calendar.MONTH,Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH,31);
		Date end = cal.getTime(); // end date
		DateRange dr = new DateRange(start,end);
		Map result = ca.getNumDocsDataForDateRange(dr, TARGET_FIELDNAME);
		System.out.println(ca.numDocsDataToString(result,null));
	}

}
