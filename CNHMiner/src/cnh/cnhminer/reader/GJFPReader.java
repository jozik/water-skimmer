package cnh.cnhminer.reader;

import java.io.IOException;
import java.util.Date;

import anl.mifs.reader.AbstractMIFSComponentReader;
import anl.mifs.reader.ContentReader;
import anl.mifs.reader.ReaderResult;
import anl.mifs.util.StandardIndexLineParser;

public class GJFPReader extends AbstractMIFSComponentReader {
	
	public static final String INDEX_FILE = "cnh.cnhminer.reader.GJFPIndex";
	
	private ContentReader ltReader = new GJFPHtmlReader();

	@Override
	protected ReaderResult getNextResult() throws IOException {
		lineItems = indexIter.nextParsedLine(lineItems);
		location = lineItems[StandardIndexLineParser.URL];
		Date date = new Date(Long.parseLong(lineItems[StandardIndexLineParser.TIMESTAMP]));
		checkDate(date);
		return ltReader.read(location, lineItems[StandardIndexLineParser.TITLE], date, "");
		
	}

	@Override
	protected String getIndexFileKey() {
		return INDEX_FILE;
	}

}
