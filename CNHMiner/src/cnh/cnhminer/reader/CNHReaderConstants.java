package cnh.cnhminer.reader;

public interface CNHReaderConstants {

	String GJFP_SOURCE = "Grand Junction Free Press";
	String AZDAILYSTAR_SOURCE = "Arizona Daily Star";
	String LASVEGASSUN_SOURCE = "Las Vegas Sun";
	String ARIZONADAILYSUN_SOURCE = "Arizona Daily Sun";
}
