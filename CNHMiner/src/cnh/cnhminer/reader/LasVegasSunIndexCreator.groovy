package cnh.cnhminer.reader;

import groovy.transform.Canonical;
import groovy.transform.InheritConstructors;
import groovy.util.slurpersupport.GPathResult;
import groovy.xml.StreamingMarkupBuilder;
import groovy.xml.XmlUtil;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import anl.mifs.reader.StringExtractor;

/**
 * This is just a stub. Does nothing at the moment. 3/2/2012
 * @author jozik
 *
 */
@Deprecated
public class LasVegasSunIndexCreator {

	private static String QUERY_STRING = "water"; // q
	private static String SEARCH_CATEGORY = "las-vegas" // Kat
	private static String START_DATE = "20100101" // StartDate
	private static String END_DATE = "20111231" // EndDate
	private static int START_INDEX_INCREMENT = 15
	private static String LINK_PREFIX = "http://www.gjfreepress.com"
	private static String INDEX_FILE = "./indices/expIndex.txt";
	private static String STRING_SEPARATOR = ":::"
	
	@InheritConstructors
	static class EndIndexNotFoundException extends Exception{
	}

	public static void main(String[] args) throws IOException, ParseException {
		LasVegasSunIndexCreator eic = new LasVegasSunIndexCreator();
		int startIndex = 0;
		int endIndex = 0;
		String baseUrl = "http://www.gjfreepress.com/apps/pbcs.dll/search?Category=SEARCH&q=${QUERY_STRING}&StartDate=${START_DATE}&EndDate=${END_DATE}&Kat=${SEARCH_CATEGORY}"  
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));
		while (true) {
			String urlString = baseUrl + "&Start=" + startIndex;
			System.out.println("Processing URL: " + urlString);
			
			def root = slurper.parse(urlString)
			if (startIndex == 0){
				try {
					endIndex = eic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For url ${urlString}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			eic.writeIndexLinks(root,writer)			
			
			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
		}
	}
	
	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}
	
	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

	/**
	 * Finds the first search results bar element and grabs the number of total results
	 * from the e.g., "1-15 of \d*" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

		def res = root.'**'.find{ it.@class == 'search_results_bar'}
		if (res == null) {
			throw new EndIndexNotFoundException("search_results_bar element not found.")
		}
		String numString = res.text()?.find(/ of (\d*)/){match, num -> num}
		if (numString == null){
			throw new EndIndexNotFoundException("search_results_bar element found but end index value not found.")
		}
		return Integer.parseInt(numString);
	}
	
	protected List<GPathResult> getTargetTables(GPathResult root){
		List<GPathResult> tables = root.'**'.findAll{it.name().equals("table")}
		List<GPathResult> goodTables = tables.findAll{it.tr.td.div.any{it.@class.text().equals("title")}}
	}
	
	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []
		
		def targetTables = getTargetTables(root)
		
		for (table in targetTables){
			LinkResult lr = new LinkResult()
			// find the div element containing the title information
			def title = table.tr.td.div.find{it.@class.text().equals("title")}
			String titleString = title.text()
			String url = title.a.@href.text()
			String timestampString = table.tr.td.span.find{it.@class.text().equals("timestamp")}.text()
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Date d = format.parse(timestampString)
			Long timestamp = d.getTime()
			lr.urlString = url
			lr.title = title
			lr.timestamp = timestamp
			result.add(lr)
		}
		
		return result
	}

}
