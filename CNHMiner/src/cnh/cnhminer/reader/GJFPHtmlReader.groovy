package cnh.cnhminer.reader

import groovy.util.slurpersupport.GPathResult;
import groovy.xml.XmlUtil;
import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;

import java.io.IOException
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.uima.internal.util.UIMALogFormatter;

import anl.mifs.reader.ContentReader;
import anl.mifs.reader.ParsedAuthors;
import anl.mifs.reader.ParsedDate;
import anl.mifs.reader.ReaderConstants;
import anl.mifs.reader.ReaderResult;

class GJFPHtmlReader implements ContentReader {

	private HTTPBuilder httpbuilder = new HTTPBuilder("")
	
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	
	protected String getContentForSubURL(String fullURL) throws IOException {
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri = fullURL
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				println ("Processing uri: $uri")
				sb.append(reader.text)
			}
			
			response.failure = {resp ->
				System.err.println ("Processing uri failed: $uri")
				System.err.println ("With response: ${resp.getStatus()}")
			}

		}
		return sb.toString()
	}
	
	@Override
	public ReaderResult read(String url, String title, Date date, String author)
			throws IOException {
		ReaderResult result = new ReaderResult();

	    result.setTitle(title);
	    result.setDate(new ParsedDate(date));
	    result.setLocation(url);
	    result.setSource(CNHReaderConstants.GJFP_SOURCE);
	    result.setLink("");
	
	    ParsedAuthors authors = new ParsedAuthors();
	    authors.addName("", "", "");
	    result.setAuthors(authors);
		
		String urlContent = getContentForSubURL(url).replaceAll(/<br \/>/, "\n")
		
		// Use the tagsoup parser to clean up the html so it can be parsed with xmlslurper
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		
		// Parse the modified url content
		def root = slurper.parseText(urlContent)
		String content = getContent(root)
		
		result.setContents(content);
		return result
	}
	
	protected String getContent(GPathResult root){
		// find the div with class = 'entry-content'
		def div = root.'**'.find{ it.@class.text().equals('entry-content')}
		// Get rid of author information section and trim
		return  StringUtils.substringBefore(div.text(), "-----").trim()
		
	}

}
