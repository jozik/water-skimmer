package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult
import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Map;

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import anl.mifs.util.IndexIterator;
import anl.mifs.util.StandardIndexLineParser;

public class LasVegasSunFirstIndexFilter {

	private static String LINK_PREFIX = "http://www.lasvegassun.com"
	private static String INDEX_FILE = "./indices/lasVegasSun_08_12_23_11_01_01_the.txt";// long
//	private static String INDEX_FILE = "./indices/lasVegasSun_Jan2012_May2012_APURLerrors.txt";// short
	private static String STRING_SEPARATOR = ":::"

	// entries for httpbuilder
	private static String BASE_URL = LINK_PREFIX
	private static String URI_PATH = "/search"
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	
	
	private static String OFFENDING_PREFIX = "http://www.lasvegassun.comhttp://"
	private static String CORRECTED_PREFIX = "http://"

	public static void main(String[] args) throws IOException, ParseException {
		LasVegasSunFirstIndexFilter lvsf = new LasVegasSunFirstIndexFilter(INDEX_FILE);
		lvsf.doFilter()
	}

	protected IndexIterator indexIter

	// Constructor for testing
	protected LasVegasSunFirstIndexFilter(){
		
	}
	
	public LasVegasSunFirstIndexFilter(String indexFile){
		this.indexFile = indexFile
		indexIter = new IndexIterator(indexFile);
		initializeHttpBuilder()
	}
	
	String indexFile
	PrintWriter outputPrintWriter
	PrintWriter errorsPrintWriter

	protected void doFilter(){
		boolean shouldCloseOutputPrintWriter = false
		if (outputPrintWriter == null){
			String filteredIndexFile = createFilteredIndexFileString(indexFile);
			outputPrintWriter = new PrintWriter(new BufferedWriter(new FileWriter(filteredIndexFile)));
			shouldCloseOutputPrintWriter = true
		}
		boolean shouldCloseErrorsPrintWriter = false
		if (errorsPrintWriter == null){
			String errorsFile = createAPURLErrorsIndexFileString(indexFile);
			errorsPrintWriter = new PrintWriter(new BufferedWriter(new FileWriter(errorsFile)));
			shouldCloseErrorsPrintWriter = true
		}
		while(indexIter.hasNext()){
			List<String[]> sameDateLines = getNextLinesWithSameDate()
			List<String[]> filteredLines = filterLines(sameDateLines)
			for (String[] line : filteredLines){
				outputPrintWriter.println(createIndexEntry(line))
			}
			outputPrintWriter.flush()
		}
		if (shouldCloseOutputPrintWriter){
			outputPrintWriter.close()
		}
		if (shouldCloseErrorsPrintWriter){
			errorsPrintWriter.close()
		}
	}
	
	static protected String createIndexEntry(String[] line){
		return StringUtils.join(line,":::")
	}
	
	protected List<String[]> filterLines(List<String[]> lines){
		List<String[]> uniqueLines = getUniqueLines(lines)
		List<String[]> urlFixedLines = getUrlFixedLines(uniqueLines)
		List<String[]> apRemovedLines = getNonAPLines(urlFixedLines)
		return apRemovedLines
	}
		
	/**
	 * Remove AP story url lines
	 */
	protected List<String[]> getNonAPLines(List<String[]> lines){
		List<String[]> result = new ArrayList<String[]>()
		// setup XmlSlurper
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		// setup httpbuilder
		initializeHttpBuilder()
		// for each line
		for (String[] line : lines){
			if (!isAPOrEmptyStory(line,slurper)){
				result.add(line)
			}
		}
		errorsPrintWriter.flush()
		return result		
	}
	
	protected boolean isAPOrEmptyStory(String[] line, XmlSlurper slurper){
		// follow link
		
		String url = line[StandardIndexLineParser.URL]
		String content = getContentForSubURL(url)
		if (content){
			// parse string
			def root = slurper.parseText(content)
			// find "<p class="byline">The Associated Press</p>"
			if (root.'**'.find{it.name().equals("p") && it.@class.text().equals("byline") && it.text().equals("The Associated Press")}){
				return true
			}
		}
		else {
			// print failed URLs to error writer
			errorsPrintWriter.println(createIndexEntry(line))
			return true
		}
		return false
	}
	
	private HTTPBuilder httpbuilder
	
	protected void initializeHttpBuilder(){
		httpbuilder = new HTTPBuilder("") // no base URL since there are two host variations
//		httpbuilder.handler.failure = { resp -> throw new IOException("httpbuilder request failed with response ${resp}") }
	}
	
	protected String getContentForSubURL(String fullURL) throws IOException {
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri = fullURL
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				println ("Processing uri: $uri")
				sb.append(reader.text)
			}
			
			response.failure = {resp ->
				System.err.println ("Processing uri failed: $uri")
				System.err.println ("With response: ${resp.getStatus()}")
			}

		}
		return sb.toString()
	}
	
	/**
	 * Remove duplicate url lines
	 * @param lines
	 * @return
	 */
	protected List<String[]> getUniqueLines(List<String[]> lines){
		List<String[]> uniqueLines = new ArrayList<String[]>()
		List<String> urls = new ArrayList<String>()
		// compare url
		for (String[] line : lines){
			String url = line[StandardIndexLineParser.URL]
			if (urls.contains(url)){
				continue
			}
			else {
				urls.add(url)
				uniqueLines.add(line)
			}
		}
		return uniqueLines
	}
	
	/**
	* Remove duplicate url lines
	* @param lines
	* @return
	*/
   protected List<String[]> getUrlFixedLines(List<String[]> lines){
	   List<String[]> urlFixedLines = new ArrayList<String[]>()

	   for (String[] line : lines){
		   String url = line[StandardIndexLineParser.URL]		   
		   if (url.startsWith(OFFENDING_PREFIX)){
			   String correctedURL = url.replaceFirst(OFFENDING_PREFIX, CORRECTED_PREFIX)
			   String [] replacementLine = Arrays.copyOf(line,line.size())
			   replacementLine[StandardIndexLineParser.URL] = correctedURL
			   urlFixedLines.add(replacementLine)
		   }
		   else {
			   urlFixedLines.add(line)
		   }
	   }
	   return urlFixedLines
   }
	

	/**
	 * Get the next lines in the index with the same dates
	 * @return
	 */
	protected List<String[]> getNextLinesWithSameDate(){
		List<String[]> result = new ArrayList<String[]>()
		Object [] firstLine = indexIter.nextParsedLine(null)
		String timestampString = firstLine[StandardIndexLineParser.TIMESTAMP]
		result.add(firstLine)
		while(indexIter.hasNext()){
			Object [] nextLine = indexIter.nextParsedLine(null)
			if (nextLine[StandardIndexLineParser.TIMESTAMP].equals(timestampString)){
				result.add(nextLine)
			}
			else {
				indexIter.rewind()
				break
			}
		}
		return result
	}

	static protected String createFilteredIndexFileString(String indexFile){
		return createModifiedIndexFileName(indexFile,"_filtered")
	}
	
	static protected String createAPURLErrorsIndexFileString(String indexFile){
		return createModifiedIndexFileName(indexFile,"_APURLerrors")
	}
	
	static protected String createModifiedIndexFileName(String indexFile, String modification){
		StringBuilder sb = new StringBuilder()
		int index = StringUtils.lastIndexOf(indexFile, ".")
		if (index == -1){
			sb.append(indexFile)
			sb.append(modification)
		}
		else {
			sb.append(StringUtils.substring(indexFile,0,index))
			sb.append(modification)
			sb.append(StringUtils.substring(indexFile, index))
		}
		return sb.toString()
	}
	
	


}
