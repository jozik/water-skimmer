package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Map.Entry;

import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method;

public class ExperimentalIndexCreator {

	private static String QUERY_STRING = "%22the%22"; // q
	private static String SEARCH = "SEARCH"
	private static String SEARCH_CATEGORY = "COMMUNITY_NEWS" // Kat
	private static String START_DATE = "19980101" // StartDate
	private static String END_DATE = "20121008" // EndDate
	private static int START_INDEX_INCREMENT = 15
	private static String LINK_PREFIX = "http://www.gjfreepress.com"
	private static String INDEX_FILE = "./indices/GJFP_Index_2000_2011_the.txt";
	private static String STRING_SEPARATOR = ":::"

	// entries for httpbuilder
	private static String BASE_URL = "http://www.gjfreepress.com"
	private static String URI_PATH = "/apps/pbcs.dll/search"
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"

	public static void main(String[] args) throws IOException, ParseException {
		ExperimentalIndexCreator eic = new ExperimentalIndexCreator();
		def map = [ Category: SEARCH, q: QUERY_STRING, StartDate:START_DATE, EndDate:END_DATE, Kat:SEARCH_CATEGORY]
		eic.initializeHttpBuilder(BASE_URL, map)
		int startIndex = 0;
		int endIndex = 0;
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));
		while (true) {

			String content = eic.getContentForStartIndex(startIndex)
			System.out.println("Processing StartIndex: " + startIndex);
			// call to get text, using httpbuilder request
			// should it throw IOException? yes.
			def root = slurper.parseText(content)
			if (startIndex == 0){
				try {
					endIndex = eic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For startIndex ${startIndex}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			eic.writeIndexLinks(root,writer)

			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
		}
	}


	private HTTPBuilder httpbuilder
	private Map qMap

	protected void initializeHttpBuilder(String url, Map defaultQMap){
		httpbuilder = new HTTPBuilder(url)
		httpbuilder.handler.failure = { resp -> throw new IOException("httpbuilder request failed with response ${resp.getStatus()}") }
		qMap = new HashMap(defaultQMap)
	}

	protected String getContentForStartIndex(int startIndex) throws IOException {
		qMap["Start"] = Integer.toString(startIndex)
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri.path = URI_PATH
			uri.query = qMap
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				sb.append(reader.text)
			}

		}
		return sb.toString()
	}

	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}

	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

	/**
	 * Finds the first search results bar element and grabs the number of total results
	 * from the e.g., "1-15 of \d*" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

		def res = root.'**'.find{ it.@class == 'search_results_bar'}
		if (res == null) {
			throw new EndIndexNotFoundException("search_results_bar element not found.")
		}
		String numString = res.text()?.find(/ of (\d*)/){match, num -> num}
		if (numString == null){
			throw new EndIndexNotFoundException("search_results_bar element found but end index value not found.")
		}
		return Integer.parseInt(numString);
	}

	protected List<GPathResult> getTargetTables(GPathResult root){
		List<GPathResult> tables = root.'**'.findAll{it.name().equals("table")}
		List<GPathResult> goodTables = tables.findAll{it.tr.td.div.any{it.@class.text().equals("title")}}
	}

	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []

		def targetTables = getTargetTables(root)

		for (table in targetTables){
			LinkResult lr = new LinkResult()
			// find the div element containing the title information
			def title = table.tr.td.div.find{it.@class.text().equals("title")}
			String titleString = title.text()
			String url = title.a.@href.text()
			String timestampString = table.tr.td.span.find{it.@class.text().equals("timestamp")}.text()
			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Date d = format.parse(timestampString)
			Long timestamp = d.getTime()
			lr.urlString = url
			lr.title = title
			lr.timestamp = timestamp
			result.add(lr)
		}

		return result
	}

}
