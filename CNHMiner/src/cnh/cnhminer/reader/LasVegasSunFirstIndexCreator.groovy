package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult
import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Map;

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class LasVegasSunFirstIndexCreator {

	// Query arguments
	private static String QUERY_STRING = "the"; // q
	private static String START_DATE = "2008-12-23" // from
	private static String END_DATE = "2011-01-01" // to
	private static String SORTBY = "date" // sortby
	private static String STORIES = "on" // stories
	
	private static int START_INDEX_INCREMENT = 40
	private static String LINK_PREFIX = "http://www.lasvegassun.com"
	private static String INDEX_FILE = "./indices/lasVegasSun_08_12_23_11_01_01_the.txt";
	private static String STRING_SEPARATOR = ":::"
	
	// entries for httpbuilder
	private static String BASE_URL = LINK_PREFIX
	private static String URI_PATH = "/search"
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	
	public static void main(String[] args) throws IOException, ParseException {
		LasVegasSunFirstIndexCreator lic = new LasVegasSunFirstIndexCreator();
		def map = [ q: QUERY_STRING, from:START_DATE, to:END_DATE, sortby:SORTBY, stories:STORIES]
		lic.initializeHttpBuilder(BASE_URL, map)
		int startIndex = 0;
		int endIndex = 0;

		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));
		int page = 1
		while (true) {

			String content = lic.getContentForPage(page)
			System.out.println("Processing page: " + page);
			// call to get text, using httpbuilder request
			// should it throw IOException? yes.
			def root = slurper.parseText(content)
			if (page == 1){
				try {
					endIndex = lic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For page ${page}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			lic.writeIndexLinks(root,writer)

			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
			page++
		}
	}
	
	private HTTPBuilder httpbuilder
	private Map qMap

	protected void initializeHttpBuilder(String url, Map defaultQMap){
		httpbuilder = new HTTPBuilder(url)
		httpbuilder.handler.failure = { resp -> throw new IOException("httpbuilder request failed with response ${resp.getStatus()}") }
		qMap = new HashMap(defaultQMap)
	}
	
	protected String getContentForPage(int page) throws IOException {
		qMap["p"] = Integer.toString(page)
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri.path = URI_PATH
			uri.query = qMap
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				println ("Processing uri: $uri")
				sb.append(reader.text)
			}

		}
		return sb.toString()
	}
	
	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}
	
	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

	/**
	 * Finds the total number of entries found
	 * from the e.g., "Found (509)" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

//		println XmlUtil.serialize(root)
		def res = root.'**'.find{ it.name().equals("h3") && it.@class.text().equals("search-results")}
		
		if (res == null) {
			throw new EndIndexNotFoundException("<end index> element not found.")
		}
		String numString = res.text()?.find(/(\d*) .+/){match, num -> num}
		if (numString == null){
			throw new EndIndexNotFoundException("Found (<end index>) element found but end index value not found.")
		}
		return Integer.parseInt(numString);
	}
	
	protected List<Pair<GPathResult,GPathResult>> getTargets(GPathResult root){
		List<GPathResult> lefts = root.'**'.findAll{it.name().equals("dd") && it.@class.text().equals("title story-headline")}
		List<GPathResult> rights = root.'**'.findAll{it.name().equals("dd") && it.@class.text().equals("date story-date")}
		// if there's an unmatched number of headlines and dates return null 
		if (lefts.size() != rights.size()){
			return null
		}
		List<Pair<GPathResult,GPathResult>> targets = new ArrayList<Pair<GPathResult,GPathResult>>()
		for (int i = 0 ; i < lefts.size(); i++){
			Pair<GPathResult,GPathResult> pair = new ImmutablePair<GPathResult, GPathResult>(lefts.get(i), rights.get(i))
			targets.add(pair)
		}
		return targets
	}
	
	protected String extractTimeString(String timeStringElement){
		return StringUtils.substringBefore(timeStringElement, "|").trim()
	}
	


	
	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []
		
		def targets = getTargets(root)
		
		for (t in targets){
			LinkResult lr = new LinkResult()
			
			String url = getURLString(t)
			String titleString = getTitleString(t)
			
			Long l = getTimestamp(t)
			if (l == null){
				continue
			}

			
			lr.urlString = url
			lr.title = titleString
			lr.timestamp = l
			result.add(lr)
		}
		
		return result
	}
	
	
	
	/**
	 * Gets the url string from the Pair.
	 * @param root
	 * @return
	 */
	protected String getURLString(Pair<GPathResult,GPathResult> pair){
		GPathResult left = pair.getLeft()
		return left.a.@href.text()
	}
	
	/**
	* Gets the title string from the Pair.
	* @param root
	* @return
	*/
   protected String getTitleString(Pair<GPathResult,GPathResult> pair){
	   GPathResult left = pair.getLeft()
	   return left.a.text()
   }
   
   /**
   * Gets the timestamp from the Pair.
   * @param root
   * @return
   */
  protected Long getTimestamp(Pair<GPathResult,GPathResult> pair){
	  GPathResult right = pair.getRight()
	  String timestampString = right.text()
	  Date d = getDateFromTimeString(extractTimeString(timestampString))
	  if (d == null) {
		  return null
	  }
	  return d.getTime()
  }
  
  protected Date getDateFromTimeString(String timeString){
	  SimpleDateFormat format = new SimpleDateFormat("E, MMM d, yyyy");
	  try {
	  Date d = format.parse(timeString)
	  return d
	  }
	  catch(ParseException pe){
		  System.err.println("Date not parsable for timeString $timeString")
		  return null
	  }
  }
   

}
