package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult
import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class ArizonaDailySunIndexCreator {

	// Query arguments
	private static String QUERY_STRING = ""; // q=water
	private static String START_DATE = "1/1/2005" // d1=1%2F1%2F2005 // 1%252F1%252F2005
	private static String END_DATE = "10/8/2012" // d2=5%2F23%2F2012
	private static String SORT_BY = "start_time" // s=start_time
	private static String SORT_ORDER = "asc" // sd=asc
	private static String CATEGORY = "news/local" // c%5B%5D=news%2Flocal
	private static String PER_PAGE = "100" // l=100
	private static String FORMAT = "html" // f=html
	private static String SPECIALIZATION = "article" // t=article
	
	private static int START_INDEX_INCREMENT = 100
	private static String LINK_PREFIX = "http://azdailysun.com"
	private static String INDEX_FILE = "./indices/azdailysun_Jan2005_Oct_8_2012_blank.txt";
	private static String STRING_SEPARATOR = ":::"
	
	// entries for httpbuilder
	private static String BASE_URL = LINK_PREFIX
	private static String URI_PATH = "/search"
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	
	public static void main(String[] args) throws IOException, ParseException {
		ArizonaDailySunIndexCreator aic = new ArizonaDailySunIndexCreator();
		def map = [ q:QUERY_STRING, d1:START_DATE, d2:END_DATE, s:SORT_BY, sd:SORT_ORDER, "c[]":CATEGORY, l:PER_PAGE, f:FORMAT, t:SPECIALIZATION]
//		map["c[]"] = CATEGORY
		aic.initializeHttpBuilder(BASE_URL, map)
		int startIndex = 0;
		int endIndex = 0;

		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));

		while (true) {

			String content = aic.getContentForPage(startIndex)
			System.out.println("Processing startIndex: " + startIndex);
			def root = slurper.parseText(content)
			if (startIndex == 0){
				try {
					endIndex = aic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For page ${startIndex}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			aic.writeIndexLinks(root,writer)

			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
		}
	}
	
	private HTTPBuilder httpbuilder
	private Map qMap

	protected void initializeHttpBuilder(String url, Map defaultQMap){
		httpbuilder = new HTTPBuilder(url)
		httpbuilder.handler.failure = { resp -> throw new IOException("httpbuilder request failed with response ${resp}") }
		qMap = new HashMap(defaultQMap)
	}
	
	protected String getContentForPage(int startIndex) throws IOException {
		qMap["o"] = Integer.toString(startIndex)
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri.path = URI_PATH
			uri.query = qMap
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				println ("Processing uri: $uri")
				sb.append(reader.text)
			}

		}
		return sb.toString()
	}
	
	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}
	
	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

/**
	 * Finds the total number of entries found
	 * from the e.g., "Found (1702)" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

//		println XmlUtil.serialize(root)
		def res = root.'**'.find{ it.name().equals("small") && it.text().startsWith("/ Found (")}
		
		if (res == null) {
			throw new EndIndexNotFoundException("Found (<end index>) element not found.")
		}
		String numString = res.text()?.find(/Found \((\d*)\)/){match, num -> num}
		if (numString == null){
			throw new EndIndexNotFoundException("Found (<end index>) element found but end index value not found.")
		}
		return Integer.parseInt(numString);
	}
	
	protected List<GPathResult> getTargets(GPathResult root){
		List<GPathResult> searchResults = root.'**'.findAll{it.name().equals("div") && it.@id.text().equals("search-results")}
		if (searchResults.isEmpty()) return new ArrayList<GPathResult>()
		List<GPathResult> targets = searchResults[0].'**'.findAll{it.name().equals("div") && it.@class.text().equals("index-list-item")}
		return targets
	}
	
	
	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []
		
		def targets = getTargets(root)
		
		for (t in targets){
			LinkResult lr = new LinkResult()
			// find the div element containing the title information
			def titleSection = t.h1.a.find{it.@title}
			// Article title
			String titleString = titleSection.@title.text()
			// Article url
			String url = titleSection.@href.text()
			
			// Article timestamp
			String timestampString = t.span.span.find{it.@class.equals("time")}.text()
			Date d = getDateFromTimeString(extractTimeString(timestampString))
			if (d == null){
				println("The date was not parsable for target: ${t.toString()}")
				continue
			}
			// corner case encountered
			if (url.startsWith("http://www.cafepress.com")){
				continue
			}
			Long timestamp = d.getTime()
			lr.urlString = url
			lr.title = titleString
			lr.timestamp = timestamp
			result.add(lr)
		}
		
		return result
	}
	
	
	protected String extractTimeString(String timeStringElement){
		return timeStringElement.trim() // just trim
	}
	
	protected Date getDateFromTimeString(String timeString){
		SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy hh:mm a");
		try {
		Date d = format.parse(timeString)
		return d
		}
		catch(ParseException pe){
			System.err.println("Date not parsable for timeString $timeString")
			return null
		}
	}
   

}
