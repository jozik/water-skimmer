package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat

import org.apache.commons.lang3.StringUtils

public class ArizonaDailyStarIndexCreator {

	private static String QUERY_STRING = ""; // q
	private static String SEARCH_CATEGORY = "COMMUNITY_NEWS" // Kat
	private static String START_DATE = "1%2F1%2F2000" // StartDate
//	private static String END_DATE = "8%2F1%2F2010" // shorter EndDate 	
	private static String END_DATE = "12%2F31%2F2011" // EndDate
	private static int START_INDEX_INCREMENT = 100
	private static String LINK_PREFIX = "http://azstarnet.com/"
	private static String INDEX_FILE = "./indices/azDailyStarIndex2000_2011_long.txt";
	private static String STRING_SEPARATOR = ":::"
	
	public static void main(String[] args) throws IOException, ParseException {
		ArizonaDailyStarIndexCreator aic = new ArizonaDailyStarIndexCreator();
		int startIndex = 0;
		int endIndex = 0;
		/**
		 * q = query
		 * d1 = starting date (uses %2F as "/")
		 * d2 = ending data (uses %2F as "/")
		 * s = list by "start_time"
		 * sd = list in order "asc"
		 * c[] = category is "news/local" (uses %5B and %5D as "[" and "]")
		 * l = number of results per page "100"
		 * f = format returned "html"
		 */
		String baseUrl = "http://azstarnet.com/search/?q=${QUERY_STRING}&d1=${START_DATE}&d2=${END_DATE}&s=start_time&sd=asc&c%5B%5D=news%2Flocal&l=100&f=html"  
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));
		while (true) {
			String urlString = baseUrl + "&o=" + startIndex;
			System.out.println("Processing URL: " + urlString);
			
			def root = slurper.parse(urlString)
			if (startIndex == 0){
				try {
					endIndex = aic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For url ${urlString}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			aic.writeIndexLinks(root,writer)			
			
			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
		}
	}
	
	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}
	
	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

	/**
	 * Finds the total number of entries found
	 * from the e.g., "Found (509)" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

//		println XmlUtil.serialize(root)
		def res = root.'**'.find{ it.name().equals("small") && it.text().startsWith("/ Found (")}
		
		if (res == null) {
			throw new EndIndexNotFoundException("Found (<end index>) element not found.")
		}
		String numString = res.text()?.find(/Found \((\d*)\)/){match, num -> num}
		if (numString == null){
			throw new EndIndexNotFoundException("Found (<end index>) element found but end index value not found.")
		}
		return Integer.parseInt(numString);
	}
	
	protected List<GPathResult> getTargets(GPathResult root){
		List<GPathResult> targets = root.'**'.findAll{it.name().equals("div") && it.@class.text().equals("index-list-item")}
		return targets
	}
	
	protected String extractTimeString(String timeStringElement){
		return timeStringElement.trim();
	}
	
	protected Date getDateFromTimeString(String timeString){
		SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm aaa");
		try {
		Date d = format.parse(timeString)
		return d
		}
		catch(ParseException pe){
			System.err.println("Date not parsable for timeString $timeString")
			return null
		}
	}

	
	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []
		
		def targets = getTargets(root)
		
		for (t in targets){
			LinkResult lr = new LinkResult()
			// find the div element containing the title information
			def titleSection = t.h1.a.find{it.@title}
			// Article title
			String titleString = titleSection.@title.text()
			// Article url
			String url = titleSection.@href.text()
			
			// Article timestamp
			String timestampString = t.span.span.find{it.@class.equals("time")}.text()
			Date d = getDateFromTimeString(extractTimeString(timestampString))
			if (d == null){
				continue
			}
			// corner case encountered
			if (url.startsWith("http://www.cafepress.com")){
				continue
			}
			Long timestamp = d.getTime()
			lr.urlString = url
			lr.title = titleString
			lr.timestamp = timestamp
			result.add(lr)
		}
		
		return result
	}

}
