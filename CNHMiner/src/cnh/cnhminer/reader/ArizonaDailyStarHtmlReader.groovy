package cnh.cnhminer.reader

import groovy.util.slurpersupport.GPathResult;
import groovy.xml.XmlUtil;

import java.io.IOException
import java.util.Date;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.uima.internal.util.UIMALogFormatter;

import anl.mifs.reader.ContentReader;
import anl.mifs.reader.ParsedAuthors;
import anl.mifs.reader.ParsedDate;
import anl.mifs.reader.ReaderConstants;
import anl.mifs.reader.ReaderResult;

class ArizonaDailyStarHtmlReader implements ContentReader {

	
	@Override
	public ReaderResult read(String url, String title, Date date, String author)
			throws IOException {
		ReaderResult result = new ReaderResult();

	    result.setTitle(title);
	    result.setDate(new ParsedDate(date));
	    result.setLocation(url);
	    result.setSource(CNHReaderConstants.AZDAILYSTAR_SOURCE);
	    result.setLink("");
	
	    ParsedAuthors authors = new ParsedAuthors();
	    authors.addName("", "", "");
	    result.setAuthors(authors);
		
		// Get the url content and replace all <br \> with a newline character
		URL u
		int retry = 0
		boolean success = false
		while (!success){
			try {
			println "Reading url $url"
			u = new URL(url)
			success = true
			}
			catch(MalformedURLException e){
				
				retry++
				println "MalformedURLException thrown for URL $url **** retry = $retry"
				if (retry == 5){
					break;
				}
			}
		}
		
		String urlContent = u.text.replaceAll(/<br \/>/, "\n")
		
		// Use the tagsoup parser to clean up the html so it can be parsed with xmlslurper
		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		
		// Parse the modified url content
		def root = slurper.parseText(urlContent)
		String content = getContent(root)
		
		result.setContents(content);
		return result
	}
	
	protected String getContent(GPathResult root){
		// find the div with class = 'entry-content'
		GPathResult div = root.'**'.find{ it.@class.text().equals('entry-content')}
		// No need to get rid of author information section
		
		if (div == null){
			return " "
		}
//		Iterate through the <p></p> sections and extract the text within
		Iterator cIter = div.childNodes()
		StringBuilder sb = new StringBuilder()
		while(cIter.hasNext()){
			def candidate = cIter.next()
			if (candidate.name().equals('p')){
				sb.append(candidate.text())
				sb.append('\n')
			}
		}
		
		String result = sb.toString()
		if (result.equals("")) return " "
		return result
		
	}

}
