package cnh.cnhminer.reader

import groovy.transform.Canonical;

@Canonical
class LinkResult {
	long timestamp
	String urlString
	String title
	String author
}
