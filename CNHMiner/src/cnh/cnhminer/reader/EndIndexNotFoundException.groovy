package cnh.cnhminer.reader

import groovy.transform.InheritConstructors

@InheritConstructors
class EndIndexNotFoundException extends Exception {

}
