package cnh.cnhminer.reader;

import groovy.util.slurpersupport.GPathResult
import groovyx.net.http.ContentType;
import groovyx.net.http.HTTPBuilder;
import groovyx.net.http.Method;

import java.io.BufferedWriter
import java.io.FileWriter
import java.io.IOException
import java.io.PrintWriter
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

public class YumaSunIndexCreator {

	/*
		q=water
		&
		site=yumasun
		&
		fistype=site
		&
		csort=newest
		&
		ctype=a
		&
		filter=p
		&
		num=100
		&
		start=0
	 */
	// Query arguments
	private static String QUERY_STRING = "water";
	private static String SITE = "yumasun";
	private static String FISTYPE = "site";
	private static String CSORT = "newest";
	private static String CTYPE = "a";
	private static String FILTER = "p";
	private static String NUM = "100";
	
	private static int START_INDEX_INCREMENT = 100
	private static String LINK_PREFIX = ""
	private static String INDEX_FILE = "./indices/yumasun_start_May2012.txt";
	private static String STRING_SEPARATOR = ":::"
	
	// entries for httpbuilder
	private static String BASE_URL = "http://www.yumasun.com"
	private static String URI_PATH = "/search/"
	private static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.5 Safari/534.55.3"
	private static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
	
	public static void main(String[] args) throws IOException, ParseException {
		YumaSunIndexCreator yic = new YumaSunIndexCreator();
		def map = [ q:QUERY_STRING, site:SITE , fistype:FISTYPE, csort:CSORT, ctype:CTYPE, filter:FILTER, num:NUM]
		yic.initializeHttpBuilder(BASE_URL, map)
		int startIndex = 0;
		int endIndex = 0;

		def tagsoupParser = new org.ccil.cowan.tagsoup.Parser();
		def slurper = new XmlSlurper(tagsoupParser)
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(INDEX_FILE)));

		while (true) {

			String content = yic.getContentForPage(startIndex)
			System.out.println("Processing startIndex: " + startIndex);
			def root = slurper.parseText(content)
			if (startIndex == 0){
				try {
					endIndex = yic.extractEndIndex(root)
				}
				catch(EndIndexNotFoundException me){
					System.err.println("For page ${startIndex}:")
					System.err.println(me.getMessage());
					break;
				}
			}
			yic.writeIndexLinks(root,writer)

			startIndex += START_INDEX_INCREMENT;
			if (startIndex >= endIndex) break;
		}
	}
	
	private HTTPBuilder httpbuilder
	private Map qMap

	protected void initializeHttpBuilder(String url, Map defaultQMap){
		httpbuilder = new HTTPBuilder(url)
		httpbuilder.handler.failure = { resp -> throw new IOException("httpbuilder request failed with response ${resp.getStatus()}") }
		qMap = new HashMap(defaultQMap)
	}
	
	protected String getContentForPage(int startIndex) throws IOException {
		qMap["start"] = Integer.toString(startIndex)
		StringBuilder sb = new StringBuilder()
		httpbuilder.request( Method.GET, ContentType.TEXT ) { req ->
			uri.path = URI_PATH
			uri.query = qMap
			headers.'User-Agent' = USER_AGENT
			headers.Accept = ACCEPT

			response.success = { resp, reader ->
				println ("Processing uri: $uri")
				sb.append(reader.text)
			}

		}
		return sb.toString()
	}
	
	protected void writeIndexLinks(GPathResult root, PrintWriter writer){
		List<LinkResult> linkResults = obtainLinks(root)
		for (LinkResult linkResult in linkResults){
			writer.println(createIndexEntry(linkResult,LINK_PREFIX))
		}
		writer.flush()
	}
	
	protected String createIndexEntry(LinkResult lr, String linkPrefix){
		StringBuffer sb = new StringBuffer()
		sb.append(lr.getTimestamp())
		sb.append(STRING_SEPARATOR)
		sb.append(linkPrefix + lr.getUrlString())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getTitle())
		sb.append(STRING_SEPARATOR)
		sb.append(lr.getAuthor() ?: "")
		return sb.toString()
	}

/**
	 * Finds the total number of entries found
	 * from the e.g., "Found (1702)" text element 
	 * @param root
	 * @return
	 */
	protected int extractEndIndex(GPathResult root) throws EndIndexNotFoundException{

//		println XmlUtil.serialize(root)
		def res = root.'**'.find{ it.name().equals("span") && it.@id.text().equals("resultsnumber")}
		
		if (res == null) {
			throw new EndIndexNotFoundException("Found (<end index>) element not found.")
		}
		String numString = res.text().trim()
		if (numString == null){
			throw new EndIndexNotFoundException("Found (<end index>) element found but end index value not found.")
		}
		return Integer.parseInt(numString.replace(",","")); // remove , separators
	}
	
	protected List<GPathResult> getTargets(GPathResult root){
		//<div class="sritemwrapper">
		List<GPathResult> targets = root.'**'.findAll{it.name().equals("div") && it.@class.text().equals("sritemwrapper")}
		return targets
	}
	
	
	/**
	 * Gets the link results from the target tables in the GPathResult. 
	 * @param root
	 * @return a list of LinkResult objects
	 */
	protected List<LinkResult> obtainLinks(GPathResult root){
		List<LinkResult> result = []
		
		def targets = getTargets(root)
		
		for (t in targets){
			LinkResult lr = new LinkResult()
			// find the span element containing the title information
			def titleSection = t.'**'.find{it.name().equals("span") && it.@class.text().equals("srheadline")}
			// Article title
			String titleString = titleSection.text().replace("...","").trim()
			
			// find the span element containing the link and date information
			// Article url
			def linkAndDateItem = t.'**'.find{it.name().equals("div") && it.@class.text().equals("srwrapper2")}
			
			String url = linkAndDateItem.a.@href.text()
			
			
			// Article timestamp
			//<span class="srpubdate">May 23, 2012</span>
			String dateString = linkAndDateItem.'**'.find{it.name().equals("span") && it.@class.text().equals("srpubdate")}.text()
			Date d = getDateFromTimeString(dateString)
			if (d == null){
				continue
			}
			Long timestamp = d.getTime()
			lr.urlString = url
			lr.title = titleString
			lr.timestamp = timestamp
			result.add(lr)
		}
		
		return result
	}
	
	protected Date getDateFromTimeString(String timeString){
		//May 23, 2012
		SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy");
		try {
		Date d = format.parse(timeString)
		return d
		}
		catch(ParseException pe){
			System.err.println("Date not parsable for timeString $timeString")
			return null
		}
	}
   

}
